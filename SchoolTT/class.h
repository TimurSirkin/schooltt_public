#ifndef CLASS_H
#define CLASS_H

#include <QPoint>
#include "tableelement.h"

class Class : public TableElement
{
public:
    Class(QString text, QWidget *parent);
    int getCapacity() const;
    void setCapacity(int value);
    void edit();
    int getId() const;

private:
    int capacity = 0;
    int id;
    QPoint dragStartPosition;

    QAction *action_clear = new QAction("Убрать карточки со всех уроков класса");

    // QWidget interface
protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void contextMenuEvent(QContextMenuEvent *event);
};

#endif // CLASS_H
