#ifndef CARDTABLE_H
#define CARDTABLE_H
#include <QLayout>
#include <QWidget>
#include <QVector>
#include "directory.h"
#include "day.h"
#include "class.h"
#include "tableelement.h"

class CardTable : public QWidget
{
    Q_OBJECT
public:
    explicit CardTable(QWidget *parent = nullptr);

    double spacerHeight = 10;
    double rowHeight = 50;
    double columnWidth = 60;
    double day_columnWidth = 40;
    double class_rowHeight = 30;
    double lesson_columnWidth = 30;
    double default_classPositionY = 0;
    double default_dayPositionX = 0;
    double minCellHeightForText = 30;
    double minCellWidthForText = 50;
    double minCellHeightForDoubleText = 50;
    const unsigned int minHight = 20;
    const unsigned int minWidth = 20;
    const unsigned int maxHight = 99;
    const unsigned int maxWidth = 99;

    void replaceTableElements();

    void set_spacerHeight(double value);
    void set_rowHeight(double value);
    void set_columnWidth(double value);
    void set_day_columnWidth(double value);
    void set_class_rowHeight(double value);
    void set_lesson_columnWidth(double value);
    void set_minCellHeightForText(double value);
    void set_minCellWidthForText(double value);
    void set_minCellHeightForDoubleText(double value);
    void AddClass(Class *newClass);
    void DeleteClass(Class *currentClass);

    Directory *directory = new Directory();

    QVector<Day*> dayList;
    QVector<Class*> classList;

    Day *getParentDay(Lesson *lesson);
    Cell *getCell(Lesson *lesson, Class* _class);

    QString getTopFlag();
    QString getBotFlag();
    QString getTipFlag();
    void setTopFlag(QString flag);
    void setBotFlag(QString flag);
    void setTipFlag(QString flag);

    Class* getClass(QString name);
    Day *getDay(QString name);

    void fill();
    int getClassId();

    void addDay(int lessonCount);
    void addDays( int dayCount = 1, int lessonCount = 1);
    void addClasses(int classCount = 1);

    void updateFocus();

private:
    void addLessons (Day* parentDay, int count = 1);
    void deleteDay(Day *currentDay);
    void deleteLesson(Lesson *currentLesson = nullptr, Day *parentDay = nullptr);
    void clearLesson(Lesson *lesson, Day *parentDay = nullptr);
    void clearClass(Class *_class);
    int getCardCount(Lesson *lesson, Day *parentDay);
    int getCardCount(Class *_class);

    QVector<QString> dayNameList;

    int classIdIndex = -1;

    void insertClass(Class *fromClass, Class *toClass);
    void setFocus(TableElement* currentElement);
    void removeFocus(TableElement* currentElement, bool isWaitForFocus = false);

    QGridLayout *tableGrid;

    TableElement *focusedElement = nullptr;

    Cell *createCell(Class* parentClass, Day *parentDay, Lesson* parentLesson);

    QString topFlag = "dis";
    QString botFlag = "class";
    QString tipFlag = "name";

signals:
    void spacerHeightChanged(double heigtDiff);
    void rowHeightChanged();
    void class_rowHeightChanged();
    void columnWidthChanged();
    void day_columnWidthChanged();
    void lesson_columnWidthChanged();
    void cell_minSizeChanged();

    void focused(Cell* cell);
    void focused(Class* _class);
    void focused(Day* day);
    void focused(Lesson* lesson);
    void unfocused(Cell* cell);
    void unfocused(Class* _class);
    void unfocused(Day* day);
    void unfocused(Lesson* lesson);
    void unfocused();

    void moveVScrollBar(int value);
    void moveHScrollBar(int value);

    void signal_topFlagChanged(QString flag);
    void signal_botFlagChanged(QString flag);
    void signal_tipFlagChanged(QString flag);
    void signal_repaintCell();
    void signal_toScrollArea(QDragEnterEvent *event);
    void signal_classSwaped();

public slots:
    void resizeDayList();
    void resizeLessonList();
    void resizeClassList();
    void resizeCellList();
    void resizeSpacers(double heightDiff);
    void repaintCellList();
    void onFocused();
    void onUnfocused();
    void onCardDragStarted();
    void onCardDragStoped();
    void slot_insertElement(TableElement *element);
    void slot_addLesson(Day *day);
    void slot_deleteLesson();
    void slot_clearLesson();
    void slot_clearDay();
    void slot_clearClass();
    void slot_cellDragStarted(Card *card, bool isFirst);

    // QWidget interface
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
};

#endif // CARDTABLE_H
