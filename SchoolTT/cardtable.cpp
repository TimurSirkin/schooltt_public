#include "cardtable.h"
#include <QPoint>
#include <QThread>
#include <QLabel>
#include <QColor>
#include <QDebug>
#include <QMessageBox>
#include <QLayout>
#include <QScrollArea>
#include <QSpacerItem>
#include "cell.h"
#include "day.h"
#include "class.h"
#include "lesson.h"
#include "teacher.h"
#include "teacherlistdialog.h"


CardTable::CardTable(QWidget *parent) : QWidget(parent)
  //Конструктор таблицы
{
    this->setFixedSize(day_columnWidth+lesson_columnWidth,class_rowHeight-spacerHeight);

    //Даю имена дням
    {
    dayNameList.push_back("Понедельник");
    dayNameList.push_back("Вторник");
    dayNameList.push_back("Среда");
    dayNameList.push_back("Четверг");
    dayNameList.push_back("Пятница");
    dayNameList.push_back("Суббота");
    dayNameList.push_back("Воскресенье");
    }

    //Связываю слоты и сигналы для изменения размеров таблицы
    connect(this, SIGNAL(rowHeightChanged()), this, SLOT(resizeCellList()));
    connect(this, SIGNAL(columnWidthChanged()), this, SLOT(resizeCellList()));
    connect(this, SIGNAL(rowHeightChanged()), this, SLOT(resizeDayList()));
    connect(this, SIGNAL(day_columnWidthChanged()), this, SLOT(resizeDayList()));
    connect(this, SIGNAL(rowHeightChanged()), this, SLOT(resizeLessonList()));
    connect(this, SIGNAL(lesson_columnWidthChanged()), this, SLOT(resizeLessonList()));
    connect(this, SIGNAL(class_rowHeightChanged()), this, SLOT(resizeClassList()));
    connect(this, SIGNAL(columnWidthChanged()), this, SLOT(resizeClassList()));
    connect(this, SIGNAL(spacerHeightChanged(double)), SLOT(resizeSpacers(double)));
    connect(this, SIGNAL(cell_minSizeChanged()), SLOT(repaintCellList()));

}

void CardTable::addDays(int dayCount, int lessonCount)
//Добавить опр-е кол-во дней, с опр-м кол-м уроков
{
    for (int daysIndex = 0; daysIndex < dayCount; daysIndex++)
   {
        Day *newDay = new Day(dayNameList[daysIndex],this);
        connect(newDay, SIGNAL(focused()), this, SLOT(onFocused()) );
        connect(newDay, SIGNAL(signal_clear()), this, SLOT(slot_clearDay()));
        newDay->resize(day_columnWidth, rowHeight);
        dayList.push_back(newDay);
        addLessons(newDay,lessonCount);
        newDay->show();
   }
    this->setFixedHeight(this->height() + dayCount*spacerHeight);
}

void CardTable::addLessons(Day *parentDay, int lessonsCount)
//Добавить опр-е кол-во уроков, в опр-й день
{
   for (int lessonIndex = 0; lessonIndex < lessonsCount; lessonIndex++)
    {
       Lesson *newLesson = new Lesson(QString::number(parentDay->lessonList.count()+1),this);
       connect(newLesson, SIGNAL(focused()), this, SLOT(onFocused()));
       connect(newLesson, SIGNAL(signal_delete()), this, SLOT(slot_deleteLesson()));
       connect(newLesson, SIGNAL(signal_clear()), this, SLOT(slot_clearLesson()));
       newLesson->resize(lesson_columnWidth, rowHeight);
       newLesson->setVisible(true);
       parentDay->lessonList.push_back(newLesson);
       parentDay->cellMatrix.push_back(QVector <Cell*>());

       for(int cellIndex = 0; cellIndex < classList.count(); cellIndex++)
       {
           createCell(classList[cellIndex], parentDay, newLesson);
       }
       //Отправляю сигнла, чтобы подвинуть скроллбар наверх
       emit moveHScrollBar(int(rowHeight));
    }
   this->setFixedHeight(this->height()+lessonsCount*rowHeight);
   parentDay->resize(day_columnWidth, parentDay->lessonList.count() * rowHeight);

   //Делаю удаление последнего урока снова доступным, если урок не единственный
   if(parentDay->lessonList.count()>1)
   {
       for (Lesson *lesson : parentDay->lessonList)
       {
           lesson->action_delete->setEnabled(true);
       }
   }

   replaceTableElements();
}

void CardTable::addClasses(int classCount)
//Добавить опр-е кол-во классов
{
    for (int classIndex = 0; classIndex < classCount; classIndex++)
    {
        Class *newClass = new Class(QString::number(classList.count()+1), this);
        AddClass(newClass);
    }
}

void CardTable::updateFocus()
{
    if(focusedElement!=nullptr)
    {
        setFocus(focusedElement);
    }
}

void CardTable::AddClass(Class *newClass)
//Добавить один конкретный класс
{
    newClass->resize(columnWidth, class_rowHeight);
    connect(newClass, SIGNAL(signal_wantToSwapWith(TableElement*)), this, SLOT(slot_insertElement(TableElement*)));
    connect(newClass, SIGNAL(focused()), this, SLOT(onFocused()));
    connect(newClass, SIGNAL(signal_clear()), this, SLOT(slot_clearClass()));
    connect(newClass, &Class::signal_visibleChanged, [=] () {replaceTableElements();});
    classList.push_back(newClass);
    for (Day *day : dayList)
    {
        for (Lesson *lesson : day->lessonList)
        {
            createCell(newClass, day, lesson);
        }
    }
    this->setFixedWidth(this->width()+columnWidth);
    newClass->setVisible(true);
    replaceTableElements();
}

void CardTable::deleteDay(Day *currentDay)
//Удалить конкретный день
{
    //Вызываем удаление всех уроков данного дня
    for (int lessonIndex = currentDay->lessonList.count()-1; lessonIndex >= 0; lessonIndex--)
    {
        deleteLesson(currentDay->lessonList[lessonIndex], currentDay);
    }

    //Удаляем сам день
    dayList.removeOne(currentDay);
    delete currentDay;

    //Подгоняем размер таблицы
    this->setFixedHeight(this->height()-spacerHeight);
}

void CardTable::deleteLesson(Lesson *currentLesson, Day *parentDay)
//Удалить последний урок у parentDay
{
    //Если передаю без указания родительского дня - сам его определяю
    if (parentDay == nullptr)
    {
        if(currentLesson == nullptr)
        {
            qDebug()<<"В функцию удаления дня нельзя передавать два nullptr";
        }
        for (Day *day : dayList)
        {
            if (day->lessonList.contains(currentLesson) == true )
            {
                parentDay = day;
            }
        }
    }
    else
    {
        if (currentLesson == nullptr)
        {
            currentLesson = parentDay->lessonList[parentDay->lessonList.count()-1];
        }
    }

    //Вычисляю строку
    int rowIndex = parentDay->lessonList.indexOf(currentLesson);

    //Удаляю все ячейки данной строки и делаю дисконекты
    for(int columnIndex = 0; columnIndex < parentDay->cellMatrix[rowIndex].count();columnIndex++)
    {
        if (( parentDay->cellMatrix[rowIndex][columnIndex]) == focusedElement)
        {
            onUnfocused();
        }
        disconnect(parentDay->cellMatrix[rowIndex][columnIndex]->classNameConnection);
        disconnect(parentDay->cellMatrix[rowIndex][columnIndex]->classVisibleConnection);
        delete(parentDay->cellMatrix[rowIndex][columnIndex]);
    }

    //Удаляю всю строку урока из матрицы
    parentDay->cellMatrix.removeAt(parentDay->lessonList.indexOf(currentLesson));

    parentDay->lessonList.removeOne(currentLesson);
    if (currentLesson == focusedElement)
    {
        onUnfocused();
    }
    delete currentLesson;
    parentDay->resize(parentDay->width(),parentDay->height()-rowHeight);
    replaceTableElements();
    this->setFixedHeight(this->height()-rowHeight);

    int lessonIndex = 0;
    for(Lesson *lesson : parentDay->lessonList)
    {
        lessonIndex++;
        lesson->setName(QString::number(lessonIndex));
    }

    //Делаю удаление последнего урока недоступным, если остался только один урок
    if(parentDay->lessonList.count()<2)
    {
        for (Lesson *lesson : parentDay->lessonList)
        {
            lesson->action_delete->setEnabled(false);
        }
    }

    //Отправляю сигнла, чтобы подвинуть скроллбар наверх
    emit moveHScrollBar(int(-rowHeight));
}

void CardTable::clearLesson(Lesson *lesson, Day *parentDay)
{
    if (parentDay == nullptr)
    {
        parentDay = getParentDay(lesson);
    }

    int rowIndex = parentDay->lessonList.indexOf(lesson);
    for(int columnIndex = 0; columnIndex < parentDay->cellMatrix[rowIndex].count();columnIndex++)
    {
        (parentDay->cellMatrix[rowIndex][columnIndex])->clearAll();
    }
}

void CardTable::clearClass(Class *_class)
//убрать карточки из столбца класса
{
    int columnIndex = classList.indexOf(_class);
    for (Day *day : dayList)
    {
        for(int rowIndex = 0; rowIndex < day->lessonList.count(); rowIndex++)
        {
            day->cellMatrix[rowIndex][columnIndex]->clearAll();
        }
    }
}

int CardTable::getCardCount(Lesson *lesson, Day *parentDay)
{
    int count = 0;
    for (Cell *cell : parentDay->cellMatrix[parentDay->lessonList.indexOf(lesson)])
    {
       count += cell->getCardCount();
    }
    return count;
}

int CardTable::getCardCount(Class *_class)
{
    int count = 0;
    int columnIndex = classList.indexOf(_class);
    for (Day *day : dayList)
    {
        for(int rowIndex = 0; rowIndex < day->lessonList.count(); rowIndex++)
        {
            count += day->cellMatrix[rowIndex][columnIndex]->getCardCount();
        }
    }
    return count;
}

void CardTable::DeleteClass(Class *currentClass)
//Удалить конкретный класс
{
    int columnIndex = classList.indexOf(currentClass);
    for (Day *day : dayList)
    {
        for(int rowIndex = 0; rowIndex < day->lessonList.count(); rowIndex++)
        {
            //Удаляю сами ячейки из столбца
            delete(day->cellMatrix[rowIndex][columnIndex]);
            //Удаляю из матрицы уничтоженный ячейки
            day->cellMatrix[rowIndex].removeAt(columnIndex);
        }
    }
    classList.removeOne(currentClass);
    delete currentClass;
    replaceTableElements();
    this->setFixedWidth(this->width()-columnWidth);
}

Day *CardTable::getParentDay(Lesson *lesson)
{
    Day *parentDay;
    for (Day *day : dayList)
    {
        if (day->lessonList.contains(lesson))
        {
            parentDay = day;
        }
    }
    return parentDay;
}

Cell *CardTable::getCell(Lesson *lesson, Class *_class)
{
    Day *day = getParentDay(lesson);
    int columnIndex = classList.indexOf(_class);
    int rowIndex = day->lessonList.indexOf(lesson);
    return (day->cellMatrix[rowIndex][columnIndex]);
}

QString CardTable::getTopFlag()
{
    return topFlag;
}

QString CardTable::getBotFlag()
{
    return botFlag;
}

QString CardTable::getTipFlag()
{
    return tipFlag;
}

void CardTable::setTopFlag(QString flag)
{
    topFlag = flag;
    emit signal_topFlagChanged(flag);
    emit signal_repaintCell();
}

void CardTable::setBotFlag(QString flag)
{
    botFlag = flag;
    emit signal_botFlagChanged(flag);
    emit signal_repaintCell();
}

void CardTable::setTipFlag(QString flag)
{
    tipFlag = flag;
    emit signal_tipFlagChanged(flag);
}

Class *CardTable::getClass(QString name)
{
    for(Class *_class : classList)
    {
       if (_class->getName() == name)
       {
           return _class;
       }
    }
    return nullptr;
}

Day *CardTable::getDay(QString name)
{
    for(Day *day : dayList)
    {
       if (day->getName() == name)
       {
           return day;
       }
    }
    return nullptr;
}

void CardTable::fill()
{
     addClasses(10);
     addDays(6,6);
     for (Day *day : dayList)
     {
         for (Lesson *lesson : day->lessonList)
         {
             for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
             {
                 cell->show();
             }
         }
     }
     replaceTableElements();

        Discipline *newDiscipline1 = new Discipline("Математика");
        Discipline *newDiscipline2 = new Discipline("Русский язык");
        Discipline *newDiscipline3 = new Discipline("Информатика");
        Discipline *newDiscipline4 = new Discipline("Английский язык");
        Discipline *newDiscipline5 = new Discipline("Технология");
        Discipline *newDiscipline6 = new Discipline("Биология");
        directory->addDiscipline(newDiscipline1);
        directory->addDiscipline(newDiscipline2);
        directory->addDiscipline(newDiscipline3);
        directory->addDiscipline(newDiscipline4);
        directory->addDiscipline(newDiscipline5);
        directory->addDiscipline(newDiscipline6);
        Room* room1 = new Room("142", 28);
        Room* room2 = new Room("11", 13);
        Room* room3 = new Room("315", 30);
        Room* room4 = new Room("Спортзал", 50);
        Room* room5 = new Room("168a", 24);
        Room* room6 = new Room("228", 19);
        directory->addRoom(room1);
        directory->addRoom(room2);
        directory->addRoom(room3);
        directory->addRoom(room4);
        directory->addRoom(room5);
        directory->addRoom(room6);


        {
        Teacher *newTeacher = new Teacher(directory);
        newTeacher->setSecondName("Морозов");
        newTeacher->setFirstName("Иван");
        newTeacher->setThirdName("Семенович");
        QMap<Class*, int> classMap;
        classMap.insert(classList[2], 15);
        classMap.insert(classList[3], 12);
        newTeacher->disciplineMap.insert(newDiscipline1, classMap);
        newTeacher->disciplineMap.insert(newDiscipline2, classMap);
        newTeacher->lessonList.push_back(dayList[0]->lessonList[0]);
        newTeacher->lessonList.push_back(dayList[0]->lessonList[2]);
        newTeacher->lessonList.push_back(dayList[0]->lessonList[3]);
        directory->addTeacher(newTeacher);
        }
        {
        Teacher *newTeacher = new Teacher(directory);
        newTeacher->setSecondName("Сиркин");
        newTeacher->setFirstName("Тимур");
        newTeacher->setThirdName("Владимирович");
        QMap<Class*, int> classMap;
        classMap.insert(classList[9], 3);
        classMap.insert(classList[7], 1);
        newTeacher->disciplineMap.insert(newDiscipline1, classMap);
        newTeacher->lessonList.push_back(dayList[1]->lessonList[0]);
        newTeacher->lessonList.push_back(dayList[2]->lessonList[0]);
        newTeacher->lessonList.push_back(dayList[0]->lessonList[3]);
        directory->addTeacher(newTeacher);
        }

        {
        Teacher *newTeacher = new Teacher(directory);
        newTeacher->setSecondName("Димасов");
        newTeacher->setFirstName("Димас");
        newTeacher->setThirdName("Семенович");
        newTeacher->lessonList.push_back(dayList[2]->lessonList[1]);
        newTeacher->lessonList.push_back(dayList[2]->lessonList[2]);
        newTeacher->lessonList.push_back(dayList[2]->lessonList[3]);
        QMap<Class*, int> classMap;
        classMap.insert(classList[0], 7);
        newTeacher->disciplineMap.insert(newDiscipline5, classMap);
        directory->addTeacher(newTeacher);
        }
}

int CardTable::getClassId()
{
    classIdIndex++;
    return classIdIndex;
}

void CardTable::addDay(int lessonCount)
{
    Day *newDay = new Day(dayNameList[dayList.count()],this);
    connect(newDay, SIGNAL(focused()), this, SLOT(onFocused()) );
    connect(newDay, SIGNAL(signal_clear()), this, SLOT(slot_clearDay()));
    newDay->resize(day_columnWidth, rowHeight);
    dayList.push_back(newDay);
    addLessons(newDay,lessonCount);
    this->setFixedHeight(this->height() + spacerHeight);
    newDay->show();
}

void CardTable::insertClass(Class *fromClass, Class *toClass)
//Меняю два класса местами
{
    //Определяю индекс в таблице по столбцу
    int columnIndexFirst = classList.indexOf(fromClass);
    int columnIndexSecond = classList.indexOf(toClass);

    //Как пробегаться в цикле и куда двигать
    int fromIndex;
    int toIndex;
    int direction;

    if (columnIndexFirst < columnIndexSecond)
    {
        fromIndex = columnIndexFirst + 1;
        toIndex = columnIndexSecond + 1;
        //Двигать буду влево
        direction = -1;
    }
    else
    {
        fromIndex = columnIndexFirst - 1;
        toIndex = columnIndexSecond - 1;
        //Двигать буду вправо
        direction = 1;
    }

    //Запомниаю столбец второго класса
    QVector <Cell*> cellList;
    for (Day *day : dayList)
    {
        for(int rowIndex = 0; rowIndex < day->lessonList.count(); rowIndex++)
        {
            cellList.push_back(day->cellMatrix[rowIndex][fromIndex + direction]);
        }
    }

    //Прохожусь по всем столбца, которые нужно двигать
    int columnIndex = fromIndex;
    while (columnIndex != toIndex)
    {
        //Двигаю каждый класс влево/вправо
        classList.replace(columnIndex + direction, classList[columnIndex]);

        //Двигаю ячейки
        for (Day *day : dayList)
        {
            for(int rowIndex = 0; rowIndex < day->lessonList.count(); rowIndex++)
            {
                //qDebug()<<"From " + day->cellMatrix[rowIndex][columnIndex]->getName();
                //qDebug()<<"To " + QString::number(columnIndex + direction);
                day->cellMatrix[rowIndex].replace(columnIndex + direction, day->cellMatrix[rowIndex][columnIndex]);
            }
        }
        columnIndex -= direction;
    }

    //Перестваляю весь первый столбце на место второго
    classList.replace(columnIndexSecond, fromClass);
    int rowsAbove = 0;
    for (Day *day : dayList)
    {
        for(int rowIndex = 0; rowIndex < day->lessonList.count(); rowIndex++)
        {
            day->cellMatrix[rowIndex].replace(columnIndexSecond, cellList[rowIndex+rowsAbove]);
        }
        rowsAbove += day->lessonList.count();
    }

    //Говорю матрице перестроиться
    replaceTableElements();
    emit signal_classSwaped();
}

void CardTable::setFocus(TableElement *currentElement)
//Установить фокус на данный элемент
{
    currentElement->isFocused = true;
    if ( Cell *cell = dynamic_cast<Cell*>(currentElement))
    {
        emit focused(cell);
    }
    if (Class *_class = dynamic_cast<Class*>(currentElement))
    {
        emit focused(_class);
    }
    if (Day *day = dynamic_cast<Day*>(currentElement))
    {
        emit focused(day);
    }
    if (Lesson *lesson = dynamic_cast<Lesson*>(currentElement))
    {
        emit focused(lesson);
    }
    focusedElement = currentElement;
    currentElement->repaint();
}

void CardTable::removeFocus(TableElement *currentElement, bool isWaitForFocus)
//Убрать фокус
{
    if(currentElement == nullptr)
    {
        return;
    }
    currentElement->isFocused = false;
    if(!isWaitForFocus)
    {
        emit unfocused();
        focusedElement = nullptr;
    }
    currentElement->repaint();
}

Cell *CardTable::createCell(Class *parentClass, Day* parentDay, Lesson *parentLesson)
{
    Cell *newCell = new Cell(this);
    newCell->setClass(parentClass);
    newCell->setLesson(parentLesson);
    newCell->classNameConnection = connect(parentClass, &Class::nameChanged, [=] () {newCell->setClass(parentClass);});
    newCell->classVisibleConnection = connect(parentClass, &Class::signal_changeCellVisible, [=] (bool visible) {newCell->setVisible(visible);});
    connect(newCell, SIGNAL(focused()), this, SLOT(onFocused()));
    newCell->resize(columnWidth,rowHeight);
    parentDay->cellMatrix[parentDay->lessonList.indexOf(parentLesson)].push_back(newCell);
    newCell->setVisible(parentClass->isVisible());
    return newCell;
}

void CardTable::resizeDayList()
//Измеить размер виджетов дней в таблице, в зависимости от текущих настроек
{
    //Костыльное решение - смотрит ширину первого дня таблицы (размер у всех одинковый, сл-но у самого дня будет такой же размер)
    //Потом расчитвает насколько изменились настройки
    double widthDiff =  day_columnWidth - dayList[0]->width();

    //Ресайзит
    for (Day *day : dayList)
    {
       day->resize(day_columnWidth, day->lessonList.count() * rowHeight);
    }
    this->setFixedWidth(this->width() + widthDiff);
    replaceTableElements();
}

void CardTable::resizeLessonList()
//Измеить размер виджетов уроков в таблице, в зависимости от текущих настроек
{
    double heightDiff  =  rowHeight - dayList[0]->lessonList[0]->height();
    double widthDiff =  lesson_columnWidth- dayList[0]->lessonList[0]->width();
    for (Day *day : dayList)
    {
        for (Lesson *lesson : day->lessonList)
        {
            lesson->resize(lesson_columnWidth, rowHeight);
            this->setFixedHeight( this->height()+heightDiff);
        }
    }
    this->setFixedWidth(this->width() + widthDiff);
    replaceTableElements();
}

void CardTable::resizeClassList()
//Измеить размер виджетов классов в таблице, в зависимости от текущих настроек
{
    double heightDiff  =  class_rowHeight - classList[0]->height();
    double widthDiff =  columnWidth - classList[0]->width();
    for (Class *_class : classList)
    {
        _class->resize(columnWidth,class_rowHeight);
        this->setFixedWidth(this->width() + widthDiff);
    }
    this->setFixedHeight(this->height()+heightDiff);
    replaceTableElements();
}

void CardTable::resizeCellList()
//Измеить размер виджетов ячеек в таблице, в зависимости от текущих настроек
{
    for (Day *day : dayList)
    {
        for (Lesson *lesson : day->lessonList)
        {
            for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
            {
                cell->resize(columnWidth, rowHeight);
            }
        }
    }
    replaceTableElements();
}

void CardTable::resizeSpacers(double heightDiff)
//Измеить размер разделителей дней в таблице, в зависимости от текущих настроек
{
    for (int dayIndex = 0 ; dayIndex < dayList.count(); dayIndex++)
    {
        this->setFixedHeight(this->height()+heightDiff);
    }
    this->setFixedHeight(this->height()-heightDiff);
    replaceTableElements();
}

void CardTable::repaintCellList()
//Перерисовать все ячейки
{
    for (Day *day : dayList)
    {
        for (Lesson *lesson : day->lessonList)
        {
            for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
            {
                cell->repaint();
            }
        }
    }
}

void CardTable::onFocused()
//Удаляю старый фокус и устанавлюваю новый
{
    TableElement *senderElement =  qobject_cast<TableElement*>(sender());
    if(senderElement == focusedElement)
    {
        return;
    }
    removeFocus(focusedElement, true);
    setFocus(senderElement);
}

void CardTable::onUnfocused()
//Слот на событие отмены фокуса
{
    removeFocus(focusedElement);
}

void CardTable::onCardDragStarted()
//Слот на начало перетаскивания карточки
{
    Card *card =  qobject_cast<Card*>(sender());
    Class *_class = card->getClass();

    for (Day *day : dayList)
    {
        for (Lesson *lesson : day->lessonList)
        {
            if( !card->getTeacher()->lessonList.contains(lesson))
            {
                continue;
            }
            bool isLessonValid = true;
            for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
            {
                if (!isLessonValid)
                {
                    continue;
                }
                if (cell->getFirstCard() != nullptr && cell->getFirstCard()->getTeacher() == card->getTeacher())
                {
                    isLessonValid = false;
                }
                if (cell->getSecondCard() != nullptr && cell->getSecondCard()->getTeacher() == card->getTeacher())
                {
                    isLessonValid = false;
                }
            }
            if (!isLessonValid)
            {
                continue;
            }

            Cell *currentCell = day->cellMatrix[day->lessonList.indexOf(lesson)][classList.indexOf(_class)];
            currentCell->isFocused = true;
            currentCell->repaint();
        }
    }
}

void CardTable::onCardDragStoped()
//Слот на конец перетаскивания карточки
{
    Card *card =  qobject_cast<Card*>(sender());

    for (Day *day : dayList)
    {
        for (Lesson *lesson : day->lessonList)
        {
            for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
            {
                if(cell->getClass() == card->getClass() && card->getTeacher()->lessonList.contains(lesson))
                {

                    if (focusedElement != cell)
                    {
                        cell->isFocused = false;
                        cell->repaint();
                    }
                }
            }
        }
    }
}

void CardTable::slot_insertElement(TableElement *element)
//Слот для того чтобы вставить элемент таблицы на другое место
{
    if (dynamic_cast<Class*>(sender()) != nullptr)
    {
        Class* fromClass = dynamic_cast<Class*>(element);
        Class* toClass = dynamic_cast<Class*>(sender());
        insertClass(fromClass, toClass);
    }
}

void CardTable::slot_addLesson(Day *day)
//Слот добавления урока
{
    QList<Teacher*> teacherList;
    if (!directory->teacherList.isEmpty())
    {
        QMessageBox warningBox(QMessageBox::Question,
                           day->getName() + ": " + QString::number(day->lessonList.count()+1) + " урок",
                           "Выбрать учителей, которым нужно добавить новый урок в рабочеее время?",
                           0, this);
        warningBox.addButton(QString::fromUtf8("Да"),
                         QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromUtf8("Нет"),
                         QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole)
        {
            TeacherListDialog *dialog = new TeacherListDialog(this);
            if (dialog->exec() == QDialog::Accepted)
            {
                teacherList = dialog->getTeacherList();
            }
        }
    }

    addLessons(day);

    if (!teacherList.isEmpty())
    {
        for (Teacher *teacher : teacherList)
        {
            teacher->lessonList.push_back(day->lessonList.last());
        }
    }

    repaint();
}

void CardTable::slot_deleteLesson()
//Слот удаления урока
{
    //Определяю какому дню пренадлежит урок
    Day *parentDay;
    Lesson *lesson = (Lesson*)sender();

    //Вычисляю последний урок дня
    for (Day *day : dayList)
    {
        if (day->lessonList.contains(lesson) == true )
        {
            parentDay = day;
        }
    }
    lesson = parentDay->lessonList[parentDay->lessonList.count()-1];

    //Проверяю, есть ли учителя, которые ведут данный урок
    QList<Teacher*> teacherList;
    QString teacherStr;
    for (Teacher *teacher : directory->teacherList)
    {
        if (teacher->lessonList.contains(lesson))
        {
            teacherList.push_back(teacher);
            teacherStr += "<b>" + teacher->getShortName() + "</b><br>";
        }
    }

    //Если список таких учителей пустой - прошу подтверждения
    if (teacherList.isEmpty())
    {
        QMessageBox warningBox(QMessageBox::Question,
                           parentDay->getName() + ": " + lesson->getName() + " урок",
                           "Удалить урок?",
                           0, this);
        warningBox.addButton(QString::fromUtf8("Да"),
                         QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromUtf8("Нет"),
                         QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole)
        {
            deleteLesson(lesson, parentDay);
        }
    }
    //Иначе
    else
    {
        //Считаю кол-во карточек на данной строке (уроке)
        int count = getCardCount(lesson, parentDay);

        //Показываю список такиъ учителей и предлагаю удалить у них этот урок из рабочего времени
        //Также говорю сколько карточек присутствует в данной строке (если такие есть)
        QString str = "У следующих учителей присуствует данный урок в рабочем времени:<br><br>" + teacherStr +"<br>";
        str += "Количесвто занятий на данном уроке в таблице: <b>" + QString::number(count) +"</b><br><br>";
        str += "Удаление урока из таблицы приведет к удалению его из рабочего времени всех учителей"
               " и возврату карточек, поставленных на данный урок, в список карточек.<br>";
        str += "Удалить урок?";
        QMessageBox warningBox(QMessageBox::Warning,
                           parentDay->getName() + ": " + lesson->getName() + " урок",
                           str,
                           0, this);
        warningBox.addButton(QString::fromUtf8("Да"),
                         QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromUtf8("Нет"),
                         QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole)
        {
            for (Teacher *teacher : teacherList)
            {
                teacher->lessonList.removeOne(lesson);
            }
            clearLesson(lesson);
            deleteLesson(lesson, parentDay);
        }
    }
}

void CardTable::slot_clearLesson()
//Слот очистки строки
{
    Lesson *lesson = (Lesson*)sender();
    Day *parentDay;
    for (Day *day : dayList)
    {
        if (day->lessonList.contains(lesson))
        {
            parentDay = day;
        }
    }

    int count = getCardCount(lesson, parentDay);
    if ( count != 0)
    {
        QMessageBox warningBox(QMessageBox::Question,
                           parentDay->getName() + ": " + lesson->getName() + " урок",
                           "Вы уверены, что хотите убрать все карточки данного урока из таблицы?"
                           "<br>Количество карточек: <b>" +QString::number(count) +  "</b>",
                           0, this);
        warningBox.addButton(QString::fromUtf8("Да"),
                         QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromUtf8("Нет"),
                         QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole)
        {
            clearLesson(lesson, parentDay);
        }
    }
}

void CardTable::slot_clearDay()
//Слот очисти дня от карточек
{
    Day *day = dynamic_cast<Day*>(sender());
    int count = 0;
    for (Lesson *lesson : day->lessonList)
    {
        count += getCardCount(lesson, day);
    }
    if ( count != 0)
    {
        QMessageBox warningBox(QMessageBox::Question,
                           day->getName(),
                           "Вы уверены, что хотите убрать все карточки данного дня из таблицы?"
                           "<br>Количество карточек: <b>" +QString::number(count) +  "</b>",
                           0, this);
        warningBox.addButton(QString::fromUtf8("Да"),
                         QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromUtf8("Нет"),
                         QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole)
        {
            for (Lesson *lesson : day->lessonList)
            {
               clearLesson(lesson, day);
            }
        }
    }
}

void CardTable::slot_clearClass()
//Слот очистки класса от карточек
{
    Class *_class = dynamic_cast<Class*>(sender());
    int count = getCardCount(_class);
    if ( count != 0)
    {
        QMessageBox warningBox(QMessageBox::Question,
                           _class->getName(),
                           "Вы уверены, что хотите убрать все карточки данного класса из таблицы?"
                           "<br>Количество карточек: <b>" +QString::number(count) +  "</b>",
                           nullptr, this);
        warningBox.addButton(QString::fromUtf8("Да"),
                         QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromUtf8("Нет"),
                         QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole)
        {
            clearClass(_class);
        }
    }
}

void CardTable::slot_cellDragStarted(Card *card, bool isFirst)
{
    Cell *senderCell =  dynamic_cast<Cell*>(sender());
    Class *_class = card->getClass();

    for (Day *day : dayList)
    {
        for (Lesson *lesson : day->lessonList)
        {
            Cell *cell = day->cellMatrix[day->lessonList.indexOf(lesson)][classList.indexOf(_class)];
            if(!card->getTeacher()->lessonList.contains(lesson))
            {
                continue;
            }

            //Проверяю ячейку отправителя на валидность по учителю
            if (cell->getFirstCard()!=nullptr)
            {
                if(!senderCell->isCardTeacherValid(cell->getFirstCard(), true))
                {
                    if (cell->getSecondCard()!=nullptr)
                    {
                        if(!senderCell->isCardTeacherValid(cell->getSecondCard(), false))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            if(!cell->isCardTeacherValid(senderCell->getCard(isFirst), isFirst))
            {
                continue;
            }

            cell->isFocused = true;
            cell->repaint();
        }
    }
}

void CardTable::mousePressEvent(QMouseEvent *event)
//Обработчик нажатия кнопки мыши
{
    //removeFocus(focusedElement);
}

void CardTable::mouseReleaseEvent(QMouseEvent *event)
//Переопределяю событие отпускания мыши над таблицей
{
    onUnfocused();
}

void CardTable::wheelEvent(QWheelEvent *event)
{
    int value = event->delta() / 60;
    if (event->modifiers() == Qt::CTRL)
    {
        if (rowHeight + value >= minHight && columnWidth + value >= minWidth
         && rowHeight + value <= maxHight && columnWidth + value <= maxWidth)
        {
            set_rowHeight(rowHeight + value);
            set_columnWidth(columnWidth + value);
        }
    }
    replaceTableElements();
}

void CardTable::replaceTableElements()
// Перемещает все элементы таблицы, опираясь на размеры указанные в нстройках и их положение в списках
{
    double dayPositionX = default_dayPositionX;
    int day_rowHeight = 0;

    for (Day *day : dayList)
    {
        double dayPositionY = class_rowHeight + day_rowHeight;
        day->move(dayPositionX, dayPositionY);
        day_rowHeight += day->height() + spacerHeight;

        for (Lesson *lesson : day->lessonList)
        {
            double lessonPositionX = day_columnWidth;
            double lessonPositionY = day->y()+ (day->lessonList.indexOf(lesson)*rowHeight);
            lesson->move(lessonPositionX, lessonPositionY);

            double cellPositionX = day_columnWidth+lesson_columnWidth;

            for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
            {
                cell->move(cellPositionX,lessonPositionY);
                if(!cell->isHidden())
                {
                    cellPositionX += columnWidth;
                }
            }
        }
    }

    double classPositionX = day_columnWidth + lesson_columnWidth;
    double classPositionY = default_classPositionY;
    for (Class *_class : classList)
    {
        _class->move(classPositionX, classPositionY);
        if (!_class->isHidden())
        {
            classPositionX += columnWidth;
        }
    }
}

void CardTable::set_spacerHeight(double value)
//Установить ширину разделителя дней
{
    double heightDiff = value - spacerHeight;
    spacerHeight = value;
    emit spacerHeightChanged(heightDiff);
}

void CardTable::set_rowHeight(double value)
//Установить высоту ячеек
{
    rowHeight = value;
    emit rowHeightChanged();
}

void CardTable::set_columnWidth(double value)
//Установить ширину ячеек
{
    columnWidth = value;
    emit columnWidthChanged();
}

void CardTable::set_day_columnWidth(double value)
//Установить ширину столбца дней
{
    day_columnWidth = value;
    emit day_columnWidthChanged();
}

void CardTable::set_class_rowHeight(double value)
//Установить высоту строки классов
{
    class_rowHeight = value;
    emit class_rowHeightChanged();
}

void CardTable::set_lesson_columnWidth(double value)
//Установить ширину строки дней
{
    lesson_columnWidth = value;
    emit lesson_columnWidthChanged();
}

void CardTable::set_minCellHeightForText(double value)
//Cеттер минимальный высоты ячейки для отображения текста
{
    minCellHeightForText = value;
    emit cell_minSizeChanged();
}

void CardTable::set_minCellWidthForText(double value)
//Cеттер минимальный ширины ячейки для отображения текста
{
    minCellWidthForText = value;
    emit cell_minSizeChanged();
}

void CardTable::set_minCellHeightForDoubleText(double value)
//Cеттер минимальный высоты ячейки для отображения всего текста
{
    minCellHeightForDoubleText = value;
    emit cell_minSizeChanged();
}

