#ifndef TABLEELEMENT_H
#define TABLEELEMENT_H

#include <QWidget>
#include <QString>
#include <QMargins>
#include <QMouseEvent>
#include <QPainter>
#include <QColor>
#include <QFont>
#include <QMenu>
class Card;

class TableElement : public QWidget
{
    Q_OBJECT
public:
    explicit TableElement(QWidget *parent = nullptr);
    void setName (QString txt);
    virtual QString getName () const;

    bool isFocused = false;

private:
    bool isInit = false;


protected:
    QString name;
    QColor _backgroundColor = QColor(200,200,200, 100);
    QColor _borderColor = Qt::white;
    QColor _focusedColor = QColor(71, 97, 140);
    QColor _foregroundColor = Qt::black;

    double defaultHeight=0;
    double defaultWidth=0;
    double fontSize = 6;


signals:
    void nameChanged();
    void focused();
    void signal_wantToSwapWith(TableElement *element);
    void signal_delete();
    void signal_clear();
    void signal_visibleChanged(bool visible);
    void signal_changeCellVisible(bool visible);
    void dragStarted(Card* card, bool isFirst);

public slots:


protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

    // QWidget interface
public slots:
    void setVisible(bool visible);
};

#endif // TABLEELEMENT_H
