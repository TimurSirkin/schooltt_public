#include "cardeditdialog.h"
#include <QStyle>
#include <QPalette>
#include <QDebug>
#include <QColorDialog>
#include <QToolTip>
#include "teacherdialog.h"
#include "mainwindow.h"
#include "styles.h"

//Диалоговое окно редактирования карточки

CardEditDialog::CardEditDialog(Card *pCard, CardTable *pCardTable, Directory *pDirectory, CardList *pCardList, QWidget *parent) : QDialog(parent)
  //Конструктор
{
    cardTable = pCardTable;
    directory = pDirectory;
    cardList = pCardList;
    card = pCard;

    setWindowTitle("Редактирование карточки");
    setMinimumSize(270, 400);
    okButton->setDefault(true);
    okButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogOkButton));
    cancelButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogCancelButton));

    //Поля для хранения цвета фона и текста
    backgroundColor = card->getBackgroundColor();
    foregroundColor = card->getForegroundColor();

    backgroundEditButton->setPalette(QPalette(card->getBackgroundColor()));
    foregroundEditButton->setPalette(QPalette(card->getForegroundColor()));
    connect(backgroundEditButton, &QPushButton::clicked, [this]() {choseColor(backgroundEditButton, &backgroundColor);});
    connect(foregroundEditButton, &QPushButton::clicked, [this]() {choseColor(foregroundEditButton, &foregroundColor);});
    connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    enabledCard = card->cloneWidget(this);
    mainLayout->addWidget(enabledCard, mainLayout->rowCount(),0,1,2);
    disabledCard = card->cloneWidget(this);
    disabledCard->setPalette(card->getEnabledPalette());
    mainLayout->addWidget(disabledCard,mainLayout->rowCount(),0,1,2);
    mainLayout->addItem(new QSpacerItem(0, 10, QSizePolicy::Minimum, QSizePolicy::Fixed), mainLayout->rowCount(), 0);

    {
    mainLayout->addWidget(new QLabel("<b>Фaмилия:<b>"),mainLayout->rowCount(),0,1,1);
    QPushButton *btn = new QPushButton(card->getTeacher()->getSecondName());
    btn->setStyleSheet(Styles::linkButton());
    connect(btn, SIGNAL(clicked(bool)), this, SLOT(linkToTeacher()));
    mainLayout->addWidget(btn, mainLayout->rowCount()-1,1,1,1,Qt::AlignLeft);
    }

    {
    mainLayout->addWidget(new QLabel("<b>Имя:</b>"),mainLayout->rowCount(),0,1,1);
    QPushButton *btn = new QPushButton(card->getTeacher()->getFirstName());
    btn->setStyleSheet(Styles::linkButton());
    connect(btn, SIGNAL(clicked(bool)), this, SLOT(linkToTeacher()));
    mainLayout->addWidget(btn,mainLayout->rowCount()-1,1,1,1,Qt::AlignLeft);
    }

    {
    mainLayout->addWidget(new QLabel("<b>Отчество:</b>"),mainLayout->rowCount(),0,1,2);
    QPushButton *btn = new QPushButton(card->getTeacher()->getThirdName());
    btn->setStyleSheet(Styles::linkButton());
    connect(btn, SIGNAL(clicked(bool)), this, SLOT(linkToTeacher()));
    mainLayout->addWidget(btn, mainLayout->rowCount()-1,1,1,1,Qt::AlignLeft);
    }

    {
    mainLayout->addWidget(new QLabel("<b>Предмет:</b>"),mainLayout->rowCount(),0,1,1);
    QPushButton *btn = new QPushButton(card->getDiscipline()->getName());
    disButton = btn;
    QString str;
    for (Class *_class : card->getTeacher()->disciplineMap[card->getDiscipline()].keys())
    {
        str += _class->getName() + "\n";
    }
    str.remove(str.length()-1,2);
    connect(disButton, &QPushButton::clicked, [=](){slot_showToolTip(str);});
    btn->setStyleSheet(Styles::linkButton());
    mainLayout->addWidget(btn,mainLayout->rowCount()-1,1,1,1,Qt::AlignLeft);
    }

    {
    mainLayout->addWidget(new QLabel("<b>Класс:</b>"),mainLayout->rowCount(),0,1,1);
    QPushButton *btn = new QPushButton(card->getClass()->getName());
    classButton = btn;
    QString str;
    str = "Количество учеников: " + QString::number(card->getClass()->getCapacity());
    connect(classButton, &QPushButton::clicked, [=](){slot_showToolTip(str);});
    btn->setStyleSheet(Styles::linkButton());
    mainLayout->addWidget(btn, mainLayout->rowCount()-1,1,1,1,Qt::AlignLeft);
    }

    mainLayout->addItem(new QSpacerItem(0, 10, QSizePolicy::Minimum, QSizePolicy::Fixed), mainLayout->rowCount(), 0);
    mainLayout->addWidget(new QLabel("<b>Рабочее время:</b>"),mainLayout->rowCount(),0,1,2);
    for (Day *day : cardTable->dayList)
    {
        QString str;
        bool isDayCaptured = false;
        for (Lesson *lesson : day->lessonList)
        {
            if (card->getTeacher()->lessonList.contains(lesson))
            {
                if (!isDayCaptured)
                {
                    str += ( day->getName() + ":  " + lesson->getName() );
                    isDayCaptured = true;
                }
                else
                {
                    str += ( ", "  + lesson->getName() );
                }
            }
        }
        if(isDayCaptured)
        {
            mainLayout->addWidget(new QLabel(str), mainLayout->rowCount(), 0, 1, 2);
        }
    }

    mainLayout->addItem(new QSpacerItem(0, 10, QSizePolicy::Minimum, QSizePolicy::Fixed), mainLayout->rowCount(), 0);
    mainLayout->addWidget(backgroundLabel, mainLayout->rowCount(), 0);
    mainLayout->addWidget(backgroundEditButton, mainLayout->rowCount(), 0, 1, 2);
    mainLayout->addWidget(foregroundLabel, mainLayout->rowCount(), 0);
    mainLayout->addWidget(foregroundEditButton, mainLayout->rowCount(), 0, 1, 2);
    mainLayout->addWidget(okButton,mainLayout->rowCount(),0);
    mainLayout->addWidget(cancelButton,mainLayout->rowCount()-1,1);
    this->setLayout(mainLayout);
}

void CardEditDialog::setCurrentColor()
{
    QPalette palette;

    palette.setColor(QPalette::Background, backgroundColor);
    palette.setColor(QPalette::Foreground, foregroundColor);
    enabledCard->setPalette(palette);

    QColor disBColor = backgroundColor;
    disBColor.setAlpha(disBColor.alpha()*0.5);
    QColor disFColor = foregroundColor;
    disFColor.setAlpha(disFColor.alpha()*0.5);
    palette.setColor(QPalette::Background, disBColor);
    palette.setColor(QPalette::Foreground, disFColor);
    disabledCard->setPalette(palette);
}

void CardEditDialog::slot_showToolTip(const QString &str)
{
    QToolTip::showText(QCursor::pos(), str);
}

void CardEditDialog::choseColor(QPushButton *currentButton, QColor *groundColor)
//Выбрать цвет
{
    //Первый параметр конструктора - начальный цвет(выбранный)
    QColorDialog* inputDialog = new QColorDialog(*groundColor, this);
    inputDialog->setWindowTitle("Выберите цвет");
    if (inputDialog->exec() == QDialog::Accepted)
    {
        QColor color = inputDialog->currentColor();
        currentButton->setPalette(color);
        *groundColor = color;

        setCurrentColor();
    }
}

void CardEditDialog::linkToTeacher()
//Открыть окно редактирования учителя из карточки
{
    TeacherDialog *teacherDialog = new  TeacherDialog(cardTable,  directory, cardList);
    teacherDialog->editTeacher(card->getTeacher(), nullptr, nullptr, true);
}
