#ifndef TEACHEREDITDIALOG_H
#define TEACHEREDITDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QList>
#include <QCheckBox>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QListWidget>
#include <QMap>
#include <QList>
#include <QScrollArea>
#include <QTreeWidget>
#include "teacher.h"
#include "discipline.h"
#include "cardtable.h"
#include "directory.h"
#include "cardlist.h"
#include "lesson.h"

class TeacherEditDialog : public QDialog
{
    Q_OBJECT
public:
    explicit TeacherEditDialog(CardTable *cardTable, Directory *directory, CardList *pCardList, bool isForLooking, Teacher *teacher = nullptr, QWidget *parent = nullptr);

    QLabel *secondNameLabel = new QLabel("Фамилия");
    QLabel *firstNameLabel = new QLabel("Имя:");
    QLabel *thirdNameLabel = new QLabel("Отчество:");
    QLabel *disciplineLabel = new QLabel("Список предметов:");
    QLabel *lessonLabel = new QLabel("Рабочее время:");
    QLabel *colorLabel = new QLabel("Цвет карточек:");
    QLineEdit *secondNameLine = new QLineEdit();
    QLineEdit *firstNameLine = new QLineEdit();
    QLineEdit *thirdNameLine = new QLineEdit();
    QScrollArea *disciplineScrollArea = new QScrollArea();
    QScrollArea *lessonScrollArea = new QScrollArea();
    QGridLayout *newLayout = new QGridLayout();
    QGridLayout *scrollLayout = new QGridLayout();
    QPushButton* okButton = new QPushButton( "Принять" );
    QPushButton* cancelButton = new QPushButton( "Отменить" );
    QPushButton* addDisciplineButton = new QPushButton("Добавить предмет");
    QPushButton *chooseColorButton = new QPushButton();
    QTreeWidget *lessonTree = new QTreeWidget();
    QMap<Discipline*, QMap<Class*, int>> disciplineMap;
    QList<Lesson*> lessonList;

    Directory* directory;
    QColor teacherBackgroundColor = QColor (177, 185, 198);


private:
    void editClassMap(Discipline* discipline, QMap<Class *, int>* classMap, QLabel *nameLabel, QPushButton *countButton);
    void addDiscipline(Discipline* discipline, QMap<Class *, int> classMap);
    void deleteDiscipline(Discipline* discipline, QList<QWidget *> wgtList);
    void selectCurrentDiscipline(QListWidget *listWidget);

    CardTable *cardTable;
    CardList *cardList;
    Teacher *teacher;

    void deleteLesson(Lesson *lesson);
    QList<QCheckBox*> checkBoxList;
    bool isForLooking;
    const unsigned int rowInc = 25;
    void checkLines();

public slots:
    void createDisciplineMap();
    void slot_deleteLesson(Lesson *lesson, QCheckBox *checkBox);
    void slot_changeDay(Day *day, QCheckBox *checkBox, QList<QCheckBox *> checkBoxList);

private slots:
    void chooseColor();
};

#endif // TEACHEREDITDIALOG_H
