#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QListWidgetItem>
#include <QLinkedList>
#include <QVector>
#include <QMainWindow>
#include <QFile>
#include <QCheckBox>
#include "teacher.h"
#include "class.h"
#include "card.h"
#include "discipline.h"
#include "exportoptions.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void createCard(Teacher *pTeacher, Class *pClass, Discipline *pDiscipline, int pCount);


private:
    void initialCardList();
    void deleteCard(Card *card);
    void fillDisciplineDirectory();

    QCheckBox *filterBox = new QCheckBox();

    CardTable *cardTable;
    CardList *cardList;

    void restart();
    void save();
    void saveAs();
    void saveAs(QString fileName);
    void open(QString fileName);
    void initCardTable();
    void initFilter();
    void initSortButtons();
    void initNewFile();
    void initNewFileBy();


    bool strToBool(QString str);
    QString boolToStr(bool flag);

    void saveTeachers();
    void saveClasses();
    void saveDisciplines();
    void saveDays();
    void saveCells();
    void saveCards();
    void saveSettings();
    void saveFilter();
    void saveExportOptions();

    void exportToXlsx(QString fileName, ExportOptions *exportOptions);
    void exportToTeacherXlsx(QString fileName, ExportOptions *exportOptions);

    void loadTeacher(QFile *file);
    void loadClass(QFile *file);
    void loadDiscipline(QFile *file);
    void loadDay(QFile *file);
    void loadCell(QFile *file);
    void loadCard(QFile *file);
    void loadSettings(QFile *file);
    void loadFilter(QFile *file);
    void loadExportOptions(QFile *file);

    QFile currentFile;
    bool isCurrenFileEmpty = true;
    void setCurrentFile(QString fileName);
    void clearCurrentFile();
    bool isFirstExec = true;

    QAction *action_save = new QAction("Сохранить расписание");
    QAction *action_new = new QAction("Создать новое расписание...");
    QAction *action_newBy = new QAction("Создать расписание на основе другого...");
    QAction *action_open = new QAction("Открыть расписание...");
    QAction *action_saveAs = new QAction("Сохранить расписание как...");
    QAction *action_exportToXls = new QAction("Экспортировать...");
    QAction *action_exportToTeacherXls = new QAction("Экспортировать относительно учителей...");

    QMenu *options = new QMenu("Настройки");
    QMenu *directoryes = new QMenu("Справочники");

private slots:
    void OpenTableOptionsWindow();
    void OpenCardOptionsWindow();
    void OpenRoomsDirectoryWindow(); 

public slots:
    void slot_editFilter();
    void slot_checkFilter(int value);
    void slot_newFile();
    void slot_newFileBy();
    void slot_saveFile();
    void slot_saveFileAs();
    void slot_exportToXlsx();
    void slot_exportToTeacherXlsx();
    void slot_openFile();

    void OpenDisciplinesDirectoryWindow();
    void OpenClassesDirectoryWindow();
    void OpenTeachersDirectoryWindow();

private:
    Ui::MainWindow *ui;
    ExportOptions *exportOptions;

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
