#include "teacher_disciplineeditdialog.h"
#include <QStyle>
#include <QMessageBox>
#include <QCheckBox>
#include <QLabel>
#include <QDebug>
#include <QIcon>
#include <QSpinBox>
#include "styles.h"
#include "cardlist.h"


Teacher_disciplineEditDialog::Teacher_disciplineEditDialog
(Teacher* pTeacher, Discipline *pDiscipline, CardList *cardList, QMap<Class *, int> *_classMap, CardTable *cardTable, QWidget *parent) : QDialog(parent)
{
    discipline = pDiscipline;
    teacher = pTeacher;
    classMap = *_classMap;
    okButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogOkButton));
    cancelButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogCancelButton));
    okButton->setStyleSheet(Styles::answerButtons());
    cancelButton->setStyleSheet(Styles::answerButtons());
    connect(okButton, SIGNAL(clicked()), this, SLOT(slot_checkCount()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    setLayout(mainLayout);

    mainLayout->addWidget(classArea, 0, 0, 1, 2);
    mainLayout->addWidget(okButton, 1, 0, 1, 1, Qt::AlignLeft);
    mainLayout->addWidget(cancelButton, 1, 1, 1, 1, Qt::AlignRight);
    int rowIndex = 0;
    for (Class *_class : cardTable->classList)
    {
        QLabel* newLabel= new QLabel(_class->getName());
        QSpinBox *newSpinBox = new QSpinBox();
        spinBoxList.push_back(newSpinBox);
        newSpinBox->setToolTip("Количество занятий в неделю");
        QCheckBox *newCheckBox = new QCheckBox();
        newSpinBox->setEnabled(false);
        scrollLayout->addWidget(newLabel, rowIndex, 0);
        scrollLayout->addWidget(newSpinBox, rowIndex, 1);
        scrollLayout->addWidget(newCheckBox, rowIndex, 2);
        rowIndex++;
        if(classMap.contains(_class))
        {
            newCheckBox->setChecked(true);
            newSpinBox->setEnabled(true);
            Card *card = cardList->getCard(pTeacher, pDiscipline, _class);
            if (card != nullptr)
            {
                newSpinBox->setMinimum(card->getMaxCount() - card->getCurrentCount());
            }
            newSpinBox->setValue(classMap[_class]);
        }
        else
        {
            Card *card = cardList->getCard(pTeacher, pDiscipline, _class);
            if (card != nullptr)
            {
                newSpinBox->setMinimum(card->getMaxCount() - card->getCurrentCount());
            }
        }
        newSpinBox->setToolTip("Количество занятий в неделю\nот " + QString::number(newSpinBox->minimum()) + " до " + QString::number(newSpinBox->maximum()));

        connect(newCheckBox, &QCheckBox::clicked, [=]()
        {change_classMap(_class, newSpinBox, newCheckBox);});
        connect(newSpinBox, &QSpinBox::editingFinished, [=]()
        {
            if(classMap.contains(_class))
            {
                classMap[_class] = newSpinBox->value();
            }
        });
    }

    QWidget *tempWidget = new QWidget();
    tempWidget->setLayout(scrollLayout);
    classArea->setWidget(tempWidget);

    setFixedSize(sizeHint());
}

void Teacher_disciplineEditDialog::slot_checkCount()
{
    int count = 0;
    for (QSpinBox *spinBox : spinBoxList)
    {
        if (spinBox->isEnabled() && spinBox->value() == 0)
        {
            count++;
        }
    }

    if (teacher == nullptr || count == 0)
    {
        accept();
    }
    else
    {
        QMessageBox warningBox(QMessageBox::Question,
                           discipline->getName(),
                           "У некоторых классов, которым вы поставили данный предмет, стоит нулевое количество занятий.<br>"
                           "Таких классов: <b>" + QString::number(count) + "</b><br>" + "Продолжить?",
                           nullptr);
        warningBox.addButton(QString::fromUtf8("Да"),
                         QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromUtf8("Нет"),
                         QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole)
        {
            accept();
        }
    }
}

void Teacher_disciplineEditDialog::change_classMap(Class *_class, QSpinBox *spinBox, QCheckBox *checkBox)
{
    int count = spinBox->value();
    bool isChecked = checkBox->isChecked();
    if (isChecked == true)
    {
        spinBox->setEnabled(true);
        classMap.insert(_class, count);
    }
    else
    {
        QString str;
        if (spinBox->minimum() > 0)
        {
            str = "В таблице присутствуют карточки с данным классом<br>В количестве: <b>" + QString::number(spinBox->minimum()) + "</b>"
            "<br>Убрать урок?";
        }
        else
        {
            str = "Убрать урок?";
        }
        QMessageBox warningBox(QMessageBox::Question,
                           _class->getName(),
                           str,
                           nullptr);
        warningBox.addButton(QString::fromUtf8("Да"),
                         QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromUtf8("Нет"),
                         QMessageBox::RejectRole);
        if (spinBox->minimum() > 0)
        {
           warningBox.setIcon(QMessageBox::Warning);
        }
        if (warningBox.exec() == QMessageBox::AcceptRole)
        {
            spinBox->setValue(0);
            spinBox->setEnabled(false);
            classMap.remove(_class);
        }
        else
        {
            checkBox->setChecked(true);
        }
    }
}
