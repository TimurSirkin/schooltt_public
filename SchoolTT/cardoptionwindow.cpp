#include "cardoptionwindow.h"
#include <QGridLayout>
#include <QGroupBox>
#include <QRadioButton>

CardOptionWindow::CardOptionWindow(CardTable *cardTable, QWidget *parent) : QDialog(parent)
{
    setWindowTitle("Настройки карточек");
    QLayout *mainLayout = new QVBoxLayout();
    setLayout(mainLayout);

    {
        QGroupBox *topGroupBox = new QGroupBox("Верхняя надпись");
        QLayout *layout = new QVBoxLayout();
        topGroupBox->setLayout(layout);
        mainLayout->addWidget(topGroupBox);

        {
            QRadioButton *radioButton = new QRadioButton("ФИО");
            radioButton->setChecked((cardTable->getTopFlag() == "name"));
            connect(radioButton, &QRadioButton::clicked, [=](bool checked){if(checked)cardTable->setTopFlag("name");});
            layout->addWidget(radioButton);
        }
        {
            QRadioButton *radioButton = new QRadioButton("Предмет");
            radioButton->setChecked((cardTable->getTopFlag() == "dis"));
            connect(radioButton, &QRadioButton::clicked, [=](bool checked){if(checked)cardTable->setTopFlag("dis");});
            layout->addWidget(radioButton);
        }
        {
            QRadioButton *radioButton = new QRadioButton("Класс");
            radioButton->setChecked((cardTable->getTopFlag() == "class"));
            connect(radioButton, &QRadioButton::clicked, [=](bool checked){if(checked)cardTable->setTopFlag("class");});
            layout->addWidget(radioButton);
        }
    }

    {
        QGroupBox *topGroupBox = new QGroupBox("Нижняя надпись");
        QLayout *layout = new QVBoxLayout();
        topGroupBox->setLayout(layout);
        mainLayout->addWidget(topGroupBox);

        {
            QRadioButton *radioButton = new QRadioButton("ФИО");
            radioButton->setChecked((cardTable->getBotFlag() == "name"));
            connect(radioButton, &QRadioButton::clicked, [=](bool checked){if(checked)cardTable->setBotFlag("name");});
            layout->addWidget(radioButton);
        }
        {
            QRadioButton *radioButton = new QRadioButton("Предмет");
            radioButton->setChecked((cardTable->getBotFlag() == "dis"));
            connect(radioButton, &QRadioButton::clicked, [=](bool checked){if(checked)cardTable->setBotFlag("dis");});
            layout->addWidget(radioButton);
        }
        {
            QRadioButton *radioButton = new QRadioButton("Класс");
            radioButton->setChecked((cardTable->getBotFlag() == "class"));
            connect(radioButton, &QRadioButton::clicked, [=](bool checked){if(checked)cardTable->setBotFlag("class");});
            layout->addWidget(radioButton);
        }
    }

    {
        QGroupBox *topGroupBox = new QGroupBox("Всплывающая подсказка");
        QLayout *layout = new QVBoxLayout();
        topGroupBox->setLayout(layout);
        mainLayout->addWidget(topGroupBox);

        {
            QRadioButton *radioButton = new QRadioButton("ФИО");
            radioButton->setChecked((cardTable->getTipFlag() == "name"));
            connect(radioButton, &QRadioButton::clicked, [=](bool checked){if(checked)cardTable->setTipFlag("name");});
            layout->addWidget(radioButton);
        }
        {
            QRadioButton *radioButton = new QRadioButton("Предмет");
            radioButton->setChecked((cardTable->getTipFlag() == "dis"));
            connect(radioButton, &QRadioButton::clicked, [=](bool checked){if(checked)cardTable->setTipFlag("dis");});
            layout->addWidget(radioButton);
        }
        {
            QRadioButton *radioButton = new QRadioButton("Класс");
            radioButton->setChecked((cardTable->getTipFlag() == "class"));
            connect(radioButton, &QRadioButton::clicked, [=](bool checked){if(checked)cardTable->setTipFlag("class");});
            layout->addWidget(radioButton);
        }
    }

    setFixedSize(minimumSize());
    setFixedWidth(220);
}
