#ifndef CARD_H
#define CARD_H
#include <QGridLayout>
#include <QLabel>
#include <QPalette>
#include <QColor>
#include "teacher.h"
#include "class.h"
#include "discipline.h"
class Directory;
class CardTable;
class CardList;

#include <QWidget>

class Card : public QWidget
{
    Q_OBJECT
public:
    explicit Card(QWidget *parent = nullptr);
    Card(Teacher *pTeacher, Class *pClass, Discipline *pDiscipline, int pCount, CardTable *pCardTable, Directory *pDirectory, CardList *pCardList, QWidget *pParent = nullptr);
    ~Card();

    void setTeacher(Teacher *value);
    void setClass(Class *const value);
    void setDiscipline(Discipline *const value);
    void setMaxCount(int value);
    void setCurrentCount(int value);
    void setBackgroundColor(QColor value);
    void setBColor(QColor value);
    void setFColor(QColor value);
    void setForegroundColor(QColor value);
    QColor getBackgroundColor() const;
    QColor getForegroundColor() const;
    Teacher *getTeacher() const;
    Class *getClass() const;
    Discipline *getDiscipline() const;
    int getMaxCount() const;
    int getCurrentCount() const;
    QPalette getEnabledPalette() const;

    void take();
    void add();

    void edit();

    QMetaObject::Connection DisciplineConnection;
    QMetaObject::Connection ClassConnection;
    QMetaObject::Connection TeacherNameConnection;
    QMetaObject::Connection TeacherDeleteConnection;
    QMetaObject::Connection topFlagConnection;
    QMetaObject::Connection botFlagConnection;
    QMetaObject::Connection tipFlagConnection;

    void setEnabledStyle();
    void restoreStyle();

    QWidget *cloneWidget(QWidget *parent);

    bool isEmpty();
    void setEmpty(bool value);

    void setTopFlag(const QString &flag);
    void setBotFlag(const QString &flag);
    void setTipFlag(const QString &flag);
    QString getTopFlag();
    QString getBotFlag();
    QString getTipFlag();

    void setHideToFilter(bool value);
    void setHideToTable(bool value);
    void setTransparentToTable(bool value);
    bool isHideToFilter();
    bool isHideToTable();
    bool isTransparentToTable();

    void updateDisColor();
private:
    Teacher *teacher;
    Class *_class;
    Discipline *discipline;
    CardTable *cardTable;
    Directory *directory;
    CardList *cardList;
    int maxCount = 0;
    int currentCount = 0;
    QPalette palette;
    QColor backgroundColor = QColor (177, 185, 198);
    QColor foregroundColor = QColor (0, 0, 0, 250);
    QColor dis_backgroundColor;
    QColor dis_foregroundColor;
    QString toolTipStr;
    int countFontSize = 18;

    QGridLayout *mainLayout = new QGridLayout();
    QLabel *nameLabel = new QLabel();
    QLabel *topLabel = new QLabel();
    QLabel *botLabel = new QLabel();
    QLabel *countLabel = new QLabel();

    QPoint dragStartPosition;
    void edit(Teacher *pTeacher, Class *pClass, Discipline *pDiscipline, int pCount);
    QString getStr(QString str);
    bool empty;

    QString topFlag;
    QString botFlag;
    QString tipFlag;

    bool hideToFilter = false;
    bool hideToTable = false;
    bool transparentToTable = false;

    void update();

signals:
    void deleteRequest();
    void colorChanged();
    void mousePressed();
    void dragStarted();
    void dragStoped();

public slots:
    void checkDiscipline(Discipline *pDiscipline);
    void checkClass(Discipline *pDiscipline, Class* pClass);
    void checkCount(Discipline *pDiscipline, Class* pClass, int pCount);

    // QWidget interface
protected:
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);


};

#endif // CARD_H
