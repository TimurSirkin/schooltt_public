#include "lesson.h"

//Урок

Lesson::Lesson(QString text ,QWidget *parent):TableElement(parent)
  //Конструктор
{
    _backgroundColor = QColor (200,200,200);
    name = text;
    fontSize = 10;

    defaultHeight = 30;
    defaultWidth = 30;

    connect(action_delete, &QAction::triggered, [this] () {emit signal_delete();});
    connect(action_clear, &QAction::triggered, [this] () {emit signal_clear();});
}

void Lesson::contextMenuEvent(QContextMenuEvent *event)
//Переопрделяю событие вызова контекстного меню
{
    QMenu *contextMenu = new QMenu();
    contextMenu->addAction(action_delete);
    contextMenu->addAction(action_clear);
    contextMenu->exec(event->globalPos());
}


