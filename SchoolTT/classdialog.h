#ifndef CLASSDIALOG_H
#define CLASSDIALOG_H

#include <QDialog>
#include <QListWidget>
#include <QLabel>
#include <QScrollArea>
#include <QVector>
#include "cardtable.h"
#include "class.h"
#include "room.h"
#include "directory.h"

class classdialog : public QDialog
{
    Q_OBJECT
public:
    explicit classdialog(CardTable *_parentWidget, Directory *_directory, QWidget *parent = nullptr);

private:
    Directory* directory;
    CardTable* cardTable;
    QScrollArea *scrollArea = new QScrollArea();
    QGridLayout *scrollLayout = new QGridLayout();
    QVector<Class*> classList;

    void editClass(Class *_class, QLabel *_nameLabel, QLabel *_sizeLabel);
    void deleteClass(Class *_class, QList<QWidget *> wgtList);

    QString getStr(QString str);

public slots:
    void addClass(Class* room);
    void createClass();
};

#endif // CLASSDIALOG_H
