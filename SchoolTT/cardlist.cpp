#include "cardlist.h"
#include <QDropEvent>
#include <QDrag>
#include <QMimeData>
#include <QDebug>
#include <QMessageBox>
#include "widgetmimedata.h"
#include "filtereditdialog.h"
#include "cell.h"
#include "card.h"
#include "teacher.h"
#include "widgetmimedata.h"

CardList::CardList(QWidget *parent) : QListWidget(parent)
  //Конструкток
{
    setSpacing(1);

    setSelectionMode(QAbstractItemView::NoSelection);
    this->setAcceptDrops(true);

    //Без этой строчки drop не будет работать - нужно указать мод драг-дропа
    this->setDragDropMode(QAbstractItemView:: InternalMove);

    setFixedWidth(160);
}

QString CardList::getSort()
//Получить текущий режим сортировки
{
    return sortFlag;
}

void CardList::addCard(Card *card)
//Добавить карточку в список
{
    bool teacherFlag = true;
    bool disciplineFlag = true;
    bool classFlag = true;
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        Card *currentCard = (Card*)(this->itemWidget(item(itemIndex)));
        if (currentCard->getTeacher() == card->getTeacher())
        {
            teacherFlag = false;
        }
        if (currentCard->getDiscipline() == card->getDiscipline())
        {
            disciplineFlag = false;
        }
        if (currentCard->getClass() == card->getClass())
        {
            classFlag = false;
        }
    }

    if(teacherFlag)
    {
        teacherFilterList.push_back(card->getTeacher());
    }
    if(disciplineFlag)
    {
        disciplineFilterList.push_back(card->getDiscipline());
    }
    if(classFlag)
    {
        classFilterList.push_back(card->getClass());
    }

    QListWidgetItem* newItem = new QListWidgetItem(this);
    QSize size;
    size.setWidth(card->sizeHint().width());
    size.setHeight(50);
    newItem->setSizeHint(size);
    setItemWidget(newItem, card );

    customList.insert(card, customList.count());

    if (sortFlag == "class")
    {
        setClassSort(isTtB);
    }
    else if (sortFlag == "teacher")
    {
        setTeacherSort(isTtB);
    }
    else if (sortFlag == "discipline")
    {
        setDisciplineSort(isTtB);
    }

    if (filterOn)
    {
        onFilter();
    }

    cardTable->updateFocus();
}

void CardList::deleteCard(Card *card)
//Удалить карточку из списка
{
    QListWidgetItem *itm = getItem(card);
    customList.remove(card);
    delete card;
    delete itm;
    int cardIndex = 0;
    for (Card *card : customList.keys())
    {
        customList[card] = cardIndex;
        cardIndex++;
    }

    bool teacherFlag = true;
    bool disciplineFlag = true;
    bool classFlag = true;
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        Card *currentCard = (Card*)(this->itemWidget(item(itemIndex)));
        if (currentCard->getTeacher() == card->getTeacher())
        {
            teacherFlag = false;
        }
        if (currentCard->getDiscipline() == card->getDiscipline())
        {
            disciplineFlag = false;
        }
        if (currentCard->getClass() == card->getClass())
        {
            classFlag = false;
        }
    }

    if(teacherFlag)
    {
        teacherFilterList.removeOne(card->getTeacher());
    }
    if(disciplineFlag)
    {
        disciplineFilterList.removeOne(card->getDiscipline());
    }
    if(classFlag)
    {
        classFilterList.removeOne(card->getClass());
    }
}

QListWidgetItem *CardList::getItem(Card *card)
//Получить айтем списка по карточке
{
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        Card *currentCard = (Card*)(this->itemWidget(item(itemIndex)));
        if(currentCard == card)
        {
            return item(itemIndex);
        }
    }
}

void CardList::refresh()
{
    customList.clear();
    teacherFilterList.clear();
    disciplineFilterList.clear();
    classFilterList.clear();
    clear();
}

void CardList::editFilter(Directory *directory, CardTable *cardTable)
//Вызвать редактирование фильтра
{
    FilterEditDialog *inputDialog = new FilterEditDialog(directory, this, cardTable);
    if (inputDialog->exec() == QDialog::Accepted)
    {
        teacherFilterList = inputDialog->teacherFilterList;
        disciplineFilterList = inputDialog->disciplineFilterList;
        classFilterList = inputDialog->classFilterList;
        cardVisible = inputDialog->isCardVisible();
        if (filterOn)
        {
            onFilter();
        }
    }
}

void CardList::onFilter()
//Включить фильтр
{
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        Card *card = (Card*)(this->itemWidget(item(itemIndex)));
        bool isContainsTeacher = teacherFilterList.contains(card->getTeacher());
        bool isContainsDiscipline = disciplineFilterList.contains(card->getDiscipline());
        bool isContainsClass = classFilterList.contains(card->getClass());
        bool isCardEmpty = (card->getCurrentCount() == 0);
        if (isContainsTeacher == false || isContainsDiscipline == false || isContainsClass == false || (isCardEmpty && !cardVisible))
        {
            setHideToFilter(item(itemIndex), true);
        }
        else
        {
            setHideToFilter(item(itemIndex), false);
        }
    }
    filterOn = true;
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        checkHide(currentItem);
    }
}

void CardList::offFilter()
//Выключить фильтр
{
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        setHideToFilter(item(itemIndex), false);
    }
    filterOn = false;
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        checkHide(currentItem);
    }
}

void CardList::showWithClass(Class *_class)
//Выделить среди остальных карточки с данным классом
{
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        Card *card = (Card*)(itemWidget(currentItem));
        if (card->getClass() != _class)
        {
            setHideToTable(currentItem, true);
        }
        else
        {
            setHideToTable(currentItem, false);
        }
    }
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        checkHide(currentItem);
    }
}

void CardList::showWithLesson(Lesson *lesson)
//Выделить среди остальных карточки с данным уроком (учителель может провести этот урок)
{
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        Card *card = (Card*)(itemWidget(currentItem));
        Teacher *teacher = card->getTeacher();
        setHideToTable(currentItem, !teacher->lessonList.contains(lesson));
        QList<Teacher*> teacherList = getTeacherList(lesson);
        card->setTransparentToTable(teacherList.contains(teacher));
    }
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        checkHide(currentItem);
    }
}

void CardList::showWithDay(Day *day)
//Выделить среди остальных карточки с данным днем
{
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        Card *card = (Card*)(itemWidget(currentItem));
        Teacher *teacher = card->getTeacher();
        bool isContains = false;
        for(Lesson *lesson : day->lessonList)
        {
            if (teacher->lessonList.contains(lesson))
            {
                isContains = true;
            }
        }
        if (isContains)
        {
            setHideToTable(currentItem, false);
        }
        else
        {
            setHideToTable(currentItem, true);
        }
    }
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        checkHide(currentItem);
    }
}

void CardList::showWithClassAndLesson(Class *_class, Lesson *lesson)
//Выделить с классом и уроком
{
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        Card *card = (Card*)(itemWidget(currentItem));
        Teacher *teacher = card->getTeacher();
        if (card->getClass() != _class || !teacher->lessonList.contains(lesson))
        {
            setHideToTable(currentItem, true);
        }
        else
        {
            setHideToTable(currentItem, false);
        }

        QList<Teacher*> teacherList = getTeacherList(lesson);
        card->setTransparentToTable(teacherList.contains(teacher));
    }
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        checkHide(currentItem);
    }
}

void CardList::showAll()
//Выделить все карточки
{
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        Card *card = (Card*)(itemWidget(currentItem));
        setHideToTable(currentItem, false);
        card->setTransparentToTable(false);
    }
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        QListWidgetItem *currentItem = item(itemIndex);
        checkHide(currentItem);
    }
}

void CardList::setClassSort(bool isTopToBottom)
//Установить сортировку по классам
{
    sortFlag = "class";
    isTtB = isTopToBottom;
    for (int roundIndex = 0; roundIndex < count(); roundIndex++)
    {
        for(int rowIndex = 0; rowIndex < count()-1; rowIndex++)
        {
            Card *firstCard = dynamic_cast<Card*>(itemWidget(item(rowIndex)));
            Card *secondCard = dynamic_cast<Card*>(itemWidget(item(rowIndex+1)));
            Class* firstClass = firstCard->getClass();
            Class* secondClass = secondCard->getClass();
            if(isTopToBottom)
            {
                if(cardTable->classList.indexOf(firstClass) > cardTable->classList.indexOf(secondClass))
                {
                    swapCards(rowIndex, rowIndex+1);
                }
            }
            else
            {
                if(cardTable->classList.indexOf(firstClass) < cardTable->classList.indexOf(secondClass))
                {
                    swapCards(rowIndex, rowIndex+1);
                }
            }
        }
    }
}

void CardList::setTeacherSort(bool isTopToBottom)
//Установить сортировку по учителям
{
    sortFlag ="teacher";
    isTtB = isTopToBottom;
    for (int roundIndex = 0; roundIndex < count(); roundIndex++)
    {
        for(int rowIndex = 0; rowIndex < count()-1; rowIndex++)
        {
            Card *firstCard = dynamic_cast<Card*>(itemWidget(item(rowIndex)));
            Card *secondCard = dynamic_cast<Card*>(itemWidget(item(rowIndex+1)));
            QString firstTeacher = firstCard->getTeacher()->getSecondName() + firstCard->getTeacher()->getFirstName() + firstCard->getTeacher()->getThirdName();
            QString secondTeacher = secondCard->getTeacher()->getSecondName() + secondCard->getTeacher()->getFirstName() + secondCard->getTeacher()->getThirdName();

            if(isTopToBottom)
            {
                if(QString::compare(firstTeacher, secondTeacher) <= 0)
                {
                    continue;
                }
            }
            else
            {
                if(QString::compare(firstTeacher, secondTeacher) >= 0)
                {
                    continue;
                }
            }
            swapCards(rowIndex, rowIndex+1);
        }
    }
}

void CardList::setDisciplineSort(bool isTopToBottom)
//Установить сортировку по предметам
{
    sortFlag = "discipline";
    isTtB = isTopToBottom;
    for (int roundIndex = 0; roundIndex < count(); roundIndex++)
    {
        for(int rowIndex = 0; rowIndex < count()-1; rowIndex++)
        {
            Card *firstCard = dynamic_cast<Card*>(itemWidget(item(rowIndex)));
            Card *secondCard = dynamic_cast<Card*>(itemWidget(item(rowIndex+1)));
            QString firstDiscipline = firstCard->getDiscipline()->getName();
            QString secondDiscipline = secondCard->getDiscipline()->getName();

            if(isTopToBottom)
            {
                if(QString::compare(firstDiscipline, secondDiscipline) <= 0)
                {
                    continue;
                }
            }
            else
            {
                if(QString::compare(firstDiscipline, secondDiscipline) >= 0)
                {
                    continue;
                }
            }
            swapCards(rowIndex, rowIndex+1);
        }
    }
}

void CardList::setCustomSort()
//Устаовить пользовательскую сортировку
{
    sortFlag = "custom";
    for(int roundIndex = 0; roundIndex < count(); roundIndex++)
    {
        for(int itemIndex = 0; itemIndex < count()-1; itemIndex++)
        {
            Card *firstCard = (Card*)(this->itemWidget(item(itemIndex)));
            Card *secondCard = (Card*)(this->itemWidget(item(itemIndex+1)));
            int firstIndex = customList[firstCard];
            int secondIndex = customList[secondCard];
            if(firstIndex > secondIndex)
            {
                swapCards(itemIndex, itemIndex+1);
            }
        }
    }
}

void CardList::insertFromTo(int firstIndex, int secondIndex)
//Перемещает карточку в таблице из одной позиции на другую
{
    //Тут была проблема с тем, что нельзя взять из списка Айтем, не удалив Виджет, пришлось создавать временный Айтем
    if (firstIndex == secondIndex)
    {
        return;
    }
    QListWidgetItem *tempItem = new QListWidgetItem();
    if(secondIndex > firstIndex)
    {
        secondIndex += 1;
    }
    else
    {
        firstIndex +=1;
    }
    insertItem(secondIndex, tempItem);
    setItemWidget(tempItem, itemWidget(item(firstIndex)));
    tempItem->setSizeHint(item(firstIndex)->sizeHint());
    tempItem->setHidden(item(firstIndex)->isHidden());
    delete takeItem(firstIndex);
}

void CardList::swapCards(int firstIndex, int secondIndex)
//Меняет карточки по индексам местами
{
    if (firstIndex == secondIndex)
    {
        return;
    }
    if (secondIndex < firstIndex)
    {
        int tempIndex = firstIndex;
        firstIndex = secondIndex;
        secondIndex =tempIndex;
    }
    insertFromTo(firstIndex,secondIndex);
    insertFromTo(secondIndex-1, firstIndex);
}

void CardList::swapCards(Card *firstCard, Card *secondCard)
//Меняю карточки местами
{
    int firstIndex = row(getItem(firstCard));
    int secondIndex = row(getItem(secondCard));
    swapCards(firstIndex, secondIndex);
}

void CardList::setHideToTable(QListWidgetItem *item, bool value)
{
    Card *card = (Card*)itemWidget(item);
    card->setHideToTable(value);
}

void CardList::setHideToFilter(QListWidgetItem *item, bool value)
{
    Card *card = (Card*)itemWidget(item);
    card->setHideToFilter(value);
}

void CardList::checkHide(QListWidgetItem *item)
{
    Card *card = (Card*)itemWidget(item);
    if (card->isHideToTable() || card->isHideToFilter())
    {
        item->setHidden(true);
    }
    else
    {
        item->setHidden(false);
    }
}

QList<Teacher*> CardList::getTeacherList(Cell *cell)
{
    Lesson *lesson = cell->getLesson();
    return getTeacherList(lesson);
}

QList<Teacher*> CardList::getTeacherList(Lesson *lesson)
{
    QList<Teacher*> teacherList;
    Day *day = cardTable->getParentDay(lesson);
    for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
    {
       if (cell->getFirstCard() != nullptr)
       {
           teacherList.push_back(cell->getFirstCard()->getTeacher());
           if (cell->getSecondCard() != nullptr)
           {
               teacherList.push_back(cell->getSecondCard()->getTeacher());
           }
       }
    }
    return teacherList;
}

Card *CardList::getCard(Teacher *teacher, Discipline *discipline, Class *_class)
{
    for (int itemIndex = 0; itemIndex < count(); itemIndex++)
    {
        Card *card = (Card*)(this->itemWidget(item(itemIndex)));
        if (card->getTeacher() == teacher && card->getDiscipline() == discipline && card->getClass() == _class)
        {
            return card;
        }
    }
    return nullptr;
}

void CardList::setSort(QString sortName, bool pIsTtB)
{
    if (sortName == "class")
    {
        setClassSort(pIsTtB);
    }
    else if (sortName == "discipline")
    {
        setDisciplineSort(pIsTtB);
    }
    else if (sortName == "teacher")
    {
        setTeacherSort(pIsTtB);
    }
    else if (sortName == "custom")
    {
        setCustomSort();
    }
}

bool CardList::getIsTtB()
{
    return isTtB;
}

bool CardList::getIsFilterOn()
{
    return filterOn;
}

bool CardList::isCardVisible()
{
    return cardVisible;
}

void CardList::setCardVisible(bool value)
{
    cardVisible = value;
}

void CardList::setCardTable(CardTable *pCardTable)
{
    cardTable = pCardTable;
}

void CardList::dragEnterEvent(QDragEnterEvent *event)
//Переопределяю событие попадания курсора на ЛистБокс во время перетаскивания
{
    if (((event->mimeData()->hasFormat("application/widget/card") && getSort()=="custom")||event->mimeData()->hasFormat("application/widget/cell/firstCard")||
        event->mimeData()->hasFormat("application/widget/cell/secondCard")))
    {
        event->acceptProposedAction();
    }
}

void CardList::slot_showWithClass(Class *_class)
//Слот для метода
{
    showWithClass(_class);
}

void CardList::slot_showWithClass(Cell *cell)
//Слот для метода
{
    slot_showWithClass(cell->getClass());
}

void CardList::slot_showWithDay(Day *day)
//Слот для метода
{
    showWithDay(day);
}

void CardList::slot_showWithLesson(Lesson *lesson)
//Слот для метода
{
    showWithLesson(lesson);
}

void CardList::slot_showWithClassAndLesson(Cell *cell)
{
    showWithClassAndLesson(cell->getClass(), cell->getLesson());
}

void CardList::slot_showAll()
//Слот для метода
{
    showAll();
}

void CardList::slot_classSort(bool isTopToBottom)
//Слот для метода
{
    setClassSort(isTopToBottom);
}

void CardList::slot_teacherSort(bool isTopToBottom)
//Слот для метода
{
    setTeacherSort(isTopToBottom);
}

void CardList::slot_disciplineSort(bool isTopToBottom)
//Слот для метода
{
    setDisciplineSort(isTopToBottom);
}

void CardList::slot_customSort()
//Слот для метода
{
    setCustomSort();
}

void CardList::slot_updateSort()
{
    setSort(sortFlag, isTtB);
}

void CardList::mousePressEvent(QMouseEvent *event)
//Переопределяю событие нажатия мыши - высылаю сигнал (шобы фокус снимать с карточек при нажатии на список)
{
    emit mousePressed();
}

void CardList::dropEvent(QDropEvent *event)
//Переопредляю событие броска в ЛистБокс
{
    const WidgetMimeData* mimeData = dynamic_cast<const WidgetMimeData*> (event->mimeData());
    if(event->mimeData()->hasFormat("application/widget/card"))
    {
        if (itemAt(event->pos()) == nullptr)
        {
            return;
        }
        Card *cardDrop = (Card*)(itemWidget(itemAt(event->pos())));
        Card *cardDrag = (Card*)(mimeData->widget());
        swapCards(cardDrop, cardDrag);
        int tempIndex = customList[cardDrop];
        customList[cardDrop] = customList[cardDrag];
        customList[cardDrag] = tempIndex;
    }
    if(event->mimeData()->hasFormat("application/widget/cell/firstCard") || event->mimeData()->hasFormat("application/widget/cell/secondCard"))
    {
        Cell *cell = (Cell*)(mimeData->widget());
        bool isFromFirst = true;
        if(event->mimeData()->hasFormat("application/widget/cell/firstCard"))
        {
            cell->clearFirst(false);
            isFromFirst = true;
        }
        if (event->mimeData()->hasFormat("application/widget/cell/secondCard"))
        {
            cell->clearSecond();
            isFromFirst = false;
        }
    }
}
