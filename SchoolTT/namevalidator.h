#ifndef NAMEVALIDATOR_H
#define NAMEVALIDATOR_H

#include <QValidator>

class NameValidator : public QValidator
{
    Q_OBJECT
public:
    explicit NameValidator(QObject *parent = nullptr);

    // QValidator interface
public:
    State validate(QString &input, int &position) const;
};

#endif // NAMEVALIDATOR_H
