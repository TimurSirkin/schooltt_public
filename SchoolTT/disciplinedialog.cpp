#include "disciplinedialog.h"
#include <QGridLayout>
#include <QShortcut>
#include <QScrollBar>
#include <QPushButton>
#include <QLabel>
#include <QInputDialog>
#include <QLineEdit>
#include <QCheckBox>
#include <QPixmap>
#include <QString>
#include <QDebug>
#include <QMessageBox>
#include "styles.h"
#include "discipline.h"
#include "disciplineeditdialog.h"
#include "dialogfeatures.h"


//Диалоговое окно для добавления, удаления и редактирования предметов

DisciplineDialog::DisciplineDialog(Directory *_directory, QWidget *parent) : QDialog(parent)
  //Конструктор
{
    directory = _directory;
    setWindowTitle("Справочник предметов");
    setFixedWidth(250);
    QGridLayout *mainLayout = new QGridLayout();
    setLayout(mainLayout);
    mainLayout->addWidget(scrollArea,0,0);
    QWidget *wgt = new QWidget();
    scrollArea->setWidget(wgt);
    wgt->setLayout(scrollLayout);
    wgt->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    scrollArea->setWidgetResizable(true);
    scrollLayout->addWidget(new QLabel("<b>Наименование"));
    for (Discipline *discipline : directory->disciplineList)
    {
        addDiscipline(discipline);
    }
    QPushButton *addButton = new QPushButton("Добавить предмет");
    mainLayout->addWidget(addButton, 1, 0);
    connect(addButton, SIGNAL(clicked()), SLOT(createDiscipline()));

    new QShortcut(QKeySequence(Qt::Key_Insert), this, SLOT(createDiscipline()));
}

QWidget* DisciplineDialog::addDiscipline(Discipline *discipline)
//Создать строку предмета в списке предметов
{
    QList<QWidget*> wgtList;

    QString name = getStr(discipline->getName());
    QLabel* nameLabel= new QLabel(name);
    scrollLayout->addWidget(nameLabel, scrollLayout->rowCount(), 0, 1, 1, Qt::AlignLeft);
    wgtList.push_back(nameLabel);

    QPushButton* editButton = new QPushButton();
    QPushButton* deleteButton = new QPushButton();
    editButton->setStyleSheet(Styles::editButton());
    deleteButton->setStyleSheet(Styles::deleteButton());
    scrollLayout->addWidget(editButton, scrollLayout->rowCount()-1, 1);
    scrollLayout->addWidget(deleteButton, scrollLayout->rowCount()-1, 2);
    wgtList.push_back(editButton);
    wgtList.push_back(deleteButton);

    connect(editButton, &QPushButton::clicked, [=]() {editDiscipline(discipline, nameLabel);});
    connect(deleteButton, &QPushButton::clicked, [=]() {deleteDiscipline(discipline, wgtList);});

    DialogFeatures::sort(scrollLayout);
    return nameLabel;
}

QString DisciplineDialog::getStr(QString str)
{
    if (str.length() > 15)
    {
        str = str.left(13) + "...";
    }
    return str;
}

void DisciplineDialog::createDiscipline()
//Создать новый предмет
{
    DisciplineEditDialog *inputDialog = new DisciplineEditDialog(directory);

    //Проверка на непустоту названия
    if (inputDialog->exec() == QDialog::Accepted)
    {
        if (inputDialog->nameLine->text()=="")
        {
            return;
        }
        QString txt = inputDialog->nameLine->text();
        Discipline *newDiscipline = new Discipline(txt);
        directory->addDiscipline(newDiscipline);
        QWidget *wgt = addDiscipline(newDiscipline);

        if (height() < 500)
        {
            resize(width(), height()+25);
        }
        DialogFeatures::sort(scrollLayout);
        scrollArea->verticalScrollBar()->setMaximum(scrollArea->verticalScrollBar()->maximum()+25);
        DialogFeatures::scrollTo(wgt, scrollArea, scrollLayout);
    }
}

void DisciplineDialog::editDiscipline(Discipline *discipline, QLabel *nameLabel)
//Редактирование предмета
{
    DisciplineEditDialog *inputDialog = new DisciplineEditDialog(directory, discipline);
    inputDialog->nameLine->setText(discipline->getName());
    inputDialog->exportNameLine->setText(discipline->getExportName());
    inputDialog->exportForDoubleNameLine->setText(discipline->getExportForDoubleName());

    //Запускае окно редактирования предмета
    if (inputDialog->exec() == QDialog::Accepted)
    {
        discipline->setName(inputDialog->nameLine->text());
        discipline->setExportName(inputDialog->exportNameLine->text());
        discipline->setExportForDoubleName(inputDialog->exportForDoubleNameLine->text());
        nameLabel->setText(getStr(inputDialog->nameLine->text()));
    }
}

void DisciplineDialog::deleteDiscipline(Discipline *discipline, QList<QWidget*> wgtList)
//Удалить предмет
{
    //Список учителей, преподающих данный предмет
    QList<Teacher*> tempList;

    //Ищу таких учителей
    for (Teacher *teacher: directory->teacherList)
    {
        if (teacher->disciplineMap.contains(discipline))
        {
            tempList.push_back(teacher);
        }
    }

    //Если пустой - прошу подтверждения
    if (tempList.isEmpty())
    {
        QMessageBox *messageBox = new QMessageBox(QMessageBox::Question, "Удаление предмета", "Вы уверены, что хотите удалить предмет?");
        messageBox->addButton("Нет", QMessageBox::RejectRole);
        messageBox->addButton("Да", QMessageBox::AcceptRole);
        if (messageBox->exec() == QDialog::Accepted)
        {
            directory->disciplineList.removeOne(discipline);
            for(QWidget *wgt : wgtList)
            {
                delete wgt;
            }
            delete discipline;
        }
    }

    //Иначе показываю у каких учителей нужно удалить даны предмет
    else
    {
        QMessageBox warningBox(QMessageBox::Warning,
                           discipline->getName(),
                           "Предмет используется у следущих учителей:<br><br>",
                           nullptr);
        for(Teacher *teacher : tempList)
        {
            warningBox.setText(warningBox.text() + "<b>" + teacher->getShortName() + "</b>" + "<br>");
        }
        warningBox.setText(warningBox.text() + "<br>Прежде чем продолжить, уберите предмет у учителей");
        warningBox.addButton(QString::fromUtf8("Принять"),
                         QMessageBox::AcceptRole);
        warningBox.exec();
    }
}
