#ifndef TEACHERLISTDIALOG_H
#define TEACHERLISTDIALOG_H

#include <QDialog>
#include <QList>
#include <QCheckBox>
#include "teacher.h"
#include "directory.h"
#include "cardtable.h"

class TeacherListDialog : public QDialog
{
    Q_OBJECT
public:
    explicit TeacherListDialog(CardTable *pCardTable, QWidget *parent = nullptr);
    QList<Teacher *> getTeacherList();

private:
    Directory *directory;
    CardTable *cardTable;
    void addTeacher(Teacher *teacher);
    QGridLayout *scrollLayout = new QGridLayout();
    QList<Teacher *> teacherList;
    QCheckBox *mainBox = new QCheckBox("Добавить/убрать всех");
};

#endif // TEACHERLISTDIALOG_H
