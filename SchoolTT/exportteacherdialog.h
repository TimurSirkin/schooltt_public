#ifndef EXPORTTEACHERDIALOG_H
#define EXPORTTEACHERDIALOG_H

#include <QDialog>
class ExportOptions;

namespace Ui {
class ExportTeacherDialog;
}

class ExportTeacherDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ExportTeacherDialog(QWidget *parent = nullptr);
    ~ExportTeacherDialog();

    int getTeacherHeight();
    int getTeacherWidth();
    int getTeacherFontSize();
    bool getTeacherCursive();
    bool getTeacherBold();

    int getHeaderHeight();
    int getHeaderWidth();
    int getHeaderFontSize();
    bool getHeaderCursive();
    bool getHeaderBold();

    int getClassHeight();
    int getClassWidth();
    int getClassFontSize();
    bool getClassCursive();
    bool getClassBold();

    int getDayHeight();
    int getDayWidth();
    int getDayFontSize();
    bool getDayCursive();
    bool getDayBold();

    int getLessonHeight();
    int getLessonWidth();
    int getLessonFontSize();
    bool getLessonCursive();
    bool getLessonBold();

    void initDialog();
    void setOptions(ExportOptions *exportOptions);

private slots:
    void on_exportButton_clicked();

    void on_teacherWidth_valueChanged(int arg1);

    void on_dayHeight_valueChanged(int arg1);

    void on_lessonHeight_valueChanged(int arg1);

    void on_classHeight_valueChanged(int arg1);

    void on_classWidth_valueChanged(int arg1);

private:
    Ui::ExportTeacherDialog *ui;
};

#endif // EXPORTTEACHERDIALOG_H
