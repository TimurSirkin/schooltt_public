#ifndef DAY_H
#define DAY_H

#include <QVector>
#include <QLabel>
#include <QPushButton>
#include "lesson.h"
#include "tableelement.h"


class Day : public TableElement
{
public:
    Day(QString pText = "День", QWidget *parent = nullptr);

    QVector<Lesson*> lessonList;
    QVector<QVector <Cell*>> cellMatrix;


private:
    QLabel *textLabel = new QLabel();
    QPushButton *addButton = new QPushButton();
    QPushButton *deleteButton = new QPushButton();

    QString text;

    QAction *action_clear = new QAction("Убрать карточки со всех уроков");

    // QWidget interface
protected:
    void paintEvent(QPaintEvent *event);

    // TableElement interface
public:
    QString getName();

    // QWidget interface
protected:
    void contextMenuEvent(QContextMenuEvent *event);
};

#endif // DAY_H
