#include "widgetmimedata.h"
#include <QDebug>
#include "card.h"
#include "cell.h"
#include "tableelement.h"

//Класс ддля хранения ссылки на виджет при перетаскивании

WidgetMimeData::WidgetMimeData() :QMimeData()
  //Конструктор
{

}

QString WidgetMimeData::getType()
//Возвращает тип в виде строки (когда произойдет drop, буду запрашивать тип, чтобы узнать, как реагировать)
{
    //Смотрю явлется ли объект карточкой
    if (qobject_cast<Card*>(pointer) != nullptr)
    {
        return "application/widget/card";
    }

    if (dynamic_cast<Class*>(pointer) != nullptr)
    {
        return "application/widget/class";
    }

    //Смотрю явлется ли объект эдементом таблицы
    TableElement *tableElement = qobject_cast<TableElement*>(pointer);
    if (tableElement != nullptr)
    {
        //Смотрю явлется ли объект ячейкой
        if ((Cell*)(tableElement) != nullptr)
        {
            if (flag == "none")
            {
                return "application/widget/cell";
            }
            else
            {
                return ("application/widget/cell/" + flag);
            }

        }
    }
}

void WidgetMimeData::setWidget(QWidget *value, QString pFlag)
//Сеттер для ссылки на виджет
{
    flag = pFlag;
    pointer = value;
    setData(getType(), QByteArray() );
}

QWidget *WidgetMimeData::widget() const
//Геттер ссылки
{
    return pointer;
}
