#ifndef EXPORTOPTIONS_H
#define EXPORTOPTIONS_H

#include <QObject>
#include "exportdialog.h"
#include "exportteacherdialog.h"

class ExportOptions : public QObject
{
    Q_OBJECT
public:
    explicit ExportOptions(QObject *parent = nullptr);

    int disciplineHeight = 20;
    int disciplineWidth = 16;
    int disciplineFontSize = 8;
    bool disciplineCursive = true;
    bool disciplineBold = false;

    int roomWidth = 4;
    int roomFontSize = 8;
    bool roomCursive = true;
    bool roomBold = false;

    int classHeight = 16;
    int classFontSize = 12;
    bool classCursive = false;
    bool classBold = true;

    int dayWidth = 4;
    int dayFontSize = 10;
    bool dayCursive = false;
    bool dayBold = true;

    int lessonWidth = 3;
    int lessonFontSize = 10;
    bool lessonCursive = false;
    bool lessonBold = true;

    // Options for teacher export

    int t_classHeight = 16;
    int t_classWidth = 4;
    int t_classFontSize = 10;
    bool t_classCursive = false;
    bool t_classBold = false;

    int t_teacherHeight = 10;
    int t_teacherWidth = 20;
    int t_teacherFontSize = 10;
    bool t_teacherCursive = false;
    bool t_teacherBold = false;

    int t_headerHeight = 10;
    int t_headerWidth = 10;
    int t_headerFontSize = 10;
    bool t_headerCursive = false;
    bool t_headerBold = true;

    int t_dayHeight = 18;
    int t_dayWidth = 10;
    int t_dayFontSize = 10;
    bool t_dayCursive = false;
    bool t_dayBold = true;

    int t_lessonHeight = 18;
    int t_lessonWidth = 10;
    int t_lessonFontSize = 10;
    bool t_lessonCursive = false;
    bool t_lessonBold = true;

    QString topLeftText = "";
    QString topCentralText = "";
    QString topRightText = "";
    QString botLeftText = "";
    QString botCentralText = "";
    QString botRightText = "";

    void getOptions(ExportDialog *exportDialog);
    void getOptions(ExportTeacherDialog *exportDialog);

signals:

public slots:
};

#endif // EXPORTOPTIONS_H
