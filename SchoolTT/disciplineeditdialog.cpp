#include "disciplineeditdialog.h"
#include <QDialog>
#include <QMessageBox>
#include <QLabel>
#include <QCheckBox>
#include <QGridLayout>
#include <QLineEdit>
#include <QStyle>
#include "styles.h"
#include "namevalidator.h"


//Диалоговое окно редактирования предмета

DisciplineEditDialog::DisciplineEditDialog(Directory *pDirectory, Discipline *pDiscipline, QWidget *parent) : QDialog(parent)
  //Конструктор
{
    discipline = pDiscipline;
    directory = pDirectory;

    this->setWindowTitle("Редактирование предмета");
    okButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogOkButton));
    okButton->setStyleSheet(Styles::answerButtons());
    cancelButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogCancelButton));
    cancelButton->setStyleSheet(Styles::answerButtons());
    connect(okButton, &QPushButton::clicked, [=] () {checkName();});
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    newLayout->addWidget(namelabel, newLayout->rowCount(), 0, 1, 2);
    newLayout->addWidget(nameLine, newLayout->rowCount(), 0, 1, 2);

    QFrame *line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    newLayout->addWidget(line, newLayout->rowCount(), 0, 1, 2);
    newLayout->addWidget(new QLabel("Настройка наименования при экспорте в xlsx"), newLayout->rowCount(), 0, 1, 2, Qt::AlignCenter);

    QFrame *frame = new QFrame();
    frame->setLineWidth(1);
    frame->setFrameShape(QFrame::StyledPanel);
    QGridLayout *frameLayout = new QGridLayout();
    frame->setLayout(frameLayout);
    newLayout->addWidget(frame, newLayout->rowCount(), 0, 1, 2);

    frameLayout->addWidget(exportNameLabel, frameLayout->rowCount(), 0);
    frameLayout->addWidget(exportNameLine, frameLayout->rowCount(), 0);
    frameLayout->addWidget(exportForDoubleNameLabel, frameLayout->rowCount(), 0);
    frameLayout->addWidget(exportForDoubleNameLine, frameLayout->rowCount(), 0);

    nameLine->setValidator(new NameValidator(this)); 
    newLayout->addWidget(okButton,  newLayout->rowCount(), 0, 1, 1, Qt::AlignLeft);
    newLayout->addWidget(cancelButton,  newLayout->rowCount()-1, 1, 1, 1, Qt::AlignRight);

    this->setLayout(newLayout);

    nameLine->setFocus();
    setFixedHeight(sizeHint().height());
    setFixedWidth(sizeHint().width());
}

void DisciplineEditDialog::checkName()
{
    if (discipline != nullptr && nameLine->text() == discipline->getName())
    {
        accept();
        return;
    }

    QString str;
    if ( discipline != nullptr)
    {
        str = discipline->getName();
    }
    else
    {
        str = "Новый предмет";
    }

    if (nameLine->text() == "")
    {
        QMessageBox warningBox(QMessageBox::Warning,
                           str,
                           QString::fromUtf8("Наименование не может быть пустым"),
                           nullptr);
        warningBox.exec();
        return;
    }

    for(Discipline *currentDiscipline : directory->disciplineList)
    {
        if(nameLine->text() == currentDiscipline->getName())
        {
            QMessageBox warningBox(QMessageBox::Warning,
                               nameLine->text(),
                               QString::fromUtf8("Предмет с таким наименованием уже присутствует"),
                               nullptr);
            warningBox.exec();
            return;
        }
    }

    accept();
}
