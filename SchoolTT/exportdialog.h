#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>
class ExportOptions;

namespace Ui {
class ExportDialog;
}

class ExportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ExportDialog(QWidget *parent = 0);
    ~ExportDialog();

    int getDisciplineHeight();
    int getDisciplineWidth();
    int getDisciplineFontSize();
    bool getDisciplineCursive();
    bool getDisciplineBold();

    int getRoomHeight();
    int getRoomWidth();
    int getRoomFontSize();
    bool getRoomCursive();
    bool getRoomBold();

    int getClassHeight();
    int getClassWidth();
    int getClassFontSize();
    bool getClassCursive();
    bool getClassBold();

    int getDayHeight();
    int getDayWidth();
    int getDayFontSize();
    bool getDayCursive();
    bool getDayBold();

    int getLessonHeight();
    int getLessonWidth();
    int getLessonFontSize();
    bool getLessonCursive();
    bool getLessonBold();

    QString getLeftTopText();
    QString getCentralTopText();
    QString getRightTopText();
    QString getLeftBotText();
    QString getCentralBotText();
    QString getRightBotText();

    void initDialog();
    void setOptions(ExportOptions *exportOptions);

private slots:
    void on_heightSpinBox1_valueChanged(int value);

    void on_widthSpinBox1_valueChanged(int value);

    void on_widthSpinBox2_valueChanged(int value);

    void on_exportButton_clicked();

private:
    Ui::ExportDialog *ui;


};

#endif // EXPORTDIALOG_H
