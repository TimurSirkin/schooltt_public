#ifndef FILTEREDITDIALOG_H
#define FILTEREDITDIALOG_H

#include <QDialog>
#include <QList>
#include "class.h"
#include "teacher.h"
#include "discipline.h"
#include "directory.h"
#include "cardlist.h"

class FilterEditDialog : public QDialog
{
    Q_OBJECT
public:
    explicit FilterEditDialog(Directory *pDirectory, CardList *pCardList, CardTable *pCardTable, QWidget *parent = nullptr);

    CardList *cardList;
    Directory *directory;
    CardTable *cardTable;

    QList <Teacher*> teacherFilterList;
    QList <Class*> classFilterList;
    QList <Discipline*> disciplineFilterList;
    bool isCardVisible();

private:
    bool cardVisible = true;

signals:

public slots:

private slots:
    void slot_changeTeacherList(Teacher *teacher, bool isNeedToPush);
    void slot_changeDisciplineList(Discipline *discipline, bool isNeedToPush);
    void slot_changeClassList(Class *_class, bool isNeedToPush);

};

#endif // FILTEREDITDIALOG_H
