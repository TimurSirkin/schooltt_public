#include "discipline.h"

//Предмет

Discipline::Discipline(QString _name, QObject *parent) : QObject(parent)
  //Конструктор
{
    setName(_name);
}

QString Discipline::getName() const
//Геттер названия
{
    return name;
}

QString Discipline::getExportName() const
{
    return exportName;
}

QString Discipline::getExportForDoubleName() const
{
    return exportForDoubleName;
}

void Discipline::setName(QString value)
//Сеттер названия
{
    name = value;
    nameChanged();
}

void Discipline::setExportName(QString value)
{
    exportName = value;
}

void Discipline::setExportForDoubleName(QString value)
{
    exportForDoubleName = value;
}
