#ifndef NEWFILEBYDIALOG_H
#define NEWFILEBYDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QPushButton>

namespace Ui {
class NewFileByDialog;
}

class NewFileByDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewFileByDialog(QWidget *parent = 0);
    ~NewFileByDialog();
    QString fileName;

    QLineEdit *nameLine;
    QLineEdit *pathLine;

private:
    Ui::NewFileByDialog *ui;

private slots:
    void slot_path();
    void slot_create();
};

#endif // NEWFILEBYDIALOG_H
