#ifndef ROOM_H
#define ROOM_H

#include <QString>
#include <QList>
#include "discipline.h"

class Room
{
public:
    Room();
    Room(QString _number, int _capacity);

    QString number;
    int capacity;
};

#endif // ROOM_H
