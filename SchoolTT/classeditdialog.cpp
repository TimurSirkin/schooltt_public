#include "classeditdialog.h"
#include <QDialog>
#include <QStyle>
#include <QMessageBox>
#include <QDebug>
#include "namevalidator.h"

//Диалоговое окно редактироваия класса
ClassEditDialog::ClassEditDialog(QVector<Class *> pClassVector, Class *pClass, QWidget *parent) : QDialog(parent)
  //Конструктор окна
{
    _class = pClass;
    classVector = pClassVector;
    this->setWindowTitle("Редактирование класса");
    okButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogOkButton));
    cancelButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogCancelButton));
    connect(okButton, &QPushButton::clicked, [=](){checkName();});
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    newLayout->addWidget(nameLabel, 0, 0, 1, 2);
    newLayout->addWidget(nameLine, 1,0, 1, 2);
    nameLine->setValidator(new NameValidator(this));
    newLayout->addWidget(sizeLabel, 2, 0, 1, 2);
    newLayout->addWidget(sizeBox,3,0,1,2);
    newLayout->addWidget(okButton,4,0);
    newLayout->addWidget(cancelButton,4,1);
    this->setLayout(newLayout);

    nameLine->setFocus();
    setFixedSize(sizeHint());
    setFixedWidth(250);
}

void ClassEditDialog::checkName()
{
    if (_class != nullptr && (nameLine->text() == _class->getName()))
    {
        accept();
        return;
    }


    QString str;
    if (_class != nullptr)
    {
        str = _class->getName();
    }
    else
    {
        str = "Новый класс";
    }

    if (nameLine->text() == "")
    {
        QMessageBox warningBox(QMessageBox::Warning,
                           str,
                           QString::fromUtf8("Наименование не может быть пустым"),
                           nullptr);
        warningBox.exec();
        return;
    }

    for(Class *currentClass : classVector)
    {
        if(currentClass->getName() == nameLine->text())
        {
            QMessageBox warningBox(QMessageBox::Warning,
                               nameLine->text(),
                               QString::fromUtf8("Класс с таким наименованием уже присутствует"),
                               nullptr);
            warningBox.exec();
            return;
        }
    }

    accept();
}
