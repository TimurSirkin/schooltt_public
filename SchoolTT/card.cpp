#include "card.h"
#include <QString>
#include <QFont>
#include <QGraphicsDropShadowEffect>
#include <QDebug>
#include <QPalette>
#include <QDrag>
#include <QDebug>
#include <QMimeData>
#include <QApplication>
#include <widgetmimedata.h>
#include "cardeditdialog.h"
#include "directory.h"
#include "cardlist.h"
#include "cardtable.h"

Card::Card(QWidget *parent) : QWidget(parent)
  //Конструкток карточки
{
    updateDisColor();

    QFont currentFont;
    currentFont.setPointSize(9);
    topLabel->setFont(currentFont);
    topLabel->setAlignment(Qt::AlignCenter);
    currentFont.setPointSize(7);
    botLabel->setFont(currentFont);
    botLabel->setAlignment(Qt::AlignCenter);
    currentFont.setPointSize(countFontSize);
    countLabel->setFont(currentFont);
    countLabel->setAlignment(Qt::AlignCenter);
    setLayout(mainLayout);

    mainLayout->addWidget(countLabel, 0, 1, 2, 1, Qt::AlignRight);
    mainLayout->addWidget(topLabel, 0, 0, 1, 1, Qt::AlignLeft);
    mainLayout->addWidget(botLabel, 1, 0, 1, 1, Qt::AlignLeft);

}

Card::Card(Teacher *pTeacher, Class *pClass, Discipline *pDiscipline, int pCount, CardTable *pCardTable, Directory *pDirectory, CardList *pCardList, QWidget *pParent): Card(pParent)
  //Конструктор, заполняющий все поля карточки, вызывает дефолтный конструктор
{
    topFlag = pCardTable->getTopFlag();
    botFlag = pCardTable->getBotFlag();
    tipFlag = pCardTable->getTipFlag();

    botFlagConnection = connect(pCardTable, &CardTable::signal_botFlagChanged, [=](QString flag){setBotFlag(flag); update();});
    topFlagConnection = connect(pCardTable, &CardTable::signal_topFlagChanged, [=](QString flag){setTopFlag(flag); update();});
    tipFlagConnection = connect(pCardTable, &CardTable::signal_tipFlagChanged, [=](QString flag){setTipFlag(flag); update();});

    setTeacher(pTeacher);
    setClass(pClass);
    setDiscipline(pDiscipline);
    cardTable = pCardTable;
    directory = pDirectory;
    cardList = pCardList;

    backgroundColor = pTeacher->getBackgroundColor();
    setBackgroundColor(backgroundColor);
    setForegroundColor(foregroundColor);
    setPalette(palette);
    setAutoFillBackground(true);


    setMaxCount(pCount);
    if(currentCount == 0)
    {
        setEmpty(true);
    }
    else
    {
        setEmpty(false);
    }
}

Card::~Card()
{
    disconnect(topFlagConnection);
    disconnect(botFlagConnection);
    disconnect(tipFlagConnection);
}

void Card::setTeacher(Teacher *value)
//Сеттер учителя
{
    teacher = value;
    QString name1 = getStr(teacher->getSecondName());
    QString name2 = getStr(teacher->getFirstName());
    QString name3 = getStr(teacher->getThirdName());
    if (topFlag == "name")
    {
        topLabel->setText(getStr(teacher->getShortName()));
    }
    if (botFlag == "name")
    {
        botLabel->setText(getStr(teacher->getShortName()));
    }
    if (tipFlag == "name")
    {
        toolTipStr = name1 + "\n" + name2 + "\n" + name3;
        setToolTip(toolTipStr);
    }
}

void Card::setClass(Class *const value)
//Сеттер класса
{
    _class = value;
    if (topFlag == "class")
    {
        topLabel->setText(getStr(_class->getName()));
    }
    if (botFlag == "class")
    {
        botLabel->setText(getStr(_class->getName()));
    }
    if (tipFlag == "class")
    {
        toolTipStr = value->getName();
        setToolTip(toolTipStr);
    }
}

void Card::setDiscipline(Discipline * const value)
//Сеттер предмета
{
    discipline = value;
    if (topFlag == "dis")
    {
        topLabel->setText(getStr(discipline->getName()));
    }
    if (botFlag == "dis")
    {
        botLabel->setText(getStr(discipline->getName()));
    }
    if (tipFlag == "dis")
    {
        toolTipStr = value->getName();
        setToolTip(toolTipStr);
    }
}

void Card::setMaxCount(int value)
//Сеттер максимально кол-ва карточек
{
    setCurrentCount(currentCount + (value - maxCount) );//Изменяю текущее кол-во на вел-ну изменения максимального кол-ва
    if (currentCount == 0)
    {
        setEmpty(true);
    }
    else
    {
        setEmpty(false);
    }
    maxCount = value;
}

void Card::setCurrentCount(int value)
//Сеттер текущего кол-ва карточек
{
    currentCount = value;
    countLabel->setText(QString::number(value));
    if(cardList != nullptr && cardList->getIsFilterOn())
    {
        cardList->onFilter();
    }
}

void Card::setBackgroundColor(QColor value)
//Сеттер цвета фона
{
    palette.setColor(QPalette::Base, value);
    setPalette(palette);
    updateDisColor();
    emit colorChanged();
}

void Card::setBColor(QColor value)
{
    backgroundColor = value;
    setBackgroundColor(value);
}

void Card::setFColor(QColor value)
{
    foregroundColor = value;
    setForegroundColor(value);
}

void Card::setForegroundColor(QColor value)
//Сеттер цвета надписей
{
    palette.setColor(QPalette::Text, value);
    setPalette(palette);
    updateDisColor();
    emit colorChanged();
}

QColor Card::getBackgroundColor() const
//Геттер цвета фона
{
    return palette.color(backgroundRole());
}

QColor Card::getForegroundColor() const
//Геттер цвета надписей
{
    return palette.color(foregroundRole());
}

Teacher *Card::getTeacher() const
//Геттер учителя
{
    return teacher;
}

Class *Card::getClass() const
//Геттер класса
{
    return _class;
}

Discipline *Card::getDiscipline() const
//Геттер предмета
{
    return discipline;
}

int Card::getMaxCount() const
//Геттер кол-ва
{
    return maxCount;
}

int Card::getCurrentCount() const
//Геттер текущего кол-ва
{
    return currentCount;
}

QPalette Card::getEnabledPalette() const
//Геттер политры выключенного стиля
{
    QPalette palette;
    QColor newColorB = backgroundColor;
    newColorB.setAlpha(newColorB.alpha()*0.5);
    QColor newColorF = foregroundColor;
    newColorF.setAlpha(newColorF.alpha()*0.5);
    palette.setColor(QPalette::Background, newColorB);
    palette.setColor(QPalette::Foreground, newColorF);
    return palette;
}

void Card::take()
//Забрать одну карточку
{
    setCurrentCount(currentCount-1);
    if(currentCount == 0)
    {
        setEmpty(true);
    }
}

void Card::add()
//Вернуть одну карточку
{
    setCurrentCount(currentCount+1);
    if(currentCount > 0)
    {
        setEmpty(false);
    }
}

void Card::edit()
//Вызывает окно редактирования карточки
{
    CardEditDialog* inputDialog = new CardEditDialog(this, cardTable, directory, cardList);
    if (inputDialog->exec() == QDialog::Accepted)
    {
        backgroundColor = inputDialog->backgroundColor;
        foregroundColor = inputDialog->foregroundColor;
        setBackgroundColor(backgroundColor);
        setForegroundColor(foregroundColor);
    }
}

void Card::setEnabledStyle()
{
    setBackgroundColor(dis_backgroundColor);
    setForegroundColor(dis_foregroundColor);
    repaint();
}

void Card::restoreStyle()
{
    setBackgroundColor(backgroundColor);
    setForegroundColor(foregroundColor);
    repaint();
}

QWidget *Card::cloneWidget(QWidget *parent)
{
    QWidget *cloneWidget = new QWidget(parent);
    QPalette clonePaltte = QPalette();
    clonePaltte.setColor(QPalette::Background, getBackgroundColor());
    clonePaltte.setColor(QPalette::Foreground, getForegroundColor());
    cloneWidget->setPalette(clonePaltte);

    QGridLayout *cloneLayout = new QGridLayout();
    cloneWidget->setLayout(cloneLayout);
    QFont cloneFont;

    cloneFont.setPointSize(8);    
    QLabel *topLbl = new QLabel(topLabel->text());
    topLbl->setFont(QFont(cloneFont));
    cloneLayout->addWidget(topLbl, 0, 0, 1, 1, Qt::AlignCenter);

    QLabel *botLbl = new QLabel(botLabel->text());
    cloneFont.setPointSize(6);
    botLbl->setFont(cloneFont);
    cloneLayout->addWidget(botLbl, 1, 0, 1, 1, Qt::AlignCenter);

    QLabel *countLbl = new QLabel(QString::number(getMaxCount()));
    cloneFont.setPointSize(10);
    countLbl->setFont(cloneFont);
    cloneLayout->addWidget(countLbl, 0, 1, 2, 1, Qt::AlignCenter);

    cloneWidget->setAutoFillBackground(true);
    cloneWidget->setFixedHeight(height());
    //cloneWidget->setFixedWidth(width());

    return cloneWidget;
}

bool Card::isEmpty()
{
    return empty;
}

void Card::setEmpty(bool value)
{
    empty = value;
}

void Card::setTopFlag(const QString &flag)
{
    topFlag = flag;
}

void Card::setBotFlag(const QString &flag)
{
    botFlag = flag;
}

void Card::setTipFlag(const QString &flag)
{
    tipFlag = flag;
}

QString Card::getTopFlag()
{
    return topFlag;
}

QString Card::getBotFlag()
{
    return botFlag;
}

QString Card::getTipFlag()
{
    return tipFlag;
}

void Card::setHideToFilter(bool value)
{
    hideToFilter = value;
}

void Card::setHideToTable(bool value)
{
    hideToTable = value;

    if (!isTransparentToTable() && hideToTable)
    {
        setTransparentToTable(true);
    }
    if(!hideToTable)
    {
        setTransparentToTable(false);
    }
}

void Card::setTransparentToTable(bool value)
{
    transparentToTable = value;
    if (transparentToTable)
    {
        setEnabledStyle();
    }
    else
    {
        if(!hideToTable)
        {
            restoreStyle();
        }
    }
}

bool Card::isHideToFilter()
{
    return hideToFilter;
}

bool Card::isHideToTable()
{
    return hideToTable;
}

bool Card::isTransparentToTable()
{
    return transparentToTable;
}

void Card::edit(Teacher *pTeacher, Class *pClass, Discipline *pDiscipline, int pCount)
//Изменяет поля карточки, в зависимоти от параметров
{
    setTeacher(pTeacher);
    setClass(pClass);
    setDiscipline(pDiscipline);
    setMaxCount(pCount);
}

QString Card::getStr(QString str)
//Обрезает слишком длинны строки
{
    if (str.length() > 14)
    {
        str = str.left(11) + "...";
    }
    return str;
}

void Card::updateDisColor()
{
    dis_backgroundColor = backgroundColor;
    dis_backgroundColor.setAlpha(dis_backgroundColor.alpha()*0.5);
    dis_foregroundColor = foregroundColor;
    dis_foregroundColor.setAlpha(dis_foregroundColor.alpha()*0.5);
}

void Card::update()
{
    setTeacher(teacher);
    setClass(_class);
    setDiscipline(discipline);
}

void Card::checkDiscipline(Discipline *pDiscipline)
//Связан с сигналом изменения дисциплины учителя, проверяет, нужно ли удаляться
{
    if (pDiscipline == discipline)
    {
        emit deleteRequest();
    }
}

void Card::checkClass(Discipline *pDiscipline, Class *pClass)
//Связан с сигналом изменения класса учителя, проверяет, нужно ли удаляться
{
    if (pClass == _class && pDiscipline == discipline)
    {
        emit deleteRequest();
    }
}

void Card::checkCount(Discipline *pDiscipline, Class *pClass, int pCount)
//Связан с сигналом изменения кол-ва уроков у учителя, проверяет, нужно ли изменяться
{
    if (pClass == _class && pDiscipline == discipline)
    {
        setMaxCount(pCount);
    }
}

void Card::mouseDoubleClickEvent(QMouseEvent *event)
//Переопределяю событие даблклика
{
    edit();
}

void Card::mousePressEvent(QMouseEvent *event)
//Переопределя. событие нажатия на кнопку мыши
{
    emit mousePressed();
    //Если это левая кнопка мыши - запоминаю точку, где нажал (нужно для того, чтобы разделить нажатие по карточки и перетаскивание)
    if (event->button() == Qt::LeftButton)
             dragStartPosition = event->pos();
}

void Card::mouseMoveEvent(QMouseEvent *event)
//Событие движения мыши над карточкой(Нужен для Drag and Drop)
{
    //Если не зажата левая кнопка мыши - return
    if (!(event->buttons() & Qt::LeftButton))
    {
        return;
    }
    //Смотрю чтобы мышь продвинулась на расстояние startDragDisctance, иначе - return
    if ((event->pos() - dragStartPosition).manhattanLength() < QApplication::startDragDistance())
    {
        return;
    }

    //Задаю параметры перетаскивания
    QDrag *drag = new QDrag(this);

    //Создаю перемунную, которая сможет хранить ссылку на виджет
    WidgetMimeData* mimeData = new WidgetMimeData;

    //Тут, собственно, пихаю туда ссыль
    mimeData->setWidget(this);

    drag->setMimeData(mimeData);
    QPixmap *pix = new QPixmap(20, 20);
    pix->fill(getBackgroundColor());
    drag->setPixmap(*pix);
    emit dragStarted();
    drag->exec();
    emit dragStoped();
}


