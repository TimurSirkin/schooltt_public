#ifndef ROOMEDITDIALOG_H
#define ROOMEDITDIALOG_H

#include <QWidget>
#include <QDialog>
#include <QLineEdit>
#include <QCheckBox>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QPushButton>
#include <QListWidget>
#include "room.h"
#include "directory.h"

class RoomEditDialog : public QDialog
{
    Q_OBJECT
public:
    explicit RoomEditDialog(QWidget *parent = nullptr);

    QLineEdit *numberLine = new QLineEdit();
    QSpinBox *capacityBox = new QSpinBox();
    QGridLayout *newLayout = new QGridLayout();
    QLabel *numberLabel = new QLabel("Название:");
    QLabel *capacityLabel = new QLabel("Вместимость:");
    QPushButton *okButton = new QPushButton("Принять");
    QPushButton *cancelButton = new QPushButton("Отмена");

};

#endif // ROOMEDITDIALOG_H
