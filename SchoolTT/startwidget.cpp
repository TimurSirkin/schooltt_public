#include "startwidget.h"
#include <QLabel>
#include <QLayout>
#include <QPushButton>

StartWidget::StartWidget(MainWindow *parent) : QDialog(parent)
{
    setWindowTitle(" ");
    setFixedSize(500,100);

    QGridLayout *mainLayout = new QGridLayout();
    setLayout(mainLayout);
    QLabel *wellcomeLabel = new QLabel();
    QString str = "";
    str +=  "Добро пожаловать в <b>SchoolTT<b><br>";
    wellcomeLabel->setText(str);

    QPushButton *createButton = new QPushButton("Создать новое расписание");
    QPushButton *openButton = new QPushButton("Открыть существующее");
    connect(createButton, &QPushButton::clicked, [=](){parent->slot_newFile(); close();});
    connect(openButton, &QPushButton::clicked, [=](){parent->slot_openFile(); close();});

    mainLayout->addWidget(wellcomeLabel, 0, 0, 1, 2, Qt::AlignCenter);
    mainLayout->addWidget(createButton, 1, 0, 1, 1);
    mainLayout->addWidget(openButton, 1, 1, 1, 1);
}
