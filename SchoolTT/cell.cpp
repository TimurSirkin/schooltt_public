#include "cell.h"
#include <QFont>
#include <QPainter>
#include <QPen>
#include <QDrag>
#include <QMimeData>
#include <QApplication>
#include <QMouseEvent>
#include <QInputDialog>
#include <QMenu>
#include <QToolTip>
#include <QWidget>
#include <QMessageBox>
#include <QDebug>
#include "styles.h"
#include "card.h"
#include "teacher.h"
#include "class.h"
#include "lesson.h"
#include "discipline.h"
#include "cardtable.h"
#include "widgetmimedata.h"

//Ячейка

Cell::Cell(CardTable *parent):TableElement(parent)
  //Конструктор
{
    setMouseTracking(true);
    cardTable = parent;

    connect(cardTable, &CardTable::signal_repaintCell, [=](){repaint();});
    connect(this, SIGNAL(dragStarted(Card*,bool)), cardTable, SLOT(slot_cellDragStarted(Card*,bool)));

    setAcceptDrops(true);
    _foregroundColor = _backgroundColor;
    _foregroundColor.setAlpha(200);
    //setStyleSheet(Styles::cell());

    defaultHeight = 30;
    defaultWidth = 50;

    clearFirstAction->setText("Убрать карточку");
    clearFirstAction->setEnabled(false);
    connect(clearFirstAction, &QAction::triggered, [this] () {clearFirst(false);});
    clearSecondAction->setText("Убрать карточку");
    clearSecondAction->setEnabled(false);
    connect(clearSecondAction, &QAction::triggered, this, &Cell::clearSecond);

    //raise();
}

void Cell::setFirstCard(Card *value)
//Сеттер первой карточки
{
    //Если пытаюсь присвоить туже карточку или пустое значение - ничего не делаю
    if(firstCard == value || value == nullptr)
    {
        clearFirstAction->setEnabled(true);
        return;
    }

    //Очищаю от предыдыщей карточки
    clearFirst();

    //Присваиваю новыую карточку ячейке
    firstCard = value;
    firstCard->take();
    connect(firstCard->getTeacher(), SIGNAL(nameChanged()), this, SLOT(repaint()));
    connect(firstCard->getDiscipline(), SIGNAL(nameChanged()), this, SLOT(repaint()));
    connect(firstCard->getClass(), SIGNAL(nameChanged()), this, SLOT(repaint()));
    connect(firstCard, SIGNAL(colorChanged()), this, SLOT(repaint()));
    repaint();

    clearFirstAction->setEnabled(true);
}

void Cell::setSecondCard(Card *value )
//Сеттер второй карточки
{
    //Если пытаюсь присвоить туже карточку или пустое значение - ничего не делаю
    if(secondCard == value || value == nullptr)
    {
        return;
    }

    //Очищаю от предыдыщей карточки
    clearSecond();

    //Присваиваю новыую карточку ячейке
    secondCard = value;
    secondCard->take();
    connect(secondCard->getTeacher(), SIGNAL(nameChanged()), this, SLOT(repaint()));
    connect(secondCard->getDiscipline(), SIGNAL(nameChanged()), this, SLOT(repaint()));
    connect(secondCard->getClass(), SIGNAL(nameChanged()), this, SLOT(repaint()));
    connect(secondCard, SIGNAL(colorChanged()), this, SLOT(repaint()));
    repaint();

    clearSecondAction->setEnabled(true);
}

void Cell::setClass(Class *pClass)
{
    _class = pClass;
    setName(_class->getName());
    repaint();
}

void Cell::setLesson(Lesson *pLesson)
{
    lesson = pLesson;
}

Card *Cell::getFirstCard()
//Геттер первой карточки
{
    return firstCard;
}

Card *Cell::getSecondCard()
//Геттер второй карточки
{
    return secondCard;
}

Card *Cell::getCard(bool isFirst)
{
    if (isFirst)
    {
        return getFirstCard();
    }
    else
    {
        return getSecondCard();
    }
}

Class *Cell::getClass()
{
    return _class;
}

Lesson *Cell::getLesson()
{
    return lesson;
}

bool Cell::isEmpty()
{
    return (secondCard == nullptr && firstCard == nullptr);
}

int Cell::getCardCount()
{
    int count = 0;
    if (firstCard != nullptr)
    {
        count++;
        if (secondCard != nullptr )
        {
            count++;
        }
    }
    return count;
}

void Cell::clearFirst(bool isFromSetter)
//Очищает ячейку от первой карточки
{
    if (firstCard != nullptr)
    {
        //disconnect (fCardDestroyedConnection);
        disconnect (firstCard, 0, this, 0);
        disconnect (firstCard->getTeacher(), 0, this, 0);
        disconnect (firstCard->getClass(), 0, this, 0);
        disconnect (firstCard->getDiscipline(), 0, this, 0);

        clearFirstAction->setEnabled(false);

        //Если дальше будет следовать добавление карточки или второй карточки нет
        if (isFromSetter || secondCard == nullptr)
        {
            firstCard->add();
            firstCard = nullptr;
        }
        //Иначе у нас будет пустая первая карточка и непустая вторая, сл-но нужно вторую карточку сделать первой
        else
        {
            //Создаю временное хранилище, т.к. нужно сначала дисконектнуть все от secondCard и конектнуть к firstCard
            Card * tempCard = secondCard;
            clearSecond();
            setFirstCard(tempCard);
        }
        repaint();
    }
    cardTable->updateFocus();
}

void Cell::clearSecond()
//Очищает ячейку от второй карточки
{
    if (secondCard != nullptr)
    {
        //disconnect (sCardDestroyedConnection);
        disconnect (secondCard, 0, this, 0);
        disconnect (firstCard->getTeacher(), 0, this, 0);
        disconnect (secondCard->getClass(), 0, this, 0);
        disconnect (secondCard->getDiscipline(), 0, this, 0);
        secondCard->add();
        secondCard = nullptr;
        repaint();

        clearSecondAction->setEnabled(false);
    }
    cardTable->updateFocus();
}

void Cell::removeCard(Card *card)
//Удалить указанную карточку из ячейки
{
    if (card == getFirstCard())
    {
        if (secondCard == nullptr)
        {
            clearFirst();
        }
        else
        {
            clearFirst(false);
        }
    }
    if (card == getSecondCard())
    {
        clearSecond();
    }
}

//ПУСТЬ ГОРЯТ В АДУ ЭТИ КАРТОЧКИ!!! У меня сгорело и я решил сделать для каждой замены по методу

void Cell::swapFfromF(Cell *guestCell)
//Меняет местами первую карточку ячейки и первую указанной
{
    Card *hostCard = getFirstCard();
    Card *guestCard = guestCell->getFirstCard();

    if (hostCard == nullptr)
    {
        if (guestCard == nullptr)
        {
            return;
        }
        else
        {
            setFirstCard(guestCard);
            if(guestCell->getSecondCard() == nullptr)
            {
                guestCell->clearFirst();
            }
            else
            {
                 guestCell->clearFirst(false);
            }
        }
    }
    else
    {
        if (guestCard == nullptr)
        {
            guestCell->setFirstCard(getFirstCard());
            clearFirst();
        }
        else
        {
            clearFirst();
            setFirstCard(guestCard);
            guestCell->clearFirst();
            guestCell->setFirstCard(hostCard);
        }
    }
}

void Cell::swapFfromS(Cell *guestCell)
//Меняет местами первую карточку ячейки и вторую указанной
{
    Card *hostCard = getFirstCard();
    Card *guestCard = guestCell->getSecondCard();

    if (hostCard == nullptr)
    {
        if (guestCard == nullptr)
        {
            return;
        }
        else
        {
            setFirstCard(guestCard);
            guestCell->clearSecond();
        }
    }
    else
    {
        if (guestCard == nullptr)
        {
            guestCell->setSecondCard(getFirstCard());
            clearFirst();
        }
        else
        {
            clearFirst();
            setFirstCard(guestCard);
            guestCell->clearSecond();
            guestCell->setSecondCard(hostCard);
        }
    }
}

void Cell::swapSfromF(Cell *guestCell)
//Меняет местами вторую карточку ячейки и первую указанной
{
    Card *hostCard = getSecondCard();
    Card *guestCard = guestCell->getFirstCard();

    if (hostCard == nullptr)
    {
        if (guestCard == nullptr)
        {
            return;
        }
        else
        {
            setSecondCard(guestCard);
            if(guestCell->getSecondCard() == nullptr)
            {
                guestCell->clearFirst();
            }
            else
            {
                 guestCell->clearFirst(false);
            }
        }
    }
    else
    {
        if (guestCard == nullptr)
        {
            guestCell->setFirstCard(getFirstCard());
            clearSecond();
        }
        else
        {
            clearSecond();
            setSecondCard(guestCard);
            guestCell->clearFirst();
            guestCell->setFirstCard(hostCard);
        }
    }
}

void Cell::swapSfromS(Cell *guestCell)
//Меняет местами вторую карточку ячейки и вторую указанной
{
    Card *hostCard = getSecondCard();
    Card *guestCard = guestCell->getSecondCard();

    if (hostCard == nullptr)
    {
        if (guestCard == nullptr)
        {
            return;
        }
        else
        {
            setSecondCard(guestCard);
            guestCell->clearSecond();
        }
    }
    else
    {
        if (guestCard == nullptr)
        {
            guestCell->setSecondCard(getFirstCard());
            clearSecond();
        }
        else
        {
            clearSecond();
            setSecondCard(guestCard);
            guestCell->clearSecond();
            guestCell->setSecondCard(hostCard);
        }
    }
}

bool Cell::isCardTeacherValid(Card *card, bool isFirst)
//Проверка кореектности карточки, проверка всех ограничений
{
    if (card == nullptr)
    {
        return true;
    }
    const Teacher *const cardTeacher = card->getTeacher();
    Day *day = cardTable->getParentDay(lesson);
    int rowIndex = day->lessonList.indexOf(lesson);
    for(int columnIndex = 0; columnIndex < day->cellMatrix[rowIndex].count();columnIndex++)
    {
        Cell *cell = day->cellMatrix[rowIndex][columnIndex];
        if (cell->getFirstCard()!= nullptr)
        {
            const Teacher *const teacher = cell->getFirstCard()->getTeacher();
            if (teacher == cardTeacher)
            {
                if (!(isFirst && (cell == this)))
                {
                    return false;
                }
            }
        }
        if (cell->getSecondCard()!= nullptr)
        {
            const Teacher *const teacher = cell->getSecondCard()->getTeacher();
            if (teacher == cardTeacher)
            {
                if (!(!isFirst && (cell == this)))
                {
                    return false;
                }
            }
        }
    }
    return true;
}

bool Cell::cardValidation(Card *card, bool isFirst)
{
    if (!isCardTeacherValid(card, isFirst))
    {
        QMessageBox warningBox(QMessageBox::Warning,
                           card->getTeacher()->getShortName(),
                           "Учитель уже ведет занятие во время " + getLesson()->getName() + " урока",
                           0);
        warningBox.exec();
        return false;
    }
    else
    {
        return true;
    }
}

void Cell::clearAll()
//Очищает ячейку от всех карточек
{
    clearSecond();
    clearFirst();
}

void Cell::paintEvent(QPaintEvent *event)
//Переопределяю метод перерисовки
{
    //Если в ячейки нет карточе, то рисуется дефолтно
    if (firstCard ==  nullptr && secondCard == nullptr)
    {
        TableElement::paintEvent(event);
    }
    //Если только одна карточка - рисуется соотв-ще
    if (firstCard !=  nullptr && secondCard == nullptr)
    {
        //Задаю параметры рисовальщика и рисую фон
        QPainter painter(this);
        painter.save();
        painter.setBrush(firstCard->getBackgroundColor());
        QPen newPen;
        if (!isFocused)
        {
            newPen.setColor(_borderColor);
        }
        else
        {
            newPen.setColor(_focusedColor);
        }
        newPen.setWidth(2);
        painter.setPen(newPen);
        painter.drawRect(rect());
        painter.restore();
        newPen.setColor(firstCard->getForegroundColor());
        painter.setPen(newPen);
        QFont font;
        font.setPointSize(fontSize);
        painter.setFont(font);

        //Проверяю помещается ли строка в ячейку
        QFontMetrics fontMetrics(font);
        QString topStr;
        if (firstCard->getTopFlag() == "name")
        {
            topStr = firstCard->getTeacher()->getShortName();
        }
        if (firstCard->getTopFlag() == "dis")
        {
            topStr = firstCard->getDiscipline()->getName();
        }
        if (firstCard->getTopFlag() == "class")
        {
            topStr = firstCard->getClass()->getName();
        }
        QString botStr;
        if (firstCard->getBotFlag() == "name")
        {
            botStr = firstCard->getTeacher()->getShortName();
        }
        if (firstCard->getBotFlag() == "dis")
        {
            botStr = firstCard->getDiscipline()->getName();
        }
        if (firstCard->getBotFlag() == "class")
        {
            botStr = firstCard->getClass()->getName();
        }


        //Если не помещается - пишу только первые три символа и троеточик
        if(fontMetrics.width(botStr) > rect().width()*0.8)
        {
            botStr = botStr.left(3) + "...";
        }
        if(fontMetrics.width(topStr) > rect().width()*0.8)
        {
            topStr = topStr.left(3) + "...";
        }

        //Сравниваю размеры ячейки с заданными минимальными размерами ячейки из таблицы - решаю как отрисовывать текст
        if (rect().height() >= cardTable->minCellHeightForText && rect().width() >= cardTable->minCellWidthForText)
        {
            if (rect().height() >= cardTable->minCellHeightForDoubleText)
            {
                painter.drawText(0, 0, rect().width(), rect().height()/2, Qt::AlignCenter, topStr);
                painter.drawText(0, rect().height()/2, rect().width(), rect().height()/2, Qt::AlignCenter, botStr);
            }
            else
            {
                painter.drawText(rect(), Qt::AlignCenter, botStr);
            }
        }
    }
    if (firstCard !=  nullptr && secondCard != nullptr)
    {
        //Задаю общие параметры рисовальщика и рисую фон
        QPainter painter(this);
        QPen newPen;
        if (!isFocused)
        {
            newPen.setColor(_borderColor);
        }
        else
        {
            newPen.setColor(_focusedColor);
        }
        newPen.setWidth(2);
        painter.setPen(newPen);
        QFont font;
        font.setPointSize(fontSize*0.6);
        painter.setFont(font);

        //Рисую прямоугольники первой и второй карточки
        painter.setBrush(firstCard->getBackgroundColor());
        painter.drawRect(0, 0, rect().width()/2, rect().height());
        painter.setBrush(secondCard->getBackgroundColor());
        painter.drawRect(rect().width()/2, 0, rect().width()/2, rect().height());

        //Проверяю помещается ли строка в ячейку
        QFontMetrics fontMetrics(font);

        QString botStrFirst;
        QString botStrSecond;
        QString topStrFirst;
        QString topStrSecond;


        {
            if (firstCard->getTopFlag() == "name")
            {
                topStrFirst = firstCard->getTeacher()->getShortName();
            }
            if (firstCard->getTopFlag() == "dis")
            {
                topStrFirst = firstCard->getDiscipline()->getName();
            }
            if (firstCard->getTopFlag() == "class")
            {
                topStrFirst = firstCard->getClass()->getName();
            }

            if (firstCard->getBotFlag() == "name")
            {
                botStrFirst = firstCard->getTeacher()->getShortName();
            }
            if (firstCard->getBotFlag() == "dis")
            {
                botStrFirst = firstCard->getDiscipline()->getName();
            }
            if (firstCard->getBotFlag() == "class")
            {
                botStrFirst = firstCard->getClass()->getName();
            }
        }


        {
            if (secondCard->getTopFlag() == "name")
            {
                topStrSecond = secondCard->getTeacher()->getShortName();
            }
            if (secondCard->getTopFlag() == "dis")
            {
                topStrSecond = secondCard->getDiscipline()->getName();
            }
            if (secondCard->getTopFlag() == "class")
            {
                topStrSecond = secondCard->getClass()->getName();
            }

            if (secondCard->getBotFlag() == "name")
            {
                botStrSecond = secondCard->getTeacher()->getShortName();
            }
            if (secondCard->getBotFlag() == "dis")
            {
                botStrSecond = secondCard->getDiscipline()->getName();
            }
            if (secondCard->getBotFlag() == "class")
            {
                botStrSecond = secondCard->getClass()->getName();
            }
        }

        //Если не помещается - пишу только первые три символа и троеточиe
        if(fontMetrics.width(botStrFirst) > rect().width()*0.8*0.5)
        {
            botStrFirst = botStrFirst.left(3) + "...";
        }
        if(fontMetrics.width(botStrSecond) > rect().width()*0.8*0.5)
        {
            botStrSecond = botStrSecond.left(3) + "...";
        }
        if(fontMetrics.width(topStrFirst) > rect().width()*0.8*0.5)
        {
            topStrFirst = topStrFirst.left(3) + "...";
        }
        if(fontMetrics.width(topStrSecond) > rect().width()*0.8*0.5)
        {
            topStrSecond = topStrSecond.left(3) + "...";
        }

        //Сравниваю размеры ячейки с заданными минимальными размерами ячейки из таблицы - решаю как отрисовывать текст
        if (rect().height() >= cardTable->minCellHeightForText && rect().width()*0.5 >= cardTable->minCellWidthForText)
        {
            if (rect().height() >= cardTable->minCellHeightForDoubleText)
            {
                //Устанавливаю цвет пера и пишу текс
                newPen.setColor(firstCard->getForegroundColor());
                painter.setPen(newPen);
                painter.drawText(0, 0, rect().width()*0.5, rect().height()*0.5, Qt::AlignCenter, topStrFirst);
                painter.drawText(0, rect().height()/2, rect().width()*0.5, rect().height()*0.5, Qt::AlignCenter, botStrFirst);

                //Устанавливаю цвет пера и пишу текс
                newPen.setColor(secondCard->getForegroundColor());
                painter.setPen(newPen);
                painter.drawText(rect().width()*0.5, 0, rect().width()*0.5, rect().height()*0.5, Qt::AlignCenter, topStrSecond);
                painter.drawText(rect().width()*0.5, rect().height()/2, rect().width()*0.5, rect().height()*0.5, Qt::AlignCenter, botStrSecond);
            }
            else
            {
                //Устанавливаю цвет пера и пишу текс
                newPen.setColor(firstCard->getForegroundColor());
                painter.setPen(newPen);
                painter.drawText(0, 0, rect().width()*0.5, rect().height(), Qt::AlignCenter, botStrFirst);

                //Устанавливаю цвет пера и пишу текс
                newPen.setColor(secondCard->getForegroundColor());
                painter.setPen(newPen);
                painter.drawText(rect().width()*0.5, 0, rect().width()*0.5, rect().height(), Qt::AlignCenter, botStrSecond);
            }
        }
    }
}

void Cell::dragEnterEvent(QDragEnterEvent *event)
//Переопределяю событие, когда курсор проходит над элементом, при этом происходит перетаскивание чего - то
{

    if (event->mimeData()->hasFormat("application/widget/card")           ||
        event->mimeData()->hasFormat("application/widget/cell/firstCard") ||
        event->mimeData()->hasFormat("application/widget/cell/secondCard")   )
    {
        const WidgetMimeData* mimeData = dynamic_cast<const WidgetMimeData*> (event->mimeData());
        Card *card;
        if( event->mimeData()->hasFormat("application/widget/card"))
        {
            card = (Card*)(mimeData->widget());
        }
        else
        {
            Cell *cell = (Cell*)(mimeData->widget());
            card = cell->getFirstCard();
        }
        if (card->getClass() == getClass() && card->getTeacher()->lessonList.contains(getLesson()))
        {
            event->acceptProposedAction();
        }
    }
}

void Cell::dropEvent(QDropEvent *event)
//Событие броска в виджет чего - то |Не смотреть - сломаешь глаза!|
{
    const WidgetMimeData* mimeData = dynamic_cast<const WidgetMimeData*> (event->mimeData());

    //Если перетащили из списка карточек
    if(event->mimeData()->hasFormat("application/widget/card"))
    {
        Card *card = (Card*)(mimeData->widget());
        if (card->isEmpty())
        {
            QMessageBox warningBox(QMessageBox::Warning,
                               card->getDiscipline()->getName() + " - " + card->getClass()->getName(),
                               "Карточки закончились!",
                               0);
            warningBox.exec();
            return;
        }
        if(event->keyboardModifiers() & Qt::CTRL)
        {
            if (firstCard != nullptr)
            {
                if(cardValidation(card, false))
                {
                    setSecondCard(card);
                }
            }
            else
            {
                if(cardValidation(card, true))
                {
                    setFirstCard(card);
                }
            }
        }
        else
        {
            if (event->pos().x() > this->width()*0.5 && secondCard != nullptr)
            {
                if(cardValidation(card, false))
                {
                    setSecondCard(card);
                }
            }
            else
            {
                if(cardValidation(card, true))
                {
                    setFirstCard(card);
                }
            }
        }
    }
    //Иначе перетащили из ячейки - обрабатываем подругому
    else
    {
        //Получаем ячейку и карточку источника
        Cell *cell = (Cell*)(mimeData->widget());
        Card *card;
        bool isFromFirst = true;
        if(event->mimeData()->hasFormat("application/widget/cell/firstCard"))
        {
            card = cell->getFirstCard();
            isFromFirst = true;
        }
        if (event->mimeData()->hasFormat("application/widget/cell/secondCard") )
        {
            card = cell->getSecondCard();
            isFromFirst = false;
        }

        //Если зажат Ctrl миллиард ифов - дабы определить как их перетасовывать...
        if (event->keyboardModifiers() & Qt::CTRL)
        {
            if (secondCard != nullptr)
            {
                if(isFromFirst)
                {
                    if((cell == this) || cardValidation(card, false))
                    {
                        if((cell == this) || cell->cardValidation(getFirstCard(), true))
                        {
                            swapSfromF(cell);
                        }
                    }
                }
                else
                {
                    if(cardValidation(card, false))
                    {
                        if(cell->cardValidation(getSecondCard(), false))
                        {
                            swapSfromS(cell);
                        }
                    }
                }
            }
            else
            {
                if (firstCard == nullptr)
                {
                    if(isFromFirst)
                    {
                        if(cardValidation(card, true))
                        {
                            if(cell->cardValidation(getFirstCard(), true))
                            {
                                swapFfromF(cell);
                            }
                        }
                    }
                    else
                    {
                        if(cardValidation(card, true))
                        {
                            if(cell->cardValidation(getSecondCard(), false))
                            {
                                swapFfromS(cell);
                            }
                        }
                    }
                }
                else
                {
                    if(cardValidation(card, false))
                    {
                       setSecondCard(card);
                       if(isFromFirst)
                       {
                           if(cell->secondCard == nullptr)
                           {
                               cell->clearFirst();
                           }
                           else
                           {
                               cell->clearFirst(false);
                           }
                       }
                       else
                       {
                           cell->clearSecond();
                       }
                    }
                }
            }
        }
        //Иначе смотрим в каую зону ячейки был бросок(в левую или правую, т.к. у ячейки может быть две карточки)
        //И снова миллиард ифов
        else
        {
            if (event->pos().x() > this->width()*0.5 && secondCard != nullptr)
            {
                if(isFromFirst)
                {
                    if((cell == this) ||cardValidation(card, false))
                    {
                        if((cell == this) ||cell->cardValidation(getFirstCard(), true))
                        {
                            swapSfromF(cell);
                        }
                    }
                }
                else
                {
                    if(cardValidation(card, false))
                    {
                        if(cell->cardValidation(getSecondCard(), false))
                        {
                            swapSfromS(cell);
                        }
                    }
                }
            }
            else
            {
                if(isFromFirst)
                {
                    if(cardValidation(card, true))
                    {
                        if(cell->cardValidation(getFirstCard(), true))
                        {
                            swapFfromF(cell);
                        }
                    }
                }
                else
                {
                    if((cell == this) ||cardValidation(card, true))
                    {
                        if((cell == this) || cell->cardValidation(getSecondCard(), false))
                        {
                            swapFfromS(cell);
                        }
                    }
                }
            }
        }
    }

    cardTable->updateFocus();
}

void Cell::mousePressEvent(QMouseEvent *event)
//Переопределяю событие нажатие кнопки мыши над ячейкой
{
    //Если это левая кнопка мыши - запоминаю точку, где нажал (нужно для того, чтобы разделить нажатие по карточки и перетаскивание)
    if (event->button() == Qt::LeftButton)
    {
        dragStartPosition = event->pos();
    }
}

void Cell::mouseMoveEvent(QMouseEvent *event)
//Переопределяю событие движение мыши над ячейкой
{
    //Если не зажата левая кнопка мыши - return
    if (!(event->buttons() & Qt::LeftButton) || firstCard == nullptr)
    {
        return;
    }
    //Смотрю чтобы мышь продвинулась на расстояние startDragDisctance, иначе - return
    if ((event->pos() - dragStartPosition).manhattanLength() < QApplication::startDragDistance())
    {
        return;
    }

    //Задаю параметры перетаскивания
    QDrag *drag = new QDrag(this);

    //Создаю перемунную, которая сможет хранить ссылку на виджет
    WidgetMimeData* mimeData = new WidgetMimeData;

    QPixmap *pix = new QPixmap(20, 20);
    //Смотрю в какой половине ячейки нажали на мышь - узнать какую карточку юзер хочет перетащить
    //(если вторая пустая - то в любом случае берем первыю)
    Card *card;
    bool isFirst= true;
    if(dragStartPosition.x() > this->width()*0.5 && secondCard != nullptr)
    {
        //Тут, собственно, пихаю туда ссыль
        mimeData->setWidget(this, "secondCard");
        pix->fill(secondCard->getBackgroundColor());
        card = secondCard;
        isFirst = false;
    }
    else
    {
        //Тут, собственно, пихаю туда ссыль
        mimeData->setWidget(this, "firstCard");
        pix->fill(firstCard->getBackgroundColor());
        card = firstCard;
    }

    drag->setMimeData(mimeData);
    drag->setPixmap(*pix);
    emit dragStarted(card, isFirst);
    drag->exec();
    card->dragStoped();
}

void Cell::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (firstCard != nullptr)
    {
        if(secondCard != nullptr)
        {
            if (event->pos().x() > this->width()*0.5 && secondCard != nullptr)
            {
                secondCard->edit();
            }
            else
            {
                firstCard->edit();
            }
        }
        else
        {
            firstCard->edit();
        }
    }
}

void Cell::contextMenuEvent(QContextMenuEvent *event)
//Переопределяю вызов контекстного меню
{
    //Костыль - иногда,при вызове меню, не срабатывает moseLeave,а должен
    // - на всякий случай высылаю еще один сигнал.
    emit leaveEvent(event);
    QMenu *contextMenu = new QMenu();
    if (event->pos().x() > this->width()*0.5 && secondCard != nullptr)
    {
        contextMenu->addAction(clearSecondAction);
    }
    else
    {
        contextMenu->addAction(clearFirstAction);
    }
    contextMenu->exec(event->globalPos());
}

void Cell::enterEvent(QEvent *event)
{
    QString str;
    if (firstCard != nullptr)
    {
        if (firstCard->getTipFlag() == "name")
        {
            str = firstCard->getTeacher()->getShortName();
        }
        if (firstCard->getTipFlag() == "dis")
        {
            str = firstCard->getDiscipline()->getName();
        }
        if (firstCard->getTipFlag() == "class")
        {
            str = firstCard->getClass()->getName();
        }
    }
    if (secondCard != nullptr)
    {
        if (secondCard->getTipFlag() == "name")
        {
            str += " || " + secondCard->getTeacher()->getShortName();
        }
        if (secondCard->getTipFlag() == "dis")
        {
            str += " || " + secondCard->getDiscipline()->getName();
        }
        if (firstCard->getTipFlag() == "class")
        {
            str += " || " + secondCard->getClass()->getName();
        }

    }

    QToolTip::showText(cursor().pos(), str, this);
}






