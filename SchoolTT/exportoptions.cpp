#include "exportoptions.h"

ExportOptions::ExportOptions(QObject *parent) : QObject(parent)
{

}

void ExportOptions::getOptions(ExportDialog *exportDialog)
{
    disciplineHeight = exportDialog->getDisciplineHeight();
    disciplineWidth = exportDialog->getDisciplineWidth();
    disciplineFontSize = exportDialog->getDisciplineFontSize();
    disciplineCursive = exportDialog->getDisciplineCursive();
    disciplineBold = exportDialog->getDisciplineBold();

    roomWidth = exportDialog->getRoomWidth();
    roomFontSize = exportDialog->getRoomFontSize();
    roomCursive = exportDialog->getRoomCursive();
    roomBold = exportDialog->getRoomBold();

    classHeight = exportDialog->getClassHeight();
    classFontSize = exportDialog->getClassFontSize();
    classCursive = exportDialog->getClassCursive();
    classBold = exportDialog->getClassBold();

    dayWidth = exportDialog->getDayWidth();
    dayFontSize = exportDialog->getDayFontSize();
    dayCursive = exportDialog->getDayCursive();
    dayBold = exportDialog->getDayBold();

    lessonWidth = exportDialog->getLessonWidth();
    lessonFontSize = exportDialog->getLessonFontSize();
    lessonCursive = exportDialog->getLessonCursive();
    lessonBold = exportDialog->getLessonBold();

    topLeftText = exportDialog->getLeftTopText();
    topCentralText = exportDialog->getCentralTopText();
    topRightText = exportDialog->getRightTopText();
    botLeftText = exportDialog->getLeftBotText();
    botCentralText = exportDialog->getCentralBotText();
    botRightText = exportDialog->getRightBotText();
}

void ExportOptions::getOptions(ExportTeacherDialog *exportDialog)
{
    t_teacherHeight = exportDialog->getTeacherHeight();
    t_teacherWidth = exportDialog->getTeacherWidth();
    t_teacherFontSize = exportDialog->getTeacherFontSize();
    t_teacherBold = exportDialog->getTeacherBold();
    t_teacherCursive = exportDialog->getTeacherCursive();

    t_headerHeight = exportDialog->getHeaderHeight();
    t_headerWidth = exportDialog->getHeaderWidth();
    t_headerFontSize = exportDialog->getHeaderFontSize();
    t_headerBold = exportDialog->getHeaderBold();
    t_headerCursive = exportDialog->getHeaderCursive();

    t_classHeight = exportDialog->getClassHeight();
    t_classWidth = exportDialog->getClassWidth();
    t_classFontSize = exportDialog->getClassFontSize();
    t_classBold = exportDialog->getClassBold();
    t_classCursive = exportDialog->getClassCursive();

    t_dayHeight = exportDialog->getDayHeight();
    t_dayWidth = exportDialog->getDayWidth();
    t_dayFontSize = exportDialog->getDayFontSize();
    t_dayBold = exportDialog->getDayBold();
    t_dayCursive = exportDialog->getDayCursive();

    t_lessonHeight = exportDialog->getLessonHeight();
    t_lessonWidth = exportDialog->getLessonWidth();
    t_lessonFontSize = exportDialog->getLessonFontSize();
    t_lessonBold = exportDialog->getLessonBold();
    t_lessonCursive = exportDialog->getLessonCursive();
}
