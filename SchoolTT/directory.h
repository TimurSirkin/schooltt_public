#ifndef DIRECTORY_H
#define DIRECTORY_H

#include <QLinkedList>
#include <QVector>
#include <QString>
#include <QObject>
#include "teacher.h"
#include "discipline.h"
#include "room.h"
#include "class.h"
#include "card.h"


class Directory : public QObject
{
    Q_OBJECT
public:
    explicit Directory(QObject *parent = nullptr);

    QLinkedList<Teacher*> teacherList;
    QLinkedList<Discipline*> disciplineList;
    QLinkedList<Room*> roomList;
    Discipline *addDiscipline(QString name);
    void addDiscipline(Discipline *discipline);
    void addRoom(Room *room);
    void addTeacher(Teacher *teacher);

    Discipline *getDiscipline(QString name);
    Teacher *getTeacher(int id);

signals:
    void classAdded(Teacher* teacher, Discipline* discipline, Class* _class);
};

#endif // DIRECTORY_H
