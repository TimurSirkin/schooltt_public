#ifndef STARTWIDGET_H
#define STARTWIDGET_H

#include <QDialog>
#include "mainwindow.h"

class StartWidget : public QDialog
{
    Q_OBJECT
public:
    explicit StartWidget(MainWindow *parent);
};

#endif // STARTWIDGET_H
