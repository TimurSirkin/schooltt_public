#ifndef DISCIPLINEDIALOG_H
#define DISCIPLINEDIALOG_H

#include <QWidget>
#include <QDialog>
#include <QScrollArea>
#include <QListWidget>
#include <QList>
#include <QPixmap>
#include <QLabel>
#include <QListWidgetItem>
#include "directory.h"
#include "discipline.h"

class DisciplineDialog : public QDialog
{
    Q_OBJECT
public:
    explicit DisciplineDialog(Directory *_directory, QWidget *parent = nullptr);

private:
    QScrollArea *scrollArea = new QScrollArea();
    QGridLayout *scrollLayout = new QGridLayout();
    Directory *directory;

    void editDiscipline(Discipline *discipline, QLabel *nameLabel);
    void deleteDiscipline(Discipline *discipline, QList<QWidget *> wgtList);
    QWidget *addDiscipline(Discipline *discipline);

    QString getStr(QString str);

public slots:
    void createDiscipline();

};

#endif // DISCIPLINEDIALOG_H
