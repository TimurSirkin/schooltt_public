#ifndef CARDEDITDIALOG_H
#define CARDEDITDIALOG_H

#include <QDialog>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include "card.h"
#include "directory.h"
#include "cardtable.h"
#include "cardlist.h"

class CardEditDialog : public QDialog
{
    Q_OBJECT
public:
    explicit CardEditDialog(Card *pCard, CardTable *pCardTable, Directory *pDirectory, CardList *pCardList, QWidget *parent = nullptr);
    QColor backgroundColor;
    QColor foregroundColor;

private:
    QGridLayout *mainLayout = new QGridLayout();
    QLabel *backgroundLabel = new QLabel("Цвет фона:");
    QLabel *foregroundLabel = new QLabel("Цвет текста:");
    QPushButton *backgroundEditButton = new QPushButton();
    QPushButton *foregroundEditButton = new QPushButton();
    QPushButton *okButton = new QPushButton("Принять");
    QPushButton *cancelButton = new QPushButton("Отменить");
    QPushButton *classButton;
    QPushButton *disButton;

    CardTable *cardTable;
    Directory *directory;
    CardList *cardList;
    Card *card;

    QWidget *enabledCard;
    QWidget *disabledCard;

    void buildWorkTimeBlock();
    void setCurrentColor();

signals:

private slots:
    void slot_showToolTip(const QString &str);

public slots:
    void choseColor(QPushButton *currentButton, QColor *groundColor);
    void linkToTeacher();
};

#endif // CARDEDITDIALOG_H
