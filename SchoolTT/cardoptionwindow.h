#ifndef CARDOPTIONWINDOW_H
#define CARDOPTIONWINDOW_H

#include <QDialog>
#include "cardtable.h"

class CardOptionWindow : public QDialog
{
    Q_OBJECT
public:
    explicit CardOptionWindow(CardTable *cardTable, QWidget *parent = nullptr);

signals:

public slots:
};

#endif // CARDOPTIONWINDOW_H
