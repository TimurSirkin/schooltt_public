#include "teacherdialog.h"
#include <QPixmap>
#include <QList>
#include <QShortcut>
#include <QGridLayout>
#include <QScrollBar>
#include <QPushButton>
#include <QLabel>
#include <QDebug>
#include <QMessageBox>
#include <QAction>
#include "styles.h"
#include <QActionGroup>
#include <QToolTip>
#include "teacher.h"
#include "teachereditdialog.h"
#include "card.h"
#include "dialogfeatures.h"

//Диалоговое окно добавления, удаления и редактирования учителя

TeacherDialog::TeacherDialog(CardTable *_cardTable, Directory *_directory, CardList *pCardList,  QWidget *parent) : QDialog(parent)
  //Конструктор
{
    directory = _directory;
    cardTable = _cardTable;
    cardList = pCardList;
    setWindowTitle("Справочник учителей");
    QGridLayout *mainLayout = new QGridLayout();
    setLayout(mainLayout);
    QPushButton *addButton = new QPushButton("Добавить учителя");
    addButton->setMinimumHeight(30);
    mainLayout->addWidget(addButton, 1, 0, 1, 2);
    connect(addButton, SIGNAL(clicked()), SLOT(createTeacher()));
    mainLayout->addWidget(teacherScrollArea,0,0,1, 2);

    QWidget *wgt = new QWidget();
    wgt->setLayout(scrollLayout);
    teacherScrollArea->setWidget(wgt);
    wgt->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    teacherScrollArea->setWidgetResizable(true);


    QLabel *lbl = new QLabel("<b>ФИО<b>");
    lbl->setFixedHeight(20);
    scrollLayout->addWidget(lbl, 0, 0);
    scrollLayout->addWidget(new QLabel("<b>Предметов<b>"), 0, 1, 1, 1, Qt::AlignCenter);

    for (Teacher *teacher : directory->teacherList)
    {
        addTeacher(teacher);
    }

    new QShortcut(QKeySequence(Qt::Key_Insert), this, SLOT(createTeacher()));
    setFixedWidth(350);

    DialogFeatures::sort(scrollLayout);
}

QWidget *TeacherDialog::addTeacher(Teacher *teacher)
//Добавить учителя в список учителей окна
{
    QList<QWidget*> wgtList;

    QString name1 = teacher->getSecondName();
    QString name2 = teacher->getFirstName();
    QString name3 = teacher->getThirdName();
    QPushButton* nameButton= new QPushButton();
    if (name1.length() > 15)
    {
        nameButton->setText(name1.left(12) + "..." + " " + name2.left(1) + "." + name3.left(1) + ".");
    }
    else
    {
        nameButton->setText(name1 + " " + name2.left(1) + "." + name3.left(1) + ".");
    }
    nameButton->setStyleSheet(Styles::linkButton());
    connect(nameButton, &QPushButton::clicked, [=] () {editTeacher(teacher, nullptr, nullptr, true);});
    wgtList.push_back(nameButton);
    scrollLayout->addWidget(nameButton, scrollLayout->rowCount(), 0, 1, 1, Qt::AlignLeft);

    QPushButton* countButton= new QPushButton(QString::number(teacher->disciplineMap.count()));
    countButton->setStyleSheet(Styles::linkButton());
    QString tipStr;
    for (Discipline* dis : teacher->disciplineMap.keys())
    {
        tipStr += dis->getName() + "<br>";
    }
    tipStr.remove(tipStr.length()-4,4);
    connect(countButton, &QPushButton::clicked, [=] () {QToolTip::showText(QCursor::pos(), tipStr);});
    wgtList.push_back(countButton);
    scrollLayout->addWidget(countButton, scrollLayout->rowCount()-1, 1, 1, 1, Qt::AlignCenter);

    QPushButton* editButton = new QPushButton();
    wgtList.push_back(editButton);
    editButton->setStyleSheet(Styles::editButton());
    scrollLayout->addWidget(editButton, scrollLayout->rowCount()-1, 2);

    QPushButton* deleteButton = new QPushButton( );
    wgtList.push_back(deleteButton);
    deleteButton->setStyleSheet(Styles::deleteButton());
    scrollLayout->addWidget(deleteButton, scrollLayout->rowCount()-1, 3);

    connect(editButton, &QPushButton::clicked, [=]() {editTeacher(teacher, nameButton, countButton);});
    connect(deleteButton, &QPushButton::clicked, [=]() {deleteTeacher(teacher, wgtList);});

    return nameButton;
}

void TeacherDialog::editTeacher(Teacher *teacher, QPushButton *nameLabel, QPushButton *countButton,  bool isForLooking)
//Редактирование учителя
{
    TeacherEditDialog *inputDialog = new TeacherEditDialog(cardTable, directory, cardList, isForLooking, teacher);
    if (inputDialog->exec() == QDialog::Accepted)
    {
        QString name1 = inputDialog->secondNameLine->text();
        QString name2 = inputDialog->firstNameLine->text();
        QString name3 = inputDialog->thirdNameLine->text();

        //Обработка измененных и удаленных карточек
        for (Discipline *oldDiscipline : teacher->disciplineMap.keys())
        {
            if (inputDialog->disciplineMap.contains(oldDiscipline))
            {
                for (Class *_class : teacher->disciplineMap[oldDiscipline].keys())
                {
                    if(inputDialog->disciplineMap[oldDiscipline].contains(_class))
                    {
                        if(inputDialog->disciplineMap[oldDiscipline][_class] !=  teacher->disciplineMap[oldDiscipline][_class])
                        {
                            teacher->countChanged(oldDiscipline, _class, inputDialog->disciplineMap[oldDiscipline][_class]);
                        }
                    }
                    else
                    {
                        for (Day *day : cardTable->dayList)
                        {
                            for (Lesson *lesson : day->lessonList)
                            {
                                Cell *cell = day->cellMatrix[day->lessonList.indexOf(lesson)][cardTable->classList.indexOf(_class)];
                                if (cell->getSecondCard() != nullptr
                                        && cell->getSecondCard()->getTeacher() == teacher
                                        && cell->getSecondCard()->getDiscipline() == oldDiscipline)
                                {
                                    cell->clearSecond();
                                }
                                if (cell->getFirstCard() != nullptr
                                        && cell->getFirstCard()->getTeacher() == teacher
                                        && cell->getFirstCard()->getDiscipline() == oldDiscipline)
                                {
                                    cell->clearFirst(false);
                                }
                            }
                        }
                        teacher->classDeleted(oldDiscipline, _class);
                    }
                }
            }
            else
            {
                for (Day *day : cardTable->dayList)
                {
                    for (Lesson *lesson : day->lessonList)
                    {
                        for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
                        {
                            if (cell->getSecondCard() != nullptr
                                    && cell->getSecondCard()->getTeacher() == teacher
                                    && cell->getSecondCard()->getDiscipline() == oldDiscipline)
                            {
                                cell->clearSecond();
                            }
                            if (cell->getFirstCard() != nullptr
                                    && cell->getFirstCard()->getTeacher() == teacher
                                    && cell->getFirstCard()->getDiscipline() == oldDiscipline)
                            {
                                cell->clearFirst(false);
                            }
                        }

                    }
                }
                teacher->disciplineDeleted(oldDiscipline);
            }
        }

        //Обработка новых карточек
        for(Discipline *newDiscipline : inputDialog->disciplineMap.keys())
        {
            if (teacher->disciplineMap.contains(newDiscipline))
            {
                for (Class *_class : inputDialog->disciplineMap[newDiscipline].keys())
                {
                    if (!teacher->disciplineMap[newDiscipline].contains(_class))
                    {
                        //Предварительно меняю кол-во уроков для класса на новое ( с нуля на значение из окна редактировани)
                        //т.к. следуйще строчко я отправлю сигнал о добавении, но в этот момент я еще не подменил старый словарь предметов на новый,
                        //сл-но у учителя не будет в словаре нового значения
                        teacher->disciplineMap[newDiscipline][_class] = inputDialog->disciplineMap[newDiscipline][_class];
                        teacher->classAdded(newDiscipline, _class);
                    }
                }
            }
            else
            {
                for (Class *_class : inputDialog->disciplineMap[newDiscipline].keys())
                {
                    //Предварительно меняю кол-во уроков для класса на новое ( с нуля на значение из окна редактировани)
                    //т.к. следуйще строчкой я отправлю сигнал о добавлении, но в этот момент я еще не подменил старый словарь предметов на новый,
                    //сл-но у учителя не будет в словаре нового значения
                    teacher->disciplineMap[newDiscipline][_class] = inputDialog->disciplineMap[newDiscipline][_class];
                    teacher->classAdded(newDiscipline, _class);
                }
            }
        }

        //Обработка удаленных дней
        for (Lesson *lesson : teacher->lessonList)
        {
            if (!inputDialog->lessonList.contains(lesson))
            {
                Day *day = cardTable->getParentDay(lesson);
                for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
                {
                    if (cell->getSecondCard() != nullptr
                            && cell->getSecondCard()->getTeacher() == teacher)
                    {
                        cell->clearSecond();
                    }
                    if (cell->getFirstCard() != nullptr
                            && cell->getFirstCard()->getTeacher() == teacher)
                    {
                        cell->clearFirst(false);
                    }
                }
            }
        }

        //Изменяю поля учителя, путем функции редактирования
        teacher->edit(name2, name1, name3, inputDialog->disciplineMap, inputDialog->lessonList);
        if (teacher->getBackgroundColor() != inputDialog->teacherBackgroundColor)
        {
            teacher->setBackgroundColor(inputDialog->teacherBackgroundColor);
            QMessageBox warningBox(QMessageBox::Question,
                               teacher->getShortName(),
                               "Перекрасить карточки с данным учителем?",
                               0);
            warningBox.addButton(QString::fromUtf8("Да"),
                             QMessageBox::AcceptRole);
            warningBox.addButton(QString::fromUtf8("Нет"),
                             QMessageBox::RejectRole);
            if (warningBox.exec() == QMessageBox::AcceptRole)
            {
                for (int itemIndex = 0; itemIndex < cardList->count(); itemIndex++)
                {
                    Card *card = (Card*)(cardList->itemWidget(cardList->item(itemIndex)));
                    if (card->getTeacher() == teacher)
                    {
                       card->setBackgroundColor(teacher->getBackgroundColor());
                    }
                }
            }
        }
        if(nameLabel != nullptr && countButton != nullptr)
        {
            if (name1.length() > 15)
            {
                nameLabel->setText(name1.left(12) + "..." + " " + name2.left(1) + "." + name3.left(1) + ".");
            }
            else
            {
                nameLabel->setText(name1 + " " + name2.left(1) + "." + name3.left(1) + ".");
            }
             countButton->setText(QString::number(teacher->disciplineMap.count()));
             QString tipStr;
             for (Discipline* dis : teacher->disciplineMap.keys())
             {
                 tipStr += dis->getName() + "<br>";
             }
             tipStr.remove(tipStr.length()-4,4);
             countButton->setToolTip(tipStr);
        }
    }
}

void TeacherDialog::deleteTeacher(Teacher *teacher, QList<QWidget*> wgtList)
//Удалить учителя
{
    //Проверяю не нахоидтся ли карточки с данным учителем в таблице
    int cardCount = 0;
    for (int itemIndex = 0; itemIndex < cardList->count(); itemIndex++)
    {
        Card *card = (Card*)(cardList->itemWidget(cardList->item(itemIndex)));
        if (card->getTeacher() == teacher)
        {
            cardCount += (card->getMaxCount() - card->getCurrentCount());
        }
    }

    QString str;
    if(cardCount != 0)
    {
        str = "В таблице присутствуют карточки с данным учителем!<br> Количество карточек: <b>" + QString::number(cardCount) +"</b><br>Удалить учителя?";
    }
    else
    {
        str = "Удалить учителя?";
    }
    QMessageBox warningBox(QMessageBox::Question,
                       teacher->getShortName(),
                       str,
                       0);
    warningBox.addButton(QString::fromUtf8("Да"),
                     QMessageBox::AcceptRole);
    warningBox.addButton(QString::fromUtf8("Нет"),
                     QMessageBox::RejectRole);
    if (cardCount > 0)
    {
        warningBox.setIcon(QMessageBox::Warning);
    }
    if (warningBox.exec() == QMessageBox::AcceptRole)
    {
        for (Day *day : cardTable->dayList)
        {
            for (Lesson *lesson : day->lessonList)
            {
                for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
                {
                    if (cell->getSecondCard() != nullptr
                            && cell->getSecondCard()->getTeacher() == teacher)
                    {
                        cell->clearSecond();
                    }
                    if (cell->getFirstCard() != nullptr
                            && cell->getFirstCard()->getTeacher() == teacher)
                    {
                        cell->clearFirst(false);
                    }
                }

            }
        }
        teacher->deleted();
        directory->teacherList.removeOne(teacher);
        for (QWidget *wgt : wgtList)
        {
            delete wgt;
        }
    }
}

void TeacherDialog::createTeacher()
//Создать нового учителя
{
    TeacherEditDialog *inputDialog = new TeacherEditDialog(cardTable, directory, cardList, false);
    inputDialog->setWindowTitle("Создание учителя");
    if (inputDialog->exec() == QDialog::Accepted)
    {
        Teacher *newTeacher = new Teacher(directory);

        newTeacher->setSecondName(inputDialog->secondNameLine->text());
        newTeacher->setFirstName(inputDialog->firstNameLine->text());
        newTeacher->setThirdName(inputDialog->thirdNameLine->text());
        newTeacher->disciplineMap = inputDialog->disciplineMap;
        newTeacher->lessonList = inputDialog->lessonList;
        newTeacher->setBackgroundColor(inputDialog->teacherBackgroundColor);

        QWidget *wgt = addTeacher(newTeacher);
        directory->addTeacher(newTeacher);

        for(Discipline *newDiscipline : newTeacher->disciplineMap.keys())
        {
                for (Class *newClass : newTeacher->disciplineMap[newDiscipline].keys())
                {
                    newTeacher->classAdded(newDiscipline, newClass);
                }
        }
        if (height() < 500)
        {
            resize(width(), height()+25);
        }

        DialogFeatures::sort(scrollLayout);
        teacherScrollArea->verticalScrollBar()->setMaximum(teacherScrollArea->verticalScrollBar()->maximum()+25);
        DialogFeatures::scrollTo(wgt, teacherScrollArea, scrollLayout);
    }
}
