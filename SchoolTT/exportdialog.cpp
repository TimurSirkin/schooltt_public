#include "exportdialog.h"
#include "ui_exportdialog.h"
#include <QDebug>
#include "exportoptions.h"

ExportDialog::ExportDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExportDialog)
{
    ui->setupUi(this);
    setWindowTitle("Настройка  файла xlsx");
    setFixedSize(size());
    initDialog();
}

ExportDialog::~ExportDialog()
{
    delete ui;
}

int ExportDialog::getDisciplineHeight()
{
    return ui->heightSpinBox1->value();
}

int ExportDialog::getDisciplineWidth()
{
    return ui->widthSpinBox1->value();
}

int ExportDialog::getDisciplineFontSize()
{
    return ui->fontSpinBox1->value();
}

bool ExportDialog::getDisciplineCursive()
{
    return ui->cursiveCheckBox1->isChecked();
}

bool ExportDialog::getDisciplineBold()
{
    return ui->boldCheckBox1->isChecked();
}

int ExportDialog::getRoomHeight()
{
    return ui->heightSpinBox2->value();
}

int ExportDialog::getRoomWidth()
{
    return ui->widthSpinBox2->value();
}

int ExportDialog::getRoomFontSize()
{
    return ui->fontSpinBox2->value();
}

bool ExportDialog::getRoomCursive()
{
    return ui->cursiveCheckBox2->isChecked();
}

bool ExportDialog::getRoomBold()
{
    return ui->boldCheckBox2->isChecked();
}

int ExportDialog::getClassHeight()
{
    return ui->heightSpinBox3->value();
}

int ExportDialog::getClassWidth()
{
    return ui->widthSpinBox3->value();
}

int ExportDialog::getClassFontSize()
{
    return ui->fontSpinBox3->value();
}

bool ExportDialog::getClassCursive()
{
    return ui->cursiveCheckBox3->isChecked();
}

bool ExportDialog::getClassBold()
{
    return ui->boldCheckBox3->isChecked();
}

int ExportDialog::getDayHeight()
{
    return ui->heightSpinBox4->value();
}

int ExportDialog::getDayWidth()
{
    return ui->widthSpinBox4->value();
}

int ExportDialog::getDayFontSize()
{
    return ui->fontSpinBox4->value();
}

bool ExportDialog::getDayCursive()
{
    return ui->cursiveCheckBox4->isChecked();
}

bool ExportDialog::getDayBold()
{
    return ui->boldCheckBox4->isChecked();
}

int ExportDialog::getLessonHeight()
{
    return ui->heightSpinBox5->value();
}

int ExportDialog::getLessonWidth()
{
    return ui->widthSpinBox5->value();;
}

int ExportDialog::getLessonFontSize()
{
    return ui->fontSpinBox5->value();
}

bool ExportDialog::getLessonCursive()
{
    return ui->cursiveCheckBox5->isChecked();
}

bool ExportDialog::getLessonBold()
{
    return ui->boldCheckBox5->isChecked();
}

QString ExportDialog::getLeftTopText()
{
     return ui->leftTopTextEdit->toPlainText();
}

QString ExportDialog::getCentralTopText()
{
     return ui->centralTopTextEdit->toPlainText();
}

QString ExportDialog::getRightTopText()
{
    return ui->rightTopTextEdit->toPlainText();
}

QString ExportDialog::getLeftBotText()
{
     return ui->leftBotTextEdit->toPlainText();
}

QString ExportDialog::getCentralBotText()
{
     return ui->centralBotTextEdit->toPlainText();
}

QString ExportDialog::getRightBotText()
{
     return ui->rightBotTextEdit->toPlainText();
}

void ExportDialog::initDialog()
{
    ui->tableLayout->setAlignment(ui->cursiveCheckBox1, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->cursiveCheckBox2, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->cursiveCheckBox3, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->cursiveCheckBox4, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->cursiveCheckBox5, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->boldCheckBox1, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->boldCheckBox2, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->boldCheckBox3, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->boldCheckBox4, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->boldCheckBox5, Qt::AlignCenter);
}

void ExportDialog::setOptions(ExportOptions *exportOptions)
{
    ui->heightSpinBox1->setValue(exportOptions->disciplineHeight);
    ui->widthSpinBox1->setValue(exportOptions->disciplineWidth);
    ui->fontSpinBox1->setValue(exportOptions->disciplineFontSize);
    ui->cursiveCheckBox1->setChecked(exportOptions->disciplineCursive);
    ui->boldCheckBox1->setChecked(exportOptions->disciplineBold);

    ui->widthSpinBox2->setValue(exportOptions->roomWidth);
    ui->fontSpinBox2->setValue(exportOptions->roomFontSize);
    ui->cursiveCheckBox2->setChecked(exportOptions->roomCursive);
    ui->boldCheckBox2->setChecked(exportOptions->roomBold);

    ui->heightSpinBox3->setValue(exportOptions->classHeight);
    ui->fontSpinBox3->setValue(exportOptions->classFontSize);
    ui->cursiveCheckBox3->setChecked(exportOptions->classCursive);
    ui->boldCheckBox3->setChecked(exportOptions->classBold);

    ui->widthSpinBox4->setValue(exportOptions->dayWidth);
    ui->fontSpinBox4->setValue(exportOptions->dayFontSize);
    ui->cursiveCheckBox4->setChecked(exportOptions->dayCursive);
    ui->boldCheckBox4->setChecked(exportOptions->dayBold);

    ui->widthSpinBox5->setValue(exportOptions->lessonWidth);
    ui->fontSpinBox5->setValue(exportOptions->lessonFontSize);
    ui->cursiveCheckBox5->setChecked(exportOptions->lessonCursive);
    ui->boldCheckBox5->setChecked(exportOptions->lessonBold);

    ui->leftTopTextEdit->setText(exportOptions->topLeftText);
    ui->centralTopTextEdit->setText(exportOptions->topCentralText);
    ui->rightTopTextEdit->setText(exportOptions->topRightText);
    ui->leftBotTextEdit->setText(exportOptions->botLeftText);
    ui->centralBotTextEdit->setText(exportOptions->botCentralText);
    ui->rightBotTextEdit->setText(exportOptions->botRightText);

    }

void ExportDialog::on_heightSpinBox1_valueChanged(int value)
{
    ui->heightSpinBox2->setValue( value);
    ui->heightSpinBox4->setValue( value * 7);
    ui->heightSpinBox5->setValue( value);
}

void ExportDialog::on_widthSpinBox1_valueChanged(int value)
{
     ui->widthSpinBox3->setValue( value + ui->widthSpinBox2->value());
}

void ExportDialog::on_widthSpinBox2_valueChanged(int value)
{
     ui->widthSpinBox3->setValue( value + ui->heightSpinBox1->value());
}

void ExportDialog::on_exportButton_clicked()
{
    accept();
}
