#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QHBoxLayout>
#include <QScrollBar>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QMenu>
#include <QLineEdit>
#include <QWidget>
#include <QDebug>
#include <QLayout>
#include <QScrollArea>
#include <QListWidget>
#include <QLabel>
#include "xlsxdocument.h"
#include "cardlist.h"
#include "cardoptionwindow.h"
#include "newfiledialog.h"
#include "cardtable.h"
#include "day.h"
#include "teacher.h"
#include "discipline.h"
#include "class.h"
#include "card.h"
#include "cell.h"
#include "startwidget.h"
#include "optionswindow.h"
#include "disciplinedialog.h"
#include "directory.h"
#include "newfilebydialog.h"
#include "roomsdialog.h"
#include "classdialog.h"
#include "styles.h"
#include "teacherdialog.h"
#include "exportdialog.h"
#include "exportteacherdialog.h"

//Главное окно
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
  //Конструктор, список а таблица создаются в форме, может стоит их тоже перенсти в код
{
    ui->setupUi(this);
    exportOptions = new ExportOptions(this);
    ui->statusBar->setStyleSheet("color: lightblue");
    setWindowIcon(QIcon(":/new/resource/Images/label.png"));
    ui->customSortButton->setArrowEnadbled(false);

    //Ставлю картинки на кнопки сортировки
    {
    QPixmap pix = QPixmap(":/new/resource/Images/Tea_down.png");
    ui->teacherSortButton->setFirstPix(pix);
    pix = QPixmap(":/new/resource/Images/Tea_up.png");
    ui->teacherSortButton->setSecondPix(pix);
    pix = QPixmap(":/new/resource/Images/Tea_down_hover.png");
    ui->teacherSortButton->setHover_FirstPix(pix);
    pix = QPixmap(":/new/resource/Images/Tea_up_hover.png");
    ui->teacherSortButton->setHover_SecondPix(pix);
    }
    {
    QPixmap pix = QPixmap(":/new/resource/Images/Class_down.png");
    ui->classSortButton->setFirstPix(pix);
    pix = QPixmap(":/new/resource/Images/Class_up.png");
    ui->classSortButton->setSecondPix(pix);
    pix = QPixmap(":/new/resource/Images/Class_down_hover.png");
    ui->classSortButton->setHover_FirstPix(pix);
    pix = QPixmap(":/new/resource/Images/Class_up_hover.png");
    ui->classSortButton->setHover_SecondPix(pix);
    }
    {
    QPixmap pix = QPixmap(":/new/resource/Images/Dis_down.png");
    ui->disciplineSortButton->setFirstPix(pix);
    pix = QPixmap(":/new/resource/Images/Dis_up.png");
    ui->disciplineSortButton->setSecondPix(pix);
    pix = QPixmap(":/new/resource/Images/Dis_down_hover.png");
    ui->disciplineSortButton->setHover_FirstPix(pix);
    pix = QPixmap(":/new/resource/Images/Dis_up_hover.png");
    ui->disciplineSortButton->setHover_SecondPix(pix);
    }
    {
    QPixmap pix = QPixmap(":/new/resource/Images/Custom_sort.svg");
    ui->customSortButton->setFirstPix(pix);
    pix = QPixmap(":/new/resource/Images/Custom_sort_hover.svg");
    ui->customSortButton->setHover_FirstPix(pix);
    }


    cardTable = ui->mainTable;
    cardList = ui->mainList;

    QLayout *newLayout = new QHBoxLayout();
    ui->filterButton->setLayout(newLayout);
    QLabel *newLabel = new QLabel("Фильтр");
    newLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
    newLayout->addWidget(newLabel);
    newLayout->setContentsMargins(45, 0, 5, 0);
    newLayout->setAlignment(newLabel, Qt::AlignCenter);
    newLayout->addWidget(filterBox);
    filterBox->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    newLayout->setAlignment(filterBox, Qt::AlignRight);
    connect(filterBox, SIGNAL(stateChanged(int)), this, SLOT(slot_checkFilter(int)));
    ui->filterButton->setFixedHeight(20);

    setWindowTitle("Расписание");

    //Создаю менюшку
    QMenu *file = new QMenu("Файл");

    QMenu *newFileMenu = new QMenu("Создать расписание...");
    file->addMenu(newFileMenu);

    connect(action_new, &QAction::triggered, [this] () {slot_newFile();});
    newFileMenu->addAction(action_new);
    action_new->setShortcut(QKeySequence("CTRL+N"));

    connect(action_newBy, &QAction::triggered, [this] () {slot_newFileBy();});
    newFileMenu->addAction(action_newBy);
    action_newBy->setShortcut(QKeySequence("CTRL+SHIFT+N"));

    file->addSeparator();

    connect(action_save, &QAction::triggered, [this] () {slot_saveFile();});
    file->addAction(action_save);
    action_save->setShortcut(QKeySequence("CTRL+S"));

    connect(action_saveAs, &QAction::triggered, [this] () {slot_saveFileAs();});
    file->addAction(action_saveAs);
    action_saveAs->setShortcut(QKeySequence("CTRL+SHIFT+S"));

    file->addSeparator();

    connect(action_open, &QAction::triggered, [this] () {slot_openFile();});
    file->addAction(action_open);
    action_open->setShortcut(QKeySequence("CTRL+O"));

    file->addSeparator();

    QMenu *exportMenu = new QMenu("Экспортировать в xlsx...");
    file->addMenu(exportMenu);

    connect(action_exportToXls, &QAction::triggered, [this] () {slot_exportToXlsx();});
    exportMenu->addAction(action_exportToXls);
    action_exportToXls->setShortcut(QKeySequence("CTRL+P"));

    connect(action_exportToTeacherXls, &QAction::triggered, [this] () {slot_exportToTeacherXlsx();});
    exportMenu->addAction(action_exportToTeacherXls);
    action_exportToTeacherXls->setShortcut(QKeySequence("CTRL+T"));

    menuBar()->addMenu(file);
    options->addAction(QIcon(":/new/resource/Images/Settings.png"),"Настройки таблицы...", this, SLOT(OpenTableOptionsWindow()));
    options->addAction(QIcon(":/new/resource/Images/Settings.png"),"Настройки карточек...", this, SLOT(OpenCardOptionsWindow()));
    menuBar()->addMenu(options);

    directoryes->addAction(QIcon(":/new/resource/Images/Tea.png"), "Справочник учителей...", this, SLOT(OpenTeachersDirectoryWindow()));
    directoryes->addAction(QIcon(":/new/resource/Images/Class.png"), "Справочник классов...", this, SLOT(OpenClassesDirectoryWindow()));
    directoryes->addAction(QIcon(":/new/resource/Images/Dis.png"), "Справочник предметов...", this, SLOT(OpenDisciplinesDirectoryWindow()));
    menuBar()->addMenu(directoryes);

    //Инициаизация элементов
    connect(cardList, SIGNAL(mousePressed()), cardTable, SLOT(onUnfocused()));
    ui->filterButton->setEnabled(false);
    ui->customSortButton->setEnabled(false);
    ui->teacherSortButton->setEnabled(false);
    ui->disciplineSortButton->setEnabled(false);
    ui->classSortButton->setEnabled(false);
    cardList->setEnabled(false);
    action_save->setEnabled(false);
    action_saveAs->setEnabled(false);
    action_exportToXls->setEnabled(false);
    action_exportToTeacherXls->setEnabled(false);
    directoryes->setEnabled(false);
    options->setEnabled(false);

    ui->statusBar->showMessage("Приложение готово к работе", 5000);
}

MainWindow::~MainWindow()
//Деструктор
{
    delete ui;
}

void MainWindow::initialCardList()
//Создать карточки - пробегаюсь по справочнику учителей, создаю соответствующие карточки в ЛистБоксе
{
    for (Teacher *teacher : cardTable->directory->teacherList)
    {
        for (Discipline *discipline : teacher->disciplineMap.keys())
        {
            for (Class *_class : teacher->disciplineMap[discipline].keys())
            {
                createCard(teacher, _class, discipline, teacher->disciplineMap[discipline][_class]);
            }
        }
    }
}

void MainWindow::createCard(Teacher *pTeacher, Class *pClass, Discipline *pDiscipline, int pCount)
//Создать карточку соотв. данному учителю-предмету-классу-количеству
{
    Card *newCard = new Card(pTeacher, pClass, pDiscipline, pCount, cardTable, cardTable->directory, cardList);
    cardList->addCard(newCard);

    //Связваю карточку и учителя. Учитель высылает сигналы об изменении его словаря предметов, карточка риагирует.
    //Связь для создания новых карточек, происходит в конструкторе учителя
    connect(pTeacher, SIGNAL(disciplineDeleted(Discipline*)), newCard, SLOT(checkDiscipline(Discipline*)));
    connect(pTeacher, SIGNAL(classDeleted(Discipline*,Class*)), newCard, SLOT(checkClass(Discipline*,Class*)));
    connect(newCard, &Card::deleteRequest, [=](){deleteCard(newCard);});
    connect(pTeacher, SIGNAL(countChanged(Discipline*,Class*,int)), newCard, SLOT(checkCount(Discipline*,Class*,int)));

    connect(newCard, SIGNAL(mousePressed()), cardTable, SLOT(onUnfocused()));
    connect(newCard, SIGNAL(dragStarted()), cardTable, SLOT(onCardDragStarted()));
    connect(newCard, SIGNAL(dragStoped()), cardTable, SLOT(onCardDragStoped()));

    //Не просто конэкчу, а еще и запоминаю их, даб потом к ним обратиться для дисконекта (при удалении)
    //Штука в том, что через обычный дисконект все хорошо с сигналами с слотами, а вот с лямбда выр-ми - все плохо
    newCard->DisciplineConnection = connect(pDiscipline, &Discipline::nameChanged, [=]() {newCard->setDiscipline(newCard->getDiscipline());});
    newCard->ClassConnection = connect(pClass, &Class::nameChanged, [=]() {newCard->setClass(newCard->getClass());});
    newCard->TeacherNameConnection = connect(pTeacher, &Teacher::nameChanged, [=]() {newCard->setTeacher(newCard->getTeacher());});
    newCard->TeacherDeleteConnection = connect(pTeacher, &Teacher::deleted, [=]() {deleteCard(newCard);});
}

void MainWindow::deleteCard(Card *card)
//Удалить карточку
{
    card->disconnect(card->DisciplineConnection);
    card->disconnect(card->ClassConnection);
    card->disconnect(card->TeacherNameConnection);
    card->disconnect(card->TeacherDeleteConnection);
    cardList->deleteCard(card);
}

void MainWindow::fillDisciplineDirectory()
{
    QString fileName = QFileDialog::getOpenFileName(this,
            tr("Загрузить шаблон"), "",
            tr("Все файлы (*)"));
    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    while (!file.atEnd())
    {
         QString str = file.readLine();
         str = str.left(str.length()-1);
         if (str != "")
         {
              cardTable->directory->addDiscipline(str);
         }
    }
}

void MainWindow::restart()
{
    //Чищу ЛистБокс
    cardList->refresh();

    //Удаляю таблицу карточек
   delete ui->scrollArea->widget();
   cardTable = new CardTable(this);
   cardList->setCardTable(cardTable);
   //Создаю новую таблицу
   ui->scrollArea->setWidget(cardTable);

   //Инициализирую все элементы по-новой
   initCardTable();
   initSortButtons();

   action_save->setEnabled(true);
   action_saveAs->setEnabled(true);
   action_exportToXls->setEnabled(true);
   action_exportToTeacherXls->setEnabled(true);
   directoryes->setEnabled(true);
   options->setEnabled(true);
   ui->filterButton->setEnabled(true);
   ui->customSortButton->setEnabled(true);
   ui->teacherSortButton->setEnabled(true);
   ui->disciplineSortButton->setEnabled(true);
   ui->classSortButton->setEnabled(true);
   cardList->setEnabled(true);
}

void MainWindow::save()
{
    currentFile.open(QIODevice::WriteOnly);

    saveDays();
    saveClasses();
    saveDisciplines();
    saveTeachers();
    saveSettings();
    saveCells();
    saveCards();
    saveFilter();
    saveExportOptions();

    currentFile.close();
    ui->statusBar->showMessage("Расписание сохранено", 5000);
}

void MainWindow::saveAs()
{
    QString str = QFileDialog::getSaveFileName(this, tr("Сохранить расписание"),currentFile.fileName().left(currentFile.fileName().length()-4),
                                               tr("Расписание (*.stt);; Все файлы (*)"));
    if(str.right(4) != ".stt")
    {
        str += ".stt";
    }
    saveAs(str);
}

void MainWindow::saveAs(QString fileName)
{
    if (fileName != "")
    {
        setCurrentFile(fileName);
        save();
    }
}

void MainWindow::open(QString fileName)
{
    restart();

    QFile file(fileName);
    file.open(QIODevice::ReadOnly);

    bool isCardListInit = false;
    while (!file.atEnd())
    {
        QString str = file.readLine();
        str = str.left(str.length()-1);

        if (str == "<Day>")
        {
            loadDay(&file);
        }
        else if (str == "<Class>")
        {
            loadClass(&file);
        }
        else if (str == "<Discipline>")
        {
            loadDiscipline(&file);
        }
        else if (str == "<Teacher>")
        {
            loadTeacher(&file);
        }
        else if (str == "<Cell>")
        {
            if (!isCardListInit)
            {
                initialCardList();
                isCardListInit = true;
            }
            loadCell(&file);
        }
        else if (str == "<Card>")
        {
            if (!isCardListInit)
            {
                initialCardList();
                isCardListInit = true;
            }
            loadCard(&file);
        }
        else if (str == "<Settings>")
        {
            loadSettings(&file);
        }
        else if (str == "<Filter>")
        {
            loadFilter(&file);
        }
        else if (str == "<Export>")
        {
            loadExportOptions(&file);
        }
    }

    file.close();
    setCurrentFile(fileName);
    ui->statusBar->showMessage("Расписание загружено", 5000);
}

void MainWindow::initCardTable()
{
    connect(cardTable->directory, &Directory::classAdded, [this](Teacher* teacher, Discipline* discipline, Class* _class)
    {createCard(teacher, _class, discipline, teacher->disciplineMap[discipline][_class]);});
    connect(cardTable, SIGNAL(focused(Cell*)), cardList, SLOT(slot_showWithClassAndLesson(Cell*)));
    connect(cardTable, SIGNAL(focused(Class*)), cardList, SLOT(slot_showWithClass(Class*)));
    connect(cardTable, SIGNAL(focused(Day*)), cardList, SLOT(slot_showWithDay(Day*)));
    connect(cardTable, SIGNAL(focused(Lesson*)), cardList, SLOT(slot_showWithLesson(Lesson*)));
    connect(cardTable, &CardTable::moveHScrollBar, [this] (int value)
    {ui->scrollArea->verticalScrollBar()->setValue(ui->scrollArea->verticalScrollBar()->value() + value);});
    connect(cardTable, SIGNAL(unfocused()), cardList, SLOT(slot_showAll()));
    connect(cardTable, SIGNAL(signal_classSwaped()), cardList, SLOT(slot_updateSort()));
}

void MainWindow::initFilter()
{
}

void MainWindow::initSortButtons()
{
    ui->customSortButton->setActivated(false);

    ui->teacherSortButton->setActivated(false);
    ui->teacherSortButton->setTopToBottom(true);
    ui->teacherSortButton->setIcon(ui->teacherSortButton->getFirstPix());

    ui->disciplineSortButton->setActivated(false);
    ui->disciplineSortButton->setTopToBottom(true);
    ui->disciplineSortButton->setIcon(ui->disciplineSortButton->getFirstPix());

    ui->classSortButton->setActivated(false);
    ui->classSortButton->setTopToBottom(true);
    ui->classSortButton->setIcon(ui->classSortButton->getFirstPix());
}

void MainWindow::initNewFileBy()
{
    NewFileByDialog *dialog = new NewFileByDialog();
    if (dialog->exec() == QDialog::Accepted)
    {
        restart();

        QFile file(dialog->fileName);
        file.open(QIODevice::ReadOnly);

        bool isCardListInit = false;
        while (!file.atEnd())
        {
            QString str = file.readLine();
            str = str.left(str.length()-1);

            if (str == "<Day>")
            {
                loadDay(&file);
            }
            else if (str == "<Class>")
            {
                loadClass(&file);
            }
            else if (str == "<Discipline>")
            {
                loadDiscipline(&file);
            }
            else if (str == "<Teacher>")
            {
                loadTeacher(&file);
            }
            else if (str == "<Card>")
            {
                if (!isCardListInit)
                {
                    initialCardList();
                    isCardListInit = true;
                }
                loadCard(&file);
            }
            else if (str == "<Settings>")
            {
                loadSettings(&file);
            }
            else if (str == "<Filter>")
            {
                loadFilter(&file);
            }
        }

        file.close();
        setCurrentFile(file.fileName());

        QString str = "";
        str += dialog->pathLine->text();
        str += "/";
        str += dialog->nameLine->text();
        saveAs(str + ".stt");
    }
}

bool MainWindow::strToBool(QString str)
{
    if (str == "true") return true;
    return false;
}

QString MainWindow::boolToStr(bool flag)
{
    if (flag == true) return "true";
    return "false";
}

void MainWindow::initNewFile()
{
    NewFileDialog *dialog = new NewFileDialog();
    if (dialog->exec() == QDialog::Accepted)
    {
        restart();
        int dayCount = dialog->daySpin->value();
        int lessonCount = dialog->lessonSpin->value();
        int classCount = dialog->classSpin->value();
        cardTable->addDays(dayCount,lessonCount);
        cardTable->addClasses(classCount);
        ui->scrollArea->verticalScrollBar()->setValue(0);
        ui->scrollArea->horizontalScrollBar()->setValue(0);
        ui->customSortButton->setActivated(true);

        QString str = "";
        str += dialog->pathLine->text();
        str += "/";
        str += dialog->nameLine->text();
        saveAs(str+ ".stt");

        QMessageBox questionBox(QMessageBox::Question,
                           "Новое расписание",
                           "Заполнить справочник предметов по шаблону?",
                           nullptr);
        questionBox.addButton(QString::fromUtf8("Да"),
                         QMessageBox::AcceptRole);
        questionBox.addButton(QString::fromUtf8("Нет"),
                         QMessageBox::RejectRole);
        if (questionBox.exec() == QMessageBox::AcceptRole)
        {
            fillDisciplineDirectory();
        }
    }
    ui->statusBar->showMessage("Расписание создано", 5000);
}

void MainWindow::saveTeachers()
{
    QTextStream stream(&currentFile);
    for (Teacher *teacher : cardTable->directory->teacherList)
    {
        stream << "<Teacher>\n";

        stream << teacher->getId() <<"\n";
        stream << teacher->getSecondName() <<"\n";
        stream << teacher->getFirstName() << "\n";
        stream << teacher->getThirdName() << "\n";
        stream << teacher->getBackgroundColor().name() << "\n";

        for (Discipline *discipline : teacher->disciplineMap.keys())
        {
            //qDebug()<<"3";
            stream << "<Discipline>\n";
            //qDebug()<<"-";
            stream << discipline->getName() << "\n";
            //qDebug()<<"4";
            for(Class *_class : teacher->disciplineMap[discipline].keys())
            {
                //qDebug()<<"5";
                stream << "<Class>\n";
                stream << _class->getName() << "\n";
                stream << teacher->disciplineMap[discipline][_class] << "\n";
                stream << "</Class>\n";
                //qDebug()<<"6";
            }
            stream << "</Discipline>\n";
            //qDebug()<<"7";
        }

        stream << "<Worktime>\n";
        for (Day *day : cardTable->dayList)
        {
            stream << "<Day>\n";
            stream << day->getName() << "\n";
            for (Lesson *lesson : teacher->lessonList)
            {
                if (day->lessonList.contains(lesson))
                {
                    stream << lesson->getName() << "\n";
                }
            }
            stream << "</Day>\n";
        }
        stream << "</Worktime>\n";

        stream << "</Teacher>\n";
    }
}

void MainWindow::saveClasses()
{
    QTextStream stream(&currentFile);
    for (Class *_class : cardTable->classList)
    {
        stream << "<Class>\n";
        stream << _class->getName() << "\n";
        stream << _class->getCapacity() << "\n";
        if (_class->isHidden())
        {
            stream << "-\n";
        }
        else
        {
            stream << "+\n";
        }
        stream << "</Class>\n";
    }
}

void MainWindow::saveDisciplines()
{
    QTextStream stream(&currentFile);
    for (Discipline *discipline : cardTable->directory->disciplineList)
    {
        stream << "<Discipline>\n";
        stream << discipline->getName() <<"\n";
        stream << discipline->getExportName() <<"\n";
        stream << discipline->getExportForDoubleName() <<"\n";
        stream << "</Discipline>\n";
    }
}

void MainWindow::saveDays()
{
    QTextStream stream(&currentFile);
    for (Day *day : cardTable->dayList)
    {
        stream << "<Day>\n";
        stream << day->lessonList.count() <<"\n";
        stream << "</Day>\n";
    }
}

void MainWindow::saveCells()
{
    QTextStream stream(&currentFile);
    for (Day *day : cardTable->dayList)
    {
        for (Lesson *lesson : day->lessonList)
        {
            for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
            {
                if (!cell->isEmpty())
                {
                    Card *firstCard = cell->getFirstCard();
                    stream << "<Cell>\n";
                    stream << firstCard->getTeacher()->getId() << "\n";
                    stream << firstCard->getDiscipline()->getName() << "\n";
                    stream << firstCard->getClass()->getName() << "\n";
                    stream << day->getName() << "\n";
                    stream << lesson->getName() << "\n";
                    stream << "1" << "\n";
                    stream << "</Cell>\n";

                    if (cell->getSecondCard() != nullptr)
                    {
                        Card *secondCard = cell->getSecondCard();
                        stream << "<Cell>\n";
                        stream << secondCard->getTeacher()->getId() << "\n";
                        stream << secondCard->getDiscipline()->getName() << "\n";
                        stream << secondCard->getClass()->getName() << "\n";
                        stream << day->getName() << "\n";
                        stream << lesson->getName() << "\n";
                        stream << "2" << "\n";
                        stream << "</Cell>\n";
                    }
                }
            }
        }
    }
}

void MainWindow::saveCards()
{
    QTextStream stream(&currentFile);
    for (Card* card : cardList->customList.keys())
    {
        stream << "<Card>\n";
        stream << card->getTeacher()->getId() << "\n";
        stream << card->getDiscipline()->getName() << "\n";
        stream << card->getClass()->getName() << "\n";
        stream << card->getBackgroundColor().name() << "\n";
        stream << card->getForegroundColor().name() << "\n";
        stream << cardList->customList[card] << "\n";
        stream << "</Card>\n";
    }
}

void MainWindow::saveSettings()
{
    QTextStream stream(&currentFile);
    stream << "<Settings>\n";
    stream << cardTable->class_rowHeight << "\n";
    stream << cardTable->rowHeight << "\n";
    stream << cardTable->spacerHeight << "\n";

    stream << cardTable->day_columnWidth << "\n";
    stream << cardTable->lesson_columnWidth << "\n";
    stream << cardTable->columnWidth << "\n";

    stream << cardTable->minCellHeightForText << "\n";
    stream << cardTable->minCellWidthForText << "\n";
    stream << cardTable->minCellHeightForDoubleText << "\n";

    stream << cardTable->getTopFlag() << "\n";
    stream << cardTable->getBotFlag() << "\n";
    stream << cardTable->getTipFlag() << "\n";

    stream << cardList->getSort() << "\n";
    if (cardList->getIsTtB())
    {
         stream << "true\n";
    }
    else
    {
         stream << "false\n";
    }

    stream << "</Settings>\n";
}

void MainWindow::saveFilter()
{
    QTextStream stream(&currentFile);
    stream << "<Filter>\n";

    if (cardList->getIsFilterOn())
    {
        stream << "true\n";
    }
    else
    {
        stream << "false\n";
    }

    if (cardList->isCardVisible())
    {
        stream << "true\n";
    }
    else
    {
        stream << "false\n";
    }

    stream << "<Teachers>\n";
    for (Teacher *teacher : cardList->teacherFilterList)
    {
        stream << teacher->getId() << "\n";
    }
    stream << "</Teachers>\n";

    stream << "<Disciplines>\n";
    for (Discipline *discipline : cardList->disciplineFilterList)
    {
        stream << discipline->getName() << "\n";
    }
    stream << "</Disciplines>\n";

    stream << "<Classes>\n";
    for (Class *_class : cardList->classFilterList)
    {
        stream << _class->getName() << "\n";
    }
    stream << "</Classes>\n";

    stream << "</Filter>\n";
}

void MainWindow::saveExportOptions()
{
    QTextStream stream(&currentFile);
    stream << "<Export>\n";

    stream << exportOptions->disciplineHeight << "\n";
    stream << exportOptions->disciplineWidth << "\n";
    stream << exportOptions->disciplineFontSize << "\n";
    stream << boolToStr(exportOptions->disciplineCursive) << "\n";
    stream << boolToStr(exportOptions->disciplineBold) << "\n";

    stream << exportOptions->roomWidth << "\n";
    stream << exportOptions->roomFontSize << "\n";
    stream << boolToStr(exportOptions->roomCursive) << "\n";
    stream << boolToStr(exportOptions->roomBold) << "\n";

    stream << exportOptions->classHeight << "\n";
    stream << exportOptions->classFontSize << "\n";
    stream << boolToStr(exportOptions->classCursive) << "\n";
    stream << boolToStr(exportOptions->classBold) << "\n";

    stream << exportOptions->dayWidth << "\n";
    stream << exportOptions->dayFontSize << "\n";
    stream << boolToStr(exportOptions->dayCursive) << "\n";
    stream << boolToStr(exportOptions->dayBold) << "\n";

    stream << exportOptions->lessonWidth << "\n";
    stream << exportOptions->lessonFontSize << "\n";
    stream << boolToStr(exportOptions->lessonCursive) << "\n";
    stream << boolToStr(exportOptions->lessonBold) << "\n";

    stream << exportOptions->t_classHeight << "\n";
    stream << exportOptions->t_classWidth << "\n";
    stream << exportOptions->t_classFontSize << "\n";
    stream << boolToStr(exportOptions->t_classCursive) << "\n";
    stream << boolToStr(exportOptions->t_classBold) << "\n";

    stream << exportOptions->t_headerFontSize << "\n";
    stream << boolToStr(exportOptions->t_headerCursive) << "\n";
    stream << boolToStr(exportOptions->t_headerBold) << "\n";

    stream << exportOptions->t_teacherWidth << "\n";
    stream << exportOptions->t_teacherFontSize << "\n";
    stream << boolToStr(exportOptions->t_teacherCursive) << "\n";
    stream << boolToStr(exportOptions->t_teacherBold) << "\n";

    stream << exportOptions->t_dayHeight << "\n";
    stream << exportOptions->t_dayFontSize << "\n";
    stream << boolToStr(exportOptions->t_dayCursive) << "\n";
    stream << boolToStr(exportOptions->t_dayBold) << "\n";

    stream << exportOptions->t_lessonHeight << "\n";
    stream << exportOptions->t_lessonFontSize << "\n";
    stream << boolToStr(exportOptions->t_lessonCursive) << "\n";
    stream << boolToStr(exportOptions->t_lessonBold) << "\n";

    stream << "<Title>\n";
    stream << exportOptions->topLeftText << "\n";
    stream << "<Title>\n";
    stream << exportOptions->topCentralText << "\n";
    stream << "<Title>\n";
    stream << exportOptions->topRightText << "\n";
    stream << "<Title>\n";
    stream << exportOptions->botLeftText << "\n";
    stream << "<Title>\n";
    stream << exportOptions->botCentralText << "\n";
    stream << "<Title>\n";
    stream << exportOptions-> botRightText << "\n";
    stream << "<Title>\n";

    stream << "</Export>\n";
}

void MainWindow::exportToXlsx(QString fileName, ExportOptions *exportOptions)
{
    QXlsx::Document xlsx;
    int rowIndex = 5;
    int columnIndex = 1;
    int columnLastIndex = 1;

    xlsx.setColumnWidth(columnIndex, columnIndex, exportOptions->dayWidth);
    columnIndex++;

    xlsx.setColumnWidth(columnIndex, columnIndex, exportOptions->lessonWidth);
    columnIndex++;

    for (Class *_class : cardTable->classList)
    {
        QString str = _class->getName();
        QXlsx::Format classStyle;
        classStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
        classStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        classStyle.setFontBold(exportOptions->classBold);
        classStyle.setFontItalic(exportOptions->classCursive);
        classStyle.setFontSize(exportOptions->classFontSize);
        classStyle.setBorderStyle(QXlsx::Format::BorderThin);
        xlsx.write(rowIndex, columnIndex, str, classStyle);
        xlsx.mergeCells(QXlsx::CellRange(rowIndex, columnIndex, rowIndex, columnIndex+1));
        xlsx.setColumnWidth(columnIndex, columnIndex, exportOptions->disciplineWidth);
        columnIndex ++;
        xlsx.setColumnWidth(columnIndex, columnIndex, exportOptions->roomWidth);
        columnIndex++;
    }
    columnLastIndex = columnIndex;

    QXlsx::Format topTextStyle;
    topTextStyle.setHorizontalAlignment(QXlsx::Format::AlignLeft);
    topTextStyle.setVerticalAlignment(QXlsx::Format::AlignTop);
    topTextStyle.setFontSize(10);
    xlsx.mergeCells(QXlsx::CellRange(1, 3, rowIndex-1, columnIndex/3));
    xlsx.write(1, 3, exportOptions->topLeftText, topTextStyle);
    topTextStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    xlsx.mergeCells(QXlsx::CellRange(1, columnIndex/3+1, rowIndex-1, columnIndex/3*2-1));
    xlsx.write(1, columnIndex/3+1, exportOptions->topCentralText, topTextStyle);
    topTextStyle.setHorizontalAlignment(QXlsx::Format::AlignRight);
    xlsx.mergeCells(QXlsx::CellRange(1, columnIndex/3*2, rowIndex-1, columnIndex-1));
    xlsx.write(1, columnIndex/3*2, exportOptions->topRightText, topTextStyle);

    xlsx.setRowHeight(rowIndex, rowIndex, exportOptions->classHeight);
    QXlsx::Format defaultStyle;
    defaultStyle.setBorderStyle(QXlsx::Format::BorderThin);
    rowIndex++;
    columnIndex = 1;

    for (Day *day : cardTable->dayList)
    {
        QXlsx::Format dayStyle;
        dayStyle.setRotation(90);
        dayStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
        dayStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
        dayStyle.setShrinkToFit(true);
        dayStyle.setFontBold(exportOptions->dayBold);
        dayStyle.setFontItalic(exportOptions->dayCursive);
        dayStyle.setFontSize(exportOptions->dayFontSize);
        dayStyle.setBorderStyle(QXlsx::Format::BorderThin);
        xlsx.write(rowIndex, columnIndex, day->getName(), dayStyle);
        int dayRowIndex = rowIndex;
        int dayColumnIndex = columnIndex;
        for (Lesson *lesson : day->lessonList)
        {
            columnIndex++;
            QXlsx::Format lessonStyle;
            lessonStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
            lessonStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
            lessonStyle.setFontBold(exportOptions->lessonBold);
            lessonStyle.setFontItalic(exportOptions->lessonCursive);
            lessonStyle.setFontSize(exportOptions->lessonFontSize);
            lessonStyle.setBorderStyle(QXlsx::Format::BorderThin);
            xlsx.write(rowIndex, columnIndex, lesson->getName(), lessonStyle);
            columnIndex++;
            for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
            {
                if (cell->getFirstCard() != nullptr)
                {
                    QXlsx::Format cellStyle;
                    cellStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
                    cellStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
                    cellStyle.setShrinkToFit(true);
                    cellStyle.setFontBold(exportOptions->disciplineBold);
                    cellStyle.setFontItalic(exportOptions->disciplineCursive);
                    cellStyle.setFontSize(exportOptions->disciplineFontSize);
                    cellStyle.setBorderStyle(QXlsx::Format::BorderThin);
                    QString str = cell->getFirstCard()->getDiscipline()->getName();
                    if (cell->getFirstCard()->getDiscipline()->getExportName()!="")
                    {
                        str = cell->getFirstCard()->getDiscipline()->getExportName();
                    }
                    xlsx.write(rowIndex, columnIndex, str, cellStyle);
                    if (cell->getSecondCard() != nullptr)
                    {
                        if (cell->getFirstCard()->getDiscipline()->getExportForDoubleName()!="")
                        {
                            str = cell->getFirstCard()->getDiscipline()->getExportForDoubleName();
                        }
                        else if (cell->getFirstCard()->getDiscipline()->getExportName()!="")
                        {
                            str = cell->getFirstCard()->getDiscipline()->getExportName();
                        }
                        else
                        {
                            str = cell->getSecondCard()->getDiscipline()->getName();
                        }

                        str += " / ";

                        if (cell->getSecondCard()->getDiscipline()->getExportForDoubleName()!="")
                        {
                            str += cell->getSecondCard()->getDiscipline()->getExportForDoubleName();
                        }
                        else if (cell->getSecondCard()->getDiscipline()->getExportName()!="")
                        {
                            str += cell->getSecondCard()->getDiscipline()->getExportName();
                        }
                        else
                        {
                            str += cell->getFirstCard()->getDiscipline()->getName();
                        }

                        xlsx.write(rowIndex, columnIndex, str, cellStyle);
                    }
                }
                columnIndex ++;
                QXlsx::Format roomStyle;
                roomStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
                roomStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
                roomStyle.setFontBold(exportOptions->roomBold);
                roomStyle.setFontItalic(exportOptions->roomCursive);
                roomStyle.setFontSize(exportOptions->roomFontSize);
                roomStyle.setFontSize(exportOptions->roomFontSize);
                roomStyle.setBorderStyle(QXlsx::Format::BorderThin);
                xlsx.write(rowIndex, columnIndex, "", roomStyle);
                columnIndex ++;
            }
            columnIndex = 1;
            rowIndex++;
        }

        xlsx.mergeCells(QXlsx::CellRange(dayRowIndex, dayColumnIndex, rowIndex-1, columnIndex));
    }

    xlsx.setRowFormat(5, rowIndex-1, defaultStyle);
    xlsx.setRowHeight(6, rowIndex-1, exportOptions->disciplineHeight);

    QXlsx::Format botTextStyle;
    botTextStyle.setHorizontalAlignment(QXlsx::Format::AlignLeft);
    botTextStyle.setVerticalAlignment(QXlsx::Format::AlignTop);
    botTextStyle.setFontSize(10);

    xlsx.mergeCells(QXlsx::CellRange(rowIndex, 3, rowIndex+3, columnLastIndex/3));
    xlsx.write(rowIndex, 3, exportOptions->botLeftText, botTextStyle);

    botTextStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    xlsx.mergeCells(QXlsx::CellRange(rowIndex, columnLastIndex/3+1, rowIndex+3, columnLastIndex/3*2-1));
    xlsx.write(rowIndex, columnLastIndex/3+1, exportOptions->botCentralText, botTextStyle);

    botTextStyle.setHorizontalAlignment(QXlsx::Format::AlignRight);
    xlsx.mergeCells(QXlsx::CellRange(rowIndex, columnLastIndex/3*2, rowIndex+3, columnLastIndex-1));
    xlsx.write(rowIndex, columnLastIndex/3*2, exportOptions->botRightText, botTextStyle);

    QString str = fileName;
    if(fileName.right(5) != ".xlsx")
    {
        str += ".xlsx";
    }
    xlsx.saveAs(str);

    ui->statusBar->showMessage("Расписание экспортировано", 5000);
}

void MainWindow::exportToTeacherXlsx(QString fileName, ExportOptions *exportOptions)
{
    QXlsx::Document xlsx;
    int rowIndex = 2;
    int columnIndex = 2;
    int dayColumnIndex = columnIndex;

    QXlsx::Format defaultStyle;
    defaultStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    defaultStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    defaultStyle.setBorderStyle(QXlsx::Format::BorderThin);
    QXlsx::Format dayStyle;
    dayStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    dayStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    dayStyle.setBorderStyle(QXlsx::Format::BorderThin);
    dayStyle.setFontBold(exportOptions->t_dayBold);
    dayStyle.setFontItalic(exportOptions->t_dayCursive);
    dayStyle.setFontSize(exportOptions->t_dayFontSize);
    QXlsx::Format lessonStyle;
    lessonStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    lessonStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    lessonStyle.setBorderStyle(QXlsx::Format::BorderThin);
    lessonStyle.setFontBold(exportOptions->t_lessonBold);
    lessonStyle.setFontItalic(exportOptions->t_lessonCursive);
    lessonStyle.setFontSize(exportOptions->t_lessonFontSize);
    QXlsx::Format classStyle;
    classStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    classStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    classStyle.setBorderStyle(QXlsx::Format::BorderThin);
    classStyle.setFontBold(exportOptions->t_classBold);
    classStyle.setFontItalic(exportOptions->t_classCursive);
    classStyle.setFontSize(exportOptions->t_classFontSize);
    QXlsx::Format teacherStyle;
    teacherStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    teacherStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    teacherStyle.setBorderStyle(QXlsx::Format::BorderThin);
    teacherStyle.setFontBold(exportOptions->t_teacherBold);
    teacherStyle.setFontItalic(exportOptions->t_teacherCursive);
    teacherStyle.setFontSize(exportOptions->t_teacherFontSize);
    QXlsx::Format teacherHeaderStyle;
    teacherHeaderStyle.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    teacherHeaderStyle.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    teacherHeaderStyle.setBorderStyle(QXlsx::Format::BorderThin);
    teacherHeaderStyle.setFontBold(exportOptions->t_headerBold);
    teacherHeaderStyle.setFontItalic(exportOptions->t_headerCursive);
    teacherHeaderStyle.setFontSize(exportOptions->t_headerFontSize);
    for(Day *day : cardTable->dayList)
    {
        dayColumnIndex = columnIndex;
        for (Lesson *lesson : day->lessonList)
        {
            xlsx.setColumnWidth(columnIndex, columnIndex, exportOptions->t_lessonWidth);
            xlsx.write(rowIndex, columnIndex, lesson->getName(), lessonStyle);
            columnIndex++;
        }
        xlsx.mergeCells(QXlsx::CellRange(rowIndex-1, dayColumnIndex, rowIndex-1, columnIndex-1));
        xlsx.write(rowIndex-1, dayColumnIndex, day->getName(), dayStyle);
    }

    xlsx.setRowFormat(1, cardTable->directory->teacherList.count()+2, defaultStyle);

    columnIndex = 1;
    rowIndex = 1;

    xlsx.mergeCells(QXlsx::CellRange(rowIndex, columnIndex, rowIndex+1, columnIndex));
    xlsx.setColumnWidth(columnIndex, columnIndex, exportOptions->t_headerWidth);
    xlsx.setRowHeight(rowIndex, rowIndex, exportOptions->t_dayHeight);
    xlsx.setRowHeight(rowIndex+1, rowIndex+1, exportOptions->t_lessonHeight);
    xlsx.write(rowIndex, columnIndex, "Учитель", teacherHeaderStyle);
    rowIndex ++;

    QMap<Teacher*, int> teacherMap;

    for (Teacher *teacher : cardTable->directory->teacherList)
    {
        rowIndex ++;
        xlsx.setRowHeight(rowIndex, rowIndex, exportOptions->t_teacherHeight);
        xlsx.write(rowIndex, columnIndex, teacher->getShortName(), teacherStyle);
        teacherMap.insert(teacher, rowIndex);
    }

    columnIndex = 2;

    for (Day *day : cardTable->dayList)
    {
        for (Lesson *lesson : day->lessonList)
        {
            for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
            {
                Card *card = cell->getCard(true);
                if (card != nullptr)
                {
                    int teacherRowIndex = teacherMap[card->getTeacher()];
                    xlsx.write(teacherRowIndex, columnIndex, card->getClass()->getName(), classStyle);

                    card = cell->getCard(false);
                    if (card != nullptr)
                    {
                        int teacherRowIndex = teacherMap[card->getTeacher()];
                        xlsx.write(teacherRowIndex, columnIndex, card->getClass()->getName(), classStyle);
                    }
                }
            }
            columnIndex++;
        }
    }

    QString str = fileName;
    if(fileName.right(5) != ".xlsx")
    {
        str += ".xlsx";
    }
    xlsx.saveAs(str);

    ui->statusBar->showMessage("Расписание экспортировано", 5000);
}

void MainWindow::loadTeacher(QFile *file)
{
    QString str = file->readLine();
    str = str.left(str.length()-1);
    int id = str.toInt();
    Teacher *teacher = new Teacher(id);
    QString secondName = file->readLine();
    secondName = secondName.left(secondName.length()-1);
    QString firstName = file->readLine();
    firstName = firstName.left(firstName.length()-1);
    QString thirdName = file->readLine();
    thirdName = thirdName.left(thirdName.length()-1);
    teacher->setSecondName(secondName);
    teacher->setFirstName(firstName);
    teacher->setThirdName(thirdName);

    str = file->readLine();
    str = str.left(str.length()-1);
    QString bColorName = str;
    QColor backgroundColor = QColor(bColorName);
    teacher->setBackgroundColor(backgroundColor);

    str = file->readLine();
    str = str.left(str.length()-1);
    while (str != "</Teacher>")
    {
        //Запускаю блок считывания дисциплины
        if( str == "<Discipline>")
        {
            QString disciplineName = file->readLine();
            disciplineName = disciplineName.left(disciplineName.length()-1);
            Discipline *discipline = cardTable->directory->getDiscipline(disciplineName);

            //Считываю все классы у которых учитель ведет по данному предмету
            str = file->readLine();
            str = str.left(str.length()-1);
            while (str != "</Discipline>")
            {
                QString _class = file->readLine();
                _class = _class.left(_class.length()-1);
                str = file->readLine();
                str = str.left(str.length()-1);
                int capacity = str.toInt();

                if (teacher->disciplineMap.contains(discipline))
                {
                    (teacher->disciplineMap[discipline]).insert(cardTable->getClass(_class), capacity);
                }
                else
                {
                    QMap<Class*, int> classMap;
                    classMap.insert(cardTable->getClass(_class), capacity);
                    teacher->disciplineMap.insert(discipline, classMap);
                }

                file->readLine();

                str = file->readLine();
                str = str.left(str.length()-1);
            }
        }
        //Обработка рабочего времени
        else if (str == "<Worktime>")
        {
            QString str = file->readLine();
            str = str.left(str.length()-1);
            while (str != "</Worktime>")
            {
                QString dayName = file->readLine();
                dayName = dayName.left(dayName.length()-1);
                Day *day = cardTable->getDay(dayName);

                str = file->readLine();
                str = str.left(str.length()-1);
                while (str != "</Day>")
                {
                    int lessonIndex = str.toInt();
                    teacher->lessonList.push_back(day->lessonList[lessonIndex-1]);

                    str = file->readLine();
                    str = str.left(str.length()-1);
                }

                str = file->readLine();
                str = str.left(str.length()-1);
            }
        }

        str = file->readLine();
        str = str.left(str.length()-1);
    }

    cardTable->directory->addTeacher(teacher);
}

void MainWindow::loadClass(QFile *file)
{
    QString str = file->readLine();
    str = str.left(str.length()-1);
    Class *_class = new Class(str, cardTable);
    str = file->readLine();
    str = str.left(str.length()-1);
    _class->setCapacity(str.toInt());
    cardTable->AddClass(_class);
    str = file->readLine();
    str = str.left(str.length()-1);
    if (str == "-")
    {
        _class->setHidden(true);
    }
    file->readLine();
}

void MainWindow::loadDiscipline(QFile *file)
{
    QString str = file->readLine();
    str = str.left(str.length()-1);
    Discipline *discipline =  cardTable->directory->addDiscipline(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    if (str != "</Discipline>")
    {
        discipline->setExportName(str);
        str = file->readLine();
        str = str.left(str.length()-1);
        if (str != "</Discipline>")
        {
            discipline->setExportForDoubleName(str);
        }
    }
}

void MainWindow::loadDay(QFile *file)
{
    QString str = file->readLine();
    str = str.left(str.length()-1);
    int count = str.toInt();
    cardTable->addDay(count);
    file->readLine();
}

void MainWindow::loadCell(QFile *file)
{
    QString str = file->readLine();
    str = str.left(str.length()-1);
    int id = str.toInt();

    QString disciplineName = file->readLine();
    disciplineName = disciplineName.left(disciplineName.length()-1);

    QString className = file->readLine();
    className = className.left(className.length()-1);

    QString dayName = file->readLine();
    dayName = dayName.left(dayName.length()-1);

    str = file->readLine();
    str = str.left(str.length()-1);
    int lessonIndex = str.toInt()-1;

    str = file->readLine();
    str = str.left(str.length()-1);
    int cardPosition = str.toInt();

    file->readLine();

    Teacher *teacher = cardTable->directory->getTeacher(id);
    Discipline *discipline = cardTable->directory->getDiscipline(disciplineName);
    Day* day = cardTable->getDay(dayName);
    Lesson *lesson = day->lessonList[lessonIndex];
    Class *_class = cardTable->getClass(className);
    Cell *cell = cardTable->getCell(lesson, _class);
    Card* card = cardList->getCard(teacher, discipline, _class);
    if (cardPosition == 1)
    {
        cell->setFirstCard(card);
    }
    else
    {
        cell->setSecondCard(card);
    }
}

void MainWindow::loadCard(QFile *file)
{
    QString str = file->readLine();
    str = str.left(str.length()-1);
    int teacherId = str.toInt();
    Teacher *teacher = cardTable->directory->getTeacher(teacherId);

    str = file->readLine();
    str = str.left(str.length()-1);
    QString disciplineName = str;
    Discipline *discipline = cardTable->directory->getDiscipline(disciplineName);

    str = file->readLine();
    str = str.left(str.length()-1);
    QString className = str;
    Class *_class = cardTable->getClass(className);

    str = file->readLine();
    str = str.left(str.length()-1);
    QString bColorName = str;
    QColor backgroundColor = QColor(bColorName);

    str = file->readLine();
    str = str.left(str.length()-1);
    QString fColorName = str;
    QColor foregroundColor = QColor(fColorName);

    str = file->readLine();
    str = str.left(str.length()-1);
    int customIndex = str.toInt();

    file->readLine();
    \
    Card *card = cardList->getCard(teacher, discipline, _class);
    card->setBColor(backgroundColor);
    card->setFColor(foregroundColor);
    cardList->customList[card] = customIndex;
}

void MainWindow::loadSettings(QFile *file)
{
    QString str = file->readLine();
    str = str.left(str.length()-1);
    int class_rowHeight = str.toInt();
    cardTable->set_class_rowHeight(class_rowHeight);

    str = file->readLine();
    str = str.left(str.length()-1);
    int rowHeight = str.toInt();
    cardTable->set_rowHeight(rowHeight);

    str = file->readLine();
    str = str.left(str.length()-1);
    int spacerHeight = str.toInt();
    cardTable->set_spacerHeight(spacerHeight);

    str = file->readLine();
    str = str.left(str.length()-1);
    int day_colomnWidth = str.toInt();
    cardTable->set_day_columnWidth(day_colomnWidth);

    str = file->readLine();
    str = str.left(str.length()-1);
    int lesson_columnWidth = str.toInt();
    cardTable->set_lesson_columnWidth(lesson_columnWidth);

    str = file->readLine();
    str = str.left(str.length()-1);
    int columnWidth = str.toInt();
    cardTable->set_columnWidth(columnWidth);

    str = file->readLine();
    str = str.left(str.length()-1);
    int minCellHeightForText = str.toInt();
    cardTable->set_minCellHeightForText(minCellHeightForText);

    str = file->readLine();
    str = str.left(str.length()-1);
    int minCellWidthForText = str.toInt();
    cardTable->set_minCellWidthForText(minCellWidthForText);

    str = file->readLine();
    str = str.left(str.length()-1);
    int minCellHeightForDoubleText = str.toInt();
    cardTable->set_minCellHeightForDoubleText(minCellHeightForDoubleText);

    str = file->readLine();
    str = str.left(str.length()-1);
    cardTable->setTopFlag(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    cardTable->setBotFlag(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    cardTable->setTipFlag(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    QString sortName = str;

    str = file->readLine();
    str = str.left(str.length()-1);
    bool isTtB = false;
    if (str == "true")
    {
        isTtB = true;
    }

    if (sortName == "custom")
    {
        cardList->slot_customSort();
        ui->customSortButton->setActivated(true);
    }

    SortButton *sortButton = nullptr;

    if (sortName == "teacher")
    {
        sortButton = ui->teacherSortButton;
    }

    if (sortName == "discipline")
    {
        sortButton = ui->disciplineSortButton;
    }

    if (sortName == "class")
    {
        sortButton = ui->classSortButton;
    }

    if (sortButton != nullptr)
    {
        sortButton->setActivated(true);
        sortButton->setTopToBottom(isTtB);
        sortButton->sortClicked(isTtB);
        if (isTtB)
        {
            sortButton->setIcon(sortButton->getFirstPix());
        }
        else
        {
            sortButton->setIcon(sortButton->getSecondPix());
        }

    }

    file->readLine();
}

void MainWindow::loadFilter(QFile *file)
{
    cardList->teacherFilterList.clear();
    cardList->disciplineFilterList.clear();
    cardList->classFilterList.clear();

    QString str = file->readLine();
    str = str.left(str.length()-1);
    bool isFilterOn = false;
    if (str == "true")
    {
        isFilterOn = true;
    }
    str = file->readLine();
    str = str.left(str.length()-1);
    bool isCardVisible = false;
    if (str == "true")
    {
        isCardVisible = true;
    }

    str = file->readLine();
    str = str.left(str.length()-1);
    while (str != "</Filter>")
    {
        if (str == "<Teachers>")
        {
            str = file->readLine();
            str = str.left(str.length()-1);
            while (str != "</Teachers>")
            {
                int teacherId = str.toInt();
                Teacher *teacher = cardTable->directory->getTeacher(teacherId);
                cardList->teacherFilterList.push_back(teacher);

                str = file->readLine();
                str = str.left(str.length()-1);
            }
        }

        if (str == "<Disciplines>")
        {
            str = file->readLine();
            str = str.left(str.length()-1);
            while (str != "</Disciplines>")
            {
                QString disciplineName = str;
                Discipline *discipline = cardTable->directory->getDiscipline(disciplineName);
                cardList->disciplineFilterList.push_back(discipline);

                str = file->readLine();
                str = str.left(str.length()-1);
            }
        }

        if (str == "<Classes>")
        {
            str = file->readLine();
            str = str.left(str.length()-1);
            while (str != "</Classes>")
            {
                QString className = str;
                Class *_class = cardTable->getClass(className);
                cardList->classFilterList.push_back(_class);

                str = file->readLine();
                str = str.left(str.length()-1);
            }
        }
        str = file->readLine();
        str = str.left(str.length()-1);
    }

    cardList->setCardVisible(isCardVisible);
    if (isFilterOn)
    {
        cardList->onFilter();
    }
    else
    {
        cardList->offFilter();
    }
    filterBox->setChecked(isFilterOn);

}

void MainWindow::loadExportOptions(QFile *file)
{

    QString str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->disciplineHeight = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->disciplineWidth = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->disciplineFontSize = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->disciplineCursive = strToBool(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->disciplineBold = strToBool(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->roomWidth = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->roomFontSize = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->roomCursive = strToBool(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->roomBold = strToBool(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->classHeight = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->classFontSize = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->classCursive = strToBool(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->classBold = strToBool(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->dayWidth = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->dayFontSize = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->dayCursive = strToBool(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->dayBold = strToBool(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->lessonWidth = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->lessonFontSize = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->lessonCursive = strToBool(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->lessonBold = strToBool(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_classHeight = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_classWidth = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_classFontSize = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_classCursive = strToBool(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_classBold = strToBool(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_headerFontSize = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_headerCursive = strToBool(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_headerBold = strToBool(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_teacherWidth = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_teacherFontSize = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_teacherCursive = strToBool(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_teacherBold = strToBool(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_dayHeight = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_dayFontSize = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_dayCursive = strToBool(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_dayBold = strToBool(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_lessonHeight = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_lessonFontSize = str.toInt();
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_lessonCursive = strToBool(str);
    str = file->readLine();
    str = str.left(str.length()-1);
    exportOptions->t_lessonBold = strToBool(str);

    str = file->readLine();
    str = str.left(str.length()-1);
    if (str == "<Title>")
    {
        exportOptions->topLeftText = "";
        exportOptions->topCentralText = "";
        exportOptions->topRightText = "";
        exportOptions->botLeftText = "";
        exportOptions->botCentralText = "";
        exportOptions->botRightText = "";


        str = file->readLine();
        str = str.left(str.length()-1);
        while (str != "<Title>")
        {
            exportOptions->topLeftText += str;
            exportOptions->topLeftText += "\n";
            str = file->readLine();
            str = str.left(str.length()-1);
        }

        str = file->readLine();
        str = str.left(str.length()-1);
        while (str != "<Title>")
        {
            exportOptions->topCentralText += str;
            exportOptions->topCentralText += "\n";
            str = file->readLine();
            str = str.left(str.length()-1);
        }

        str = file->readLine();
        str = str.left(str.length()-1);
        while (str != "<Title>")
        {
            exportOptions->topRightText += str;
            exportOptions->topRightText += "\n";
            str = file->readLine();
            str = str.left(str.length()-1);
        }

        str = file->readLine();
        str = str.left(str.length()-1);
        while (str != "<Title>")
        {
            exportOptions->botLeftText += str;
            exportOptions->botLeftText += "\n";
            str = file->readLine();
            str = str.left(str.length()-1);
        }

        str = file->readLine();
        str = str.left(str.length()-1);
        while (str != "<Title>")
        {
            exportOptions->botCentralText += str;
            exportOptions->botCentralText += "\n";
            str = file->readLine();
            str = str.left(str.length()-1);
        }

        str = file->readLine();
        str = str.left(str.length()-1);
        while (str != "<Title>")
        {
            exportOptions->botRightText += str;
            exportOptions->botRightText += "\n";
            str = file->readLine();
            str = str.left(str.length()-1);
        }

        file->readLine();
    }
}

void MainWindow::setCurrentFile(QString fileName)
{
    currentFile.setFileName(fileName);
    isCurrenFileEmpty = false;

    QString title = currentFile.fileName();
    QString str;
    int titleIndex = title.count()-1;
    while(str != "/")
    {
        str = title[titleIndex];
        titleIndex--;
    }
    title = title.remove(0, titleIndex+2);
    title = title.remove(title.length()-4, 4);
    setWindowTitle("Расписание - " + title);
}

void MainWindow::clearCurrentFile()
{
    isCurrenFileEmpty = true;
}

void MainWindow::OpenTableOptionsWindow()
//Открыть окно настроек
{
    OptionsWindow* optionsWindow = new OptionsWindow(cardTable);
    if (optionsWindow->exec() == QDialog::Accepted)
    {
        cardTable->set_rowHeight(optionsWindow->rowHeight->value());
        cardTable->set_columnWidth(optionsWindow->columnWidth->value());
        cardTable->set_spacerHeight(optionsWindow->spacerHeight->value());
        cardTable->set_lesson_columnWidth(optionsWindow->lesson_columnWidth->value());
        cardTable->set_day_columnWidth(optionsWindow->day_columnWidth->value());
        cardTable->set_class_rowHeight(optionsWindow->class_rowHeight->value());
        cardTable->set_minCellHeightForText(optionsWindow->minHeightForText->value());
        cardTable->set_minCellWidthForText(optionsWindow->minWidthForText->value());
        cardTable->set_minCellHeightForDoubleText(optionsWindow->minHeightForDoubleText->value());

    }
    delete optionsWindow;
}

void MainWindow::OpenCardOptionsWindow()
{
    CardOptionWindow* optionsWindow = new CardOptionWindow(cardTable);
    if (optionsWindow->exec() == QDialog::Accepted)
    {
    }
    delete optionsWindow;
}

void MainWindow::OpenDisciplinesDirectoryWindow()
//Открыть справочник предметов
{
    DisciplineDialog* disciplineDialog = new DisciplineDialog(cardTable->directory);
    disciplineDialog->exec();
}

void MainWindow::OpenRoomsDirectoryWindow()
 //Открыть справочник кабинетов
{
    RoomsDialog* roomsDialog = new RoomsDialog(cardTable->directory);
    roomsDialog->exec();
}

void MainWindow::OpenClassesDirectoryWindow()
//Открыть справочник классов
{
    classdialog *classDialog = new classdialog(cardTable, cardTable->directory);
    classDialog->exec();
}

void MainWindow::OpenTeachersDirectoryWindow()
//Открыть справочник учителей
{
    TeacherDialog *teacherDialog = new TeacherDialog(cardTable, cardTable->directory, cardList);
    teacherDialog->exec();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (isCurrenFileEmpty)
    {
        return;
    }

    QMessageBox questionBox(QMessageBox::Question,
                       "Закрытие приложения",
                       "Сохранить текущее расписание?",nullptr);
    questionBox.addButton(QString::fromUtf8("Да"),QMessageBox::AcceptRole);
    questionBox.addButton(QString::fromUtf8("Нет"), QMessageBox::RejectRole);
    questionBox.addButton(QString::fromUtf8("Отмена"), QMessageBox::AcceptRole);


   int answer;
   answer = questionBox.exec();
   switch (answer)
   {
   case QMessageBox::AcceptRole:
       save();
       break;
   case QMessageBox::RejectRole:
       break;
   default:
       event->ignore();
       break;
   }
}

void MainWindow::slot_editFilter()
{
   cardList->editFilter(cardTable->directory, cardTable);
}

void MainWindow::slot_checkFilter(int value)
{
    if(value>0)
    {
        cardList->onFilter();
    }
    else
    {
        cardList->offFilter();
    }

}

void MainWindow::slot_newFile()
{
    if (isCurrenFileEmpty)
    {
        initNewFile();
    }
    else
    {
        QMessageBox warningBox(QMessageBox::Question,
                           "Новое расписание",
                           "Сохранить текущее расписание?",
                           nullptr, this);
        warningBox.addButton(QString::fromUtf8("Да"),
                         QMessageBox::AcceptRole);
        warningBox.addButton(QString::fromUtf8("Нет"),
                         QMessageBox::RejectRole);
        if (warningBox.exec() == QMessageBox::AcceptRole)
        {
            slot_saveFile();
        }
        initNewFile();
    }
}

void MainWindow::slot_newFileBy()
{
    initNewFileBy();
}

void MainWindow::slot_saveFile()
{
    if (!isCurrenFileEmpty)
    {
        save();
    }
    else
    {
        saveAs();
    }
}

void MainWindow::slot_saveFileAs()
{
    saveAs();
}

void MainWindow::slot_exportToXlsx()
{
    ExportDialog *exportDialog = new ExportDialog();
    exportDialog->setOptions(exportOptions);
    if (exportDialog->exec() == QDialog::Accepted)
    {
        QString fileName = QFileDialog::getSaveFileName(this,
                tr("Экспорт в xlsx"), "",
                tr("Расписание (*.xlsx);;Все файлы (*)"));

        exportOptions->getOptions(exportDialog);
        exportToXlsx(fileName, exportOptions);
    }
}

void MainWindow::slot_exportToTeacherXlsx()
{
    ExportTeacherDialog *exportDialog = new ExportTeacherDialog();
    exportDialog->setOptions(exportOptions);
    if (exportDialog->exec() == QDialog::Accepted)
    {
        QString fileName = QFileDialog::getSaveFileName(this,
                tr("Экспорт в xlsx"), "",
                tr("Расписание (*.xlsx);;Все файлы (*)"));

        exportOptions->getOptions(exportDialog);
        exportToTeacherXlsx(fileName, exportOptions);
    }
}

void MainWindow::slot_openFile()
{    
    QString str = QFileDialog::getOpenFileName(this,
            tr("Загрузить расписание"), "",
            tr("Расписание (*.stt);;Все файлы (*)"));
    if (str != "")
    {
       open(str);
    }
    ui->scrollArea->verticalScrollBar()->setValue(0);
    ui->scrollArea->horizontalScrollBar()->setValue(0);
}
