#ifndef TEACHER_DISCIPLINEEDITDIALOG_H
#define TEACHER_DISCIPLINEEDITDIALOG_H

#include <QDialog>
#include <QMap>
#include <QGridLayout>
#include <QPushButton>
#include <QSpinBox>
#include <QCheckBox>
#include <QScrollArea>
#include "class.h"
#include "cardtable.h"

class Teacher_disciplineEditDialog : public QDialog
{
    Q_OBJECT
public:
    explicit Teacher_disciplineEditDialog(Teacher *pTeacher, Discipline *pDiscipline, CardList *cardList, QMap<Class *, int> *_classMap, CardTable *cardTable, QWidget *parent = nullptr);
    QMap<Class *, int> classMap;

    QScrollArea *classArea = new QScrollArea();

private:
    QGridLayout *mainLayout = new QGridLayout();
    QGridLayout* scrollLayout = new QGridLayout();
    QPushButton *okButton = new QPushButton("Принять");
    QPushButton *cancelButton = new QPushButton("Отменить");

    QList<QSpinBox*> spinBoxList;
    Discipline *discipline;
    Teacher *teacher;

    void change_classMap(Class *_class, QSpinBox *spinBox, QCheckBox *checkBox);

private slots:
    void slot_checkCount();
};

#endif // TEACHER_DISCIPLINEEDITDIALOG_H
