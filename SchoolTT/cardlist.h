#ifndef CARDLIST_H
#define CARDLIST_H

#include <QDragMoveEvent>
#include <QDropEvent>
#include <QListWidget>
#include "cardtable.h"
#include "class.h"
#include "lesson.h"
#include "day.h"
#include "cell.h"
#include "directory.h"
#include "card.h"

class CardList : public QListWidget
{
    Q_OBJECT
public:
    explicit CardList(QWidget *parent = nullptr);

    QString getSort();

    void addCard(Card *card);
    void deleteCard(Card *card);
    QListWidgetItem *getItem(Card* card);
    void refresh();

    void editFilter(Directory *directory, CardTable *cardTable);
    void onFilter();
    void offFilter();

    QList <Teacher*> teacherFilterList;
    QList <Class*> classFilterList;
    QList <Discipline*> disciplineFilterList;
    QMap <Card*, int> customList;

    Card *getCard (Teacher* teacher, Discipline *discipline, Class* _class);

    void setSort(QString sortName, bool pIsTtB);
    bool getIsTtB();
    bool getIsFilterOn();
    bool isCardVisible();
    void setCardVisible(bool value);

    void setCardTable(CardTable *pCardTable);

private:
    void showWithClass(Class* _class);
    void showWithLesson(Lesson *lesson);
    void showWithDay(Day *day);
    void showWithClassAndLesson(Class* _class, Lesson *lesson);
    void showAll();

    void setClassSort(bool isTopToBottom);
    void setTeacherSort(bool isTopToBottom);
    void setDisciplineSort(bool isTopToBottom);
    void setCustomSort();

    void insertFromTo(int firstIndex, int secondIndex);
    void swapCards(int firstIndex, int secondIndex);
    void swapCards(Card *firstCard, Card *secondCard);

    QString sortFlag = "custom";

    bool isTtB = true;
    bool filterOn = false;
    bool cardVisible = true;
    void setHideToTable(QListWidgetItem *item, bool value);
    void setHideToFilter(QListWidgetItem *item, bool value);
    void checkHide(QListWidgetItem *item);

    QList<Teacher *> getTeacherList(Cell *cell);
    QList<Teacher *> getTeacherList(Lesson *lesson);

    CardTable *cardTable;
protected:
        void dropEvent(QDropEvent* event);
        void dragEnterEvent(QDragEnterEvent* event);

signals:
        void mousePressed();

public slots:
        void slot_showWithClass(Class* _class);
        void slot_showWithClass(Cell* cell);
        void slot_showWithDay(Day *day);
        void slot_showWithLesson(Lesson *lesson);
        void slot_showWithClassAndLesson(Cell *cell);
        void slot_showAll();

        void slot_classSort(bool isTopToBottom);
        void slot_teacherSort(bool isTopToBottom);
        void slot_disciplineSort(bool isTopToBottom);
        void slot_customSort();
        void slot_updateSort();

        // QWidget interface
protected:
        void mousePressEvent(QMouseEvent *event);
};

#endif // CARDLIST_H
