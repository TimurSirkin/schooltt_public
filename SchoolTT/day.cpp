#include "day.h"
#include <QLayout>
#include <QPushButton>
#include <QLabel>
#include <QPoint>
#include <QDebug>
#include "styles.h"
#include "cardtable.h"

//День

Day::Day(QString pText ,QWidget *parent):TableElement(parent)
  //Конструктор
{
    text = pText;
    _backgroundColor = QColor (200,200,205);
    fontSize = 10;

    defaultHeight = 30;
    defaultWidth = 60;

    QGridLayout *mainLayout = new QGridLayout();
    mainLayout->setSpacing(0);
    setLayout(mainLayout);
    textLabel->setText(text);

    addButton->setStyleSheet(Styles::dayButton());
    addButton->setText("+");
    mainLayout->addWidget(addButton, 1, 0);
    mainLayout->setAlignment(addButton, Qt::AlignBottom);

    deleteButton->setStyleSheet(Styles::dayButton());
    deleteButton->setText("-");
    mainLayout->addWidget(deleteButton, 1, 1);
    mainLayout->setAlignment(deleteButton, Qt::AlignBottom);
    mainLayout->setMargin(0);

    CardTable *cardTable = (CardTable*)parent;
    connect(addButton, &QPushButton::clicked, [this, cardTable] () {cardTable->slot_addLesson(this);
        deleteButton->setEnabled(lessonList[0]->action_delete->isEnabled());});
    connect(deleteButton, &QPushButton::clicked, [this] () {lessonList[0]->action_delete->trigger();
        deleteButton->setEnabled(lessonList[0]->action_delete->isEnabled());});

    connect(action_clear, &QAction::triggered, [this] () {emit signal_clear();});
}


void Day::paintEvent(QPaintEvent *event)
//Переопределяю событие отрисовки этого виджета (рисуется не так, как остальные элементы таблицы)
{
    TableElement::paintEvent(event);

    QPainter painter(this);
    QFont font;
    font.setPointSize(fontSize*1.5);
    painter.setFont(font);
    QFontMetrics fontMetrics(font);
    painter.translate(width()/2, height()/2);
    painter.rotate(270.5);
    QString str;
    if(fontMetrics.width(text) >= height() - addButton->height() - 20)
    {
        str = text.left(3) + "....";
        if(fontMetrics.width(str) >= height() - addButton->height() - 20)
        {
            str =  "";
        }
    }
    else
    {
        str = text;
    }
    painter.drawText(-fontMetrics.width(str)/2, -fontMetrics.height()/2, height(), width(), Qt::AlignLeft, str);
}

QString Day::getName()
//Переопределяю метод родительского класса - нужно возфращать не name а свое поле text (name TableElement рисует)
{
    return text;
}

void Day::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu *contextMenu = new QMenu();
    contextMenu->addAction(lessonList[0]->action_delete);
    contextMenu->addAction(action_clear);
    CardTable *cardTable = (CardTable*)parent();
    contextMenu->addAction("Добавить урок", cardTable, [=](){cardTable->slot_addLesson(this);});
    contextMenu->exec(event->globalPos());
}

