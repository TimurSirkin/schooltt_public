#include "teachereditdialog.h"
#include <QDebug>
#include <QToolTip>
#include <QShortcut>
#include <QCheckBox>
#include <QStyle>
#include <QTreeWidgetItem>
#include <QSpinBox>
#include <QMessageBox>
#include <QColorDialog>
#include <QVariant>
#include "discipline.h"
#include "namevalidator.h"
#include "styles.h"
#include "day.h"
#include "lesson.h"
#include "teacher_disciplineeditdialog.h"

//Окно редактирования учителя

TeacherEditDialog::TeacherEditDialog(CardTable *_cardTable, Directory *_directory,  CardList *pCardList, bool pIsForLooking, Teacher *_teacher, QWidget *parent) : QDialog(parent)

  //Конструктор
{
    directory = _directory;
    cardTable = _cardTable;
    teacher = _teacher;
    cardList = pCardList;
    isForLooking = pIsForLooking;

    if (teacher != nullptr)
    {
        teacherBackgroundColor = teacher->getBackgroundColor();
        chooseColorButton->setPalette(teacherBackgroundColor);
    }

    disciplineScrollArea->setFixedHeight(40);
    lessonScrollArea->setFixedHeight(150);
    if (teacher != nullptr)
    {
        lessonList = teacher->lessonList;
    }

    disciplineLabel->setHidden(true);
    disciplineScrollArea->setHidden(true);

    if (teacher != nullptr)
    {
        secondNameLine->setText(teacher->getSecondName());
        firstNameLine->setText(teacher->getFirstName());
        thirdNameLine->setText(teacher->getThirdName());
    }
    //Добавляем соотв-е учителю дисциплины в окошко
    for (Discipline *discipline : directory->disciplineList)
    {
        if (teacher != nullptr)
        {
            if (teacher->disciplineMap.contains(discipline))
            {
                addDiscipline(discipline, teacher->disciplineMap[discipline]);
            }
        }
    }

    if (!isForLooking)
    {
        this->setWindowTitle("Просмотр учителя");
        okButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogOkButton));
        okButton->setStyleSheet(Styles::answerButtons());
        cancelButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogCancelButton));
        cancelButton->setStyleSheet(Styles::answerButtons());
        connect(chooseColorButton, SIGNAL(clicked()), this, SLOT(chooseColor()));
        connect(okButton, &QPushButton::clicked, [=](){checkLines();});
        connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
        newLayout->addWidget(okButton, 13, 0, 1, 1, Qt::AlignLeft);
        newLayout->addWidget(cancelButton, 13, 1, 1, 1, Qt::AlignRight);
        newLayout->addWidget(addDisciplineButton, 10, 0, 1, 2);
    }
    else
    {
        this->setWindowTitle("Редактирование учителя");
        secondNameLine->setEnabled(false);
        firstNameLine->setEnabled(false);
        thirdNameLine->setEnabled(false);
        chooseColorButton->setEnabled(false);
    }
    NameValidator *nameValidator = new NameValidator(this);
    firstNameLine->setValidator(nameValidator);
    secondNameLine->setValidator(nameValidator);
    thirdNameLine->setValidator(nameValidator);


    newLayout->addWidget(secondNameLabel, 0, 0, 1, 2);
    newLayout->addWidget(secondNameLine, 1, 0, 1, 2);
    newLayout->addWidget(firstNameLabel, 2, 0, 1, 2);
    newLayout->addWidget(firstNameLine, 3, 0, 1, 2);
    newLayout->addWidget(thirdNameLabel, 4, 0, 1, 2);
    newLayout->addWidget(thirdNameLine, 5, 0, 1, 2);
    newLayout->addWidget(colorLabel, 6, 0, 1, 2);
    newLayout->addWidget(chooseColorButton, 7, 0, 1, 2);
    newLayout->addWidget(disciplineLabel, 8, 0, 1, 2);
    newLayout->addWidget(disciplineScrollArea, 9, 0, 1, 2);
    newLayout->addWidget(lessonLabel, 11, 0, 1, 2);
    newLayout->addWidget(lessonScrollArea, 12, 0, 1, 2);

    QWidget *wgt = new QWidget();
    wgt->setLayout(scrollLayout);
    disciplineScrollArea->setWidget(wgt);
    disciplineScrollArea->setWidgetResizable(true);
    wgt->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    scrollLayout->addWidget(new QLabel("<b>Наименование<b>"), 0, 0);
    scrollLayout->addWidget(new QLabel("<b>Классов<b>"), 0, 1, 1, 1, Qt::AlignCenter);

    this->setLayout(newLayout);
    connect(addDisciplineButton, SIGNAL(clicked()), SLOT(createDisciplineMap()));

    //Создаем скролл арею для рабочих дней учителя
    QGridLayout *newLayout = new QGridLayout();
    lessonScrollArea->setLayout(newLayout);
    newLayout->addWidget(lessonTree);
    lessonTree->setColumnCount(2);
    lessonTree->setHeaderHidden(true);
    for (Day *day : cardTable->dayList)
    {
        QTreeWidgetItem *dayItem = new QTreeWidgetItem(lessonTree);
        dayItem->setText(0,day->getName());
        if(isForLooking)
        {
            dayItem->setDisabled(true);
        }
        QCheckBox *dayCheckBox = new QCheckBox();
        dayCheckBox->setDisabled(isForLooking);
        lessonTree->setItemWidget(dayItem,1,dayCheckBox);
        bool isDayFull = true;
        QList<QCheckBox*> lessonCheckBoxList;
        for(Lesson *lesson : day->lessonList)
        {
            QTreeWidgetItem *lessonItem = new QTreeWidgetItem(dayItem);
            lessonItem->setText(0,lesson->getName());
            QCheckBox *lessonCheckBox = new QCheckBox();
            checkBoxList.push_back(lessonCheckBox);

            if (isForLooking)
            {
                lessonCheckBox->setDisabled(true);
            }
            lessonTree->setItemWidget(lessonItem,1,lessonCheckBox);
            if (teacher == nullptr || lessonList.contains(lesson) == true)
            {
                lessonCheckBox->setChecked(true);
                dayItem->setExpanded(true);
                if(teacher == nullptr)
                {
                    lessonList.push_back(lesson);
                }
            }
            else
            {
                //Флаг дает понять - все ли уроки текущего дня у учителя рабочие
                isDayFull = false;
            }

            lessonCheckBoxList.push_back(lessonCheckBox);

            connect(lessonCheckBox, &QCheckBox::clicked, [=]()
            {
                if (lessonCheckBox->isChecked() == true)
                {
                    lessonList.push_back(lesson);
                }
                else
                {
                    slot_deleteLesson(lesson, lessonCheckBox);
                }
            });

        }
        if (isDayFull == true)
        {
            dayCheckBox->setChecked(true);
            dayItem->setExpanded(false);
        }
        connect(dayCheckBox, &QCheckBox::clicked, [=]()
        {
            slot_changeDay(day, dayCheckBox, lessonCheckBoxList);
        });
    }

    new QShortcut(QKeySequence(Qt::Key_Insert), this, SLOT(createDisciplineMap()));
    secondNameLine->setFocus();
    setFixedWidth(320);
    setFixedHeight(sizeHint().height());
}

void TeacherEditDialog::editClassMap(Discipline *discipline, QMap<Class *, int> *classMap, QLabel *nameLabel, QPushButton *countButton)
//Редактировать предмет учителя
{
    Teacher_disciplineEditDialog *inputDialog = new Teacher_disciplineEditDialog(teacher, discipline, cardList, classMap, cardTable);
    inputDialog->setWindowTitle(discipline->getName());
    if (inputDialog->exec() == QDialog::Accepted)
    {
        *classMap = inputDialog->classMap;
        nameLabel->setText(discipline->getName());
        countButton->setText(QString::number(classMap->count()));
        QString str;
        for (Class *_class: classMap->keys())
        {
            str += _class->getName() + "<br>";
        }
        str.remove(str.length()-4, 4);
        countButton->setToolTip(str);
    }
}

void TeacherEditDialog::addDiscipline(Discipline *discipline, QMap<Class *, int> classMap)
//Создать строки дисциплин, соотв. данному учителю
{
    QList<QWidget*> wgtList;

     disciplineLabel->setHidden(false);
     disciplineScrollArea->setHidden(false);

    //При добавлении первых двух предметов нужно расширить скрол арию и сделать видимым лэйбэл
    if(disciplineMap.count() < 3)
    {
        disciplineScrollArea->setFixedHeight(disciplineScrollArea->height() + rowInc);
    }

    //QPushButton* nameButton= new QPushButton();
    QLabel* nameLabel= new QLabel();
    //nameLabel->setStyleSheet(Styles::linkButton());
    QString str = discipline->getName();
    if (str.length() > 15)
    {
        nameLabel->setText(str.left(13) + "...");
    }
    else
    {
       nameLabel->setText(discipline->getName());
    }
    scrollLayout->addWidget(nameLabel, scrollLayout->rowCount(), 0, 1, 1, Qt::AlignLeft);
    wgtList.push_back(nameLabel);


    QPushButton* countButton= new QPushButton();
    if (teacher != nullptr)
    {
        countButton->setText(QString::number(classMap.count()));
    }
    else
    {
        countButton->setText("0");
    }
    countButton->setStyleSheet(Styles::linkButton());
    scrollLayout->addWidget(countButton, scrollLayout->rowCount()-1, 1, 1, 1, Qt::AlignCenter);
    str = "";
    for (Class *_class: classMap.keys())
    {
        str += _class->getName() + "<br>";
    }
    str.remove(str.length()-4, 4);
    countButton->setToolTip(str);
    connect(countButton, &QPushButton::clicked, [=](){QToolTip::showText(QCursor::pos(), countButton->toolTip());});
    wgtList.push_back(countButton);

    if(!isForLooking)
    {
        QPushButton *editButton = new QPushButton();
        QPushButton *deleteButton = new QPushButton();
        editButton->setStyleSheet(Styles::editButton());
        deleteButton->setStyleSheet(Styles::deleteButton());
        scrollLayout->addWidget(editButton, scrollLayout->rowCount()-1, 2, 1, 1);
        scrollLayout->addWidget(deleteButton, scrollLayout->rowCount()-1, 3, 1, 1);
        wgtList.push_back(editButton);
        wgtList.push_back(deleteButton);

        connect(editButton, &QPushButton::clicked, [=]() {editClassMap(discipline, &(disciplineMap[discipline]), nameLabel, countButton);});
        connect(deleteButton, &QPushButton::clicked, [=]() {deleteDiscipline(discipline, wgtList);});
    }
    else
    {
        nameLabel->setEnabled(false);
    }

    disciplineMap.insert(discipline, classMap);
}

void TeacherEditDialog::deleteDiscipline(Discipline *discipline, QList<QWidget*> wgtList)
//Удалить дисциплину у учителя
{
    //Проверяю не нахоидтся ли карточки с данным учителем в таблице
    int cardCount = 0;
    for (int itemIndex = 0; itemIndex < cardList->count(); itemIndex++)
    {
        Card *card = (Card*)(cardList->itemWidget(cardList->item(itemIndex)));
        if (card->getTeacher() == teacher && card->getDiscipline() == discipline)
        {
            cardCount += (card->getMaxCount() - card->getCurrentCount());
        }
    }

    QString str;
    if(cardCount != 0)
    {
        str = "В таблице присутствуют карточки с данным предметом<br>В количестве:<b>"
                +QString::number(cardCount) + "</b><br>Убрать предмет?";
    }
    else
    {
        str = "Убрать предмет?";
    }
    QMessageBox warningBox(QMessageBox::Question,
                       discipline->getName(),
                       str,
                       nullptr);
    warningBox.addButton(QString::fromUtf8("Да"),
                     QMessageBox::AcceptRole);
    warningBox.addButton(QString::fromUtf8("Нет"),
                     QMessageBox::RejectRole);
    if(cardCount > 0)
    {
        warningBox.setIcon(QMessageBox::Warning);
    }
    if (warningBox.exec() == QMessageBox::AcceptRole)
    {
        for (QWidget *wgt : wgtList)
        {
            delete wgt;
        }
        disciplineMap.remove(discipline);

        //При добавлении первых двух предметов нужно расширить скрол арию и сделать видимым лэйбэл
        if(disciplineMap.count() <= 2)
        {
            if(disciplineMap.count() == 0)
            {
                disciplineLabel->setHidden(true);
                disciplineScrollArea->setHidden(true);
                disciplineScrollArea->setFixedHeight(45);
            }
            else
            {
                disciplineScrollArea->setFixedHeight(disciplineScrollArea->height() - rowInc);
            }
        }
    }
    setFixedHeight(sizeHint().height());
}

void TeacherEditDialog::selectCurrentDiscipline(QListWidget *listWidget)
{
    int currentRow = listWidget->currentRow();
    int disciplineID = (listWidget->item(currentRow))->data(Qt::UserRole).toInt();
    QLinkedList<Discipline*>::iterator it = directory->disciplineList.begin();
    for (int i = 0; i < disciplineID; i++)
    {
        ++it;
    }
    QMap<Class*, int> newMap;
    addDiscipline(*it, newMap);
    setFixedHeight(sizeHint().height());
}

void TeacherEditDialog::checkLines()
{
    if (firstNameLine->text() != "" && secondNameLine->text() != "" && thirdNameLine->text() != "")
    {
        bool isEmpty = true;
        for (QCheckBox *checkBox : checkBoxList)
        {
            if(checkBox->isChecked())
            {
                isEmpty = false;
            }
        }
        if(teacher == nullptr && isEmpty)
        {
            QMessageBox warningBox(QMessageBox::Question,
                               "Новый учитель",
                               "У данного учителя не поставлено ни одного рабочего дня.<br>"
                               "Продолжить?",
                               nullptr);
            warningBox.addButton(QString::fromUtf8("Да"),
                             QMessageBox::AcceptRole);
            warningBox.addButton(QString::fromUtf8("Нет"),
                             QMessageBox::RejectRole);
            if (warningBox.exec() == QMessageBox::AcceptRole)
            {
                accept();
            }
        }
        else
        {
            accept();
        }
    }
    else
    {
        QString str;
        if(teacher != nullptr) str = teacher->getShortName();
        else str = "Новый учитель";
        QMessageBox warningBox(QMessageBox::Warning,
                           str,
                           QString::fromUtf8("Все поля должны быть заполнены"),
                           nullptr);
        warningBox.exec();
    }
}

void TeacherEditDialog::createDisciplineMap()
//Добавить учителю новый предмет
{
    QDialog *inputDialog = new QDialog();
    inputDialog->setWindowTitle("Список предметов");
    QGridLayout *mainLayout = new QGridLayout();
    inputDialog->setLayout(mainLayout);
    QListWidget *disciplineQList = new QListWidget();
    mainLayout->addWidget(disciplineQList);
    QPushButton *addButton = new QPushButton("Добавить");

    connect(addButton, SIGNAL(clicked()), inputDialog, SLOT(accept()));
    connect(disciplineQList, &QListWidget::doubleClicked, [=](){inputDialog->accept();});
    mainLayout->addWidget(addButton, 1, 0);
    inputDialog->setFixedWidth(inputDialog->sizeHint().width());

    //Махинации для выбора нужного предмета, криво через итератор
    //Каждому предмету при вызове присваиваю ID
    int disciplineID = 0;
    addButton->setEnabled(false);
    for (Discipline *discipline : directory->disciplineList)
    {
        if (!disciplineMap.contains(discipline))
        {
            addButton->setEnabled(true);
            QListWidgetItem *newItem = new QListWidgetItem();
            newItem->setText(discipline->getName());
            newItem->setData(Qt::UserRole, disciplineID);
            disciplineQList->addItem(newItem);
        }
        disciplineID++;
    }

    //Если нажали Принять - определю по ID нужный мне предмет
    if (inputDialog->exec() == QDialog::Accepted)
    {
       selectCurrentDiscipline(disciplineQList);
    }
}

void TeacherEditDialog::slot_deleteLesson(Lesson *lesson, QCheckBox * checkBox)
{
    int isEmpty = true;
    if (teacher == nullptr)
    {
        lessonList.removeAll(lesson);
        return;
    }
    //Проверяю не нахоидтся ли карточки с данным учителем в таблице
    Day *day = cardTable->getParentDay(lesson);
    for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
    {
        if (cell->getSecondCard() != nullptr
                && cell->getSecondCard()->getTeacher() == teacher)
        {
            isEmpty = false;
            break;
        }
        if (cell->getFirstCard() != nullptr
                && cell->getFirstCard()->getTeacher() == teacher)
        {
            isEmpty = false;
            break;
        }
    }
    QString str;
    if (!isEmpty)
    {
        str = "Учитель ведет на данном уроке занятие"
        "<br>Удалить урок?";
    }
    else
    {
        str = "Удалить урок?";
    }
    QMessageBox warningBox(QMessageBox::Question,
                       teacher->getShortName() + " - " + lesson->getName() + " урок",
                       str,
                       nullptr);
    warningBox.addButton(QString::fromUtf8("Да"),
                     QMessageBox::AcceptRole);
    warningBox.addButton(QString::fromUtf8("Нет"),
                     QMessageBox::RejectRole);
    if (!isEmpty)
    {
       warningBox.setIcon(QMessageBox::Warning);
    }
    if (warningBox.exec() == QMessageBox::AcceptRole)
    {
        lessonList.removeAll(lesson);
    }
    else
    {
        checkBox->setChecked(true);
    }
}

void TeacherEditDialog::slot_changeDay(Day *day, QCheckBox *checkBox, QList<QCheckBox *> checkBoxList)
{
    if (checkBox->isChecked())
    {
        for (QCheckBox *checkBox : checkBoxList)
        {
            checkBox->setChecked(true);
        }
        for (Lesson *lesson : day->lessonList)
        {
            if (!lessonList.contains(lesson))
            {
                lessonList.push_back(lesson);
            }
        }
        return;
    }

    if (teacher == nullptr)
    {
        for (QCheckBox *checkBox : checkBoxList)
        {
            checkBox->setChecked(false);
        }
        for (Lesson *lesson : day->lessonList)
        {
            if (lessonList.contains(lesson))
            {
                lessonList.removeOne(lesson);
            }
        }
        return;
    }

    int count = 0;
    //Проверяю не нахоидтся ли карточки с данным учителем в таблице
    for (Lesson *lesson : day->lessonList)
    {
        for (Cell *cell : day->cellMatrix[day->lessonList.indexOf(lesson)])
        {
            if (cell->getSecondCard() != nullptr
                    && cell->getSecondCard()->getTeacher() == teacher)
            {
                count++;
            }
            if (cell->getFirstCard() != nullptr
                    && cell->getFirstCard()->getTeacher() == teacher)
            {
                count++;
            }
        }
    }

    QString str;
    if (count != 0)
    {
        str = "Учитель ведет в данный день занятия<br>В количестве: <b>" + QString::number(count) + "</b>"
        "<br>Убрать день?";
    }
    else
    {
        str = "Убрать день?";
    }
    QMessageBox warningBox(QMessageBox::Question,
                       teacher->getShortName() + " - " + day->getName(),
                       str,
                       nullptr);
    warningBox.addButton(QString::fromUtf8("Да"),
                     QMessageBox::AcceptRole);
    warningBox.addButton(QString::fromUtf8("Нет"),
                     QMessageBox::RejectRole);
    if (count !=0)
    {
       warningBox.setIcon(QMessageBox::Warning);
    }


    if (warningBox.exec() == QMessageBox::AcceptRole)
    {
        for (QCheckBox *checkBox : checkBoxList)
        {
            checkBox->setChecked(false);
        }
        for (Lesson *lesson : day->lessonList)
        {
            if (lessonList.contains(lesson))
            {
                lessonList.removeOne(lesson);
            }
        }
    }
    else
    {
        checkBox->setChecked(true);
    }
}

void TeacherEditDialog::chooseColor()
{
    //Первый параметр конструктора - начальный цвет(выбранный)
    QColorDialog* inputDialog = new QColorDialog(teacherBackgroundColor, this);
    inputDialog->setWindowTitle("Выберите цвет");
    if (inputDialog->exec() == QDialog::Accepted)
    {
        QColor color = inputDialog->currentColor();
        teacherBackgroundColor = color;
        chooseColorButton->setPalette(color);
    }
}
