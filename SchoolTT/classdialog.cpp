#include "classdialog.h"
#include <QDebug>
#include <QCheckBox>
#include <QShortcut>
#include <QScrollBar>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QString>
#include <QMessageBox>
#include "styles.h"
#include "room.h"
#include "discipline.h"
#include "classeditdialog.h"


//Диалоговое окна для добавления, удаления и редактирования учителей

classdialog::classdialog(CardTable *_parentWidget, Directory *_directory, QWidget *parent) : QDialog(parent)
  //Конструктор окна
{
    directory = _directory;
    cardTable = _parentWidget;
    setWindowTitle("Справочник классов");
    QGridLayout *mainLayout = new QGridLayout();
    setLayout(mainLayout);
    QPushButton *addButton = new QPushButton("Добавить класс");
    mainLayout->addWidget(addButton, 1, 0, 1, 2);
    connect(addButton, SIGNAL(clicked()), SLOT(createClass()));

    mainLayout->addWidget(scrollArea, 0, 0, 1, 2);
    QWidget *wgt = new QWidget();
    wgt->setLayout(scrollLayout);
    wgt->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    scrollArea->setWidget(wgt);
    scrollArea->setWidgetResizable(true);

    scrollLayout->addWidget(new QLabel("<b>Наименование  "), 0, 0);
    scrollLayout->addWidget(new QLabel("<b>Учеников"), 0, 1);
    scrollLayout->addWidget(new QLabel("<b>Нагрузка"), 0, 2);
    QLabel *lbl = new QLabel();
    lbl->setPixmap(QPixmap(":/new/resource/Images/eye.png"));
    scrollLayout->addWidget(lbl, 0, 3, 1, 1, Qt::AlignCenter);

    classList =  _parentWidget->classList;
    for (Class *_class : classList)
    {
        addClass(_class);
    }

    new QShortcut(QKeySequence(Qt::Key_Insert), this, SLOT(createClass()));
    setFixedWidth(sizeHint().width()+15);
}

void classdialog::editClass(Class *_class, QLabel *_nameLabel, QLabel *_sizeLabel)
//Редактирование класса, изменяет поля класса и строку в окне, соотв. текущему классу
{
    ClassEditDialog *inputDialog = new ClassEditDialog(cardTable->classList, _class);
    inputDialog->nameLine->setText(_class->getName());
    inputDialog->sizeBox->setValue(_class->getCapacity());
    if (inputDialog->exec() == QDialog::Accepted)
   {
        if (inputDialog->nameLine->text() == "")
        {
            return;
        }
        QString _name(inputDialog->nameLine->text());
        int _size = inputDialog->sizeBox->value();
        _class->setName(_name);
        _class->setCapacity(_size);
        _nameLabel->setText(getStr(_name));
        _sizeLabel->setText(QString::number(_size));
    }
}

void classdialog::deleteClass(Class *_class, QList<QWidget*> wgtList)
//Удаление класса
{
    //Список для хранения учителей, у которых удалили класс (само удаление происходит после нажания кнопки "Принять"
    QList<Teacher*> tempList;

    //Cмотрим у какие учителя преподают у данного класса
    for (Teacher *teacher: directory->teacherList)
    {
        for(Discipline *discipline : teacher->disciplineMap.keys())
        {
            if (teacher->disciplineMap[discipline].contains(_class))
            {
                if (!tempList.contains(teacher))
                {
                    tempList.push_back(teacher);
                }
            }
        }
    }

    //Если таких учителей нет - просим подтвердить действие
    if (tempList.isEmpty())
    {
        QMessageBox *messageBox = new QMessageBox(QMessageBox::Question, "Удаление предмета", "Вы уверены, что хотите удалить предмет?");
        messageBox->addButton("Нет", QMessageBox::RejectRole);
        messageBox->addButton("Да", QMessageBox::AcceptRole);
        if (messageBox->exec() == QDialog::Accepted)
        {
            for (QWidget *wgt : wgtList)
            {
                delete wgt;
            }
            //Говорим таблице удалить класс
            cardTable->DeleteClass(_class);
        }
    }

    //Если такие учителя существуют, говорим пользователю, что удалить нельзя, пока не удалишь класс у найденных учителей
    else
    {
        QMessageBox warningBox(QMessageBox::Warning,
                           _class->getName(),
                           "Класс используется у следущих учителей:<br><br>",
                           0);
        for(Teacher *teacher : tempList)
        {
            warningBox.setText(warningBox.text() + "<b>" + teacher->getShortName() + "</b>" + "<br>");
        }
        warningBox.setText(warningBox.text() + "<br>Прежде чем продолжить, удалите класс у учителей");
        warningBox.addButton(QString::fromUtf8("Принять"),
                         QMessageBox::AcceptRole);
        warningBox.exec();
    }
}

QString classdialog::getStr(QString str)
{
    if (str.length() > 10)
    {
        str = str.left(8) + "...";
    }
    return str;
}

void classdialog::addClass(Class *_class)
//Добавить класс в список
{
    QList<QWidget*> wgtList;

    //Создаем строку соотв. данному классу в списке
    QLabel* nameLabel= new QLabel(getStr(_class->getName()));
    scrollLayout->addWidget(nameLabel, scrollLayout->rowCount(), 0, 1, 1, Qt::AlignLeft);
    wgtList.push_back(nameLabel);

    QLabel* sizeLabel = new QLabel(getStr(QString::number(_class->getCapacity())));
    scrollLayout->addWidget(sizeLabel, scrollLayout->rowCount()-1, 1, 1, 1, Qt::AlignCenter);
    wgtList.push_back(sizeLabel);

    int count = 0;
    for(Teacher *teacher : directory->teacherList)
    {
        for(QMap<Class*, int> classMap : teacher->disciplineMap)
        {
            for(Class* currentCLass : classMap.keys())
            {
                if (_class == currentCLass)
                {
                    count += classMap[currentCLass];
                }
            }
        }
    }
    QLabel *countLabel = new QLabel(QString::number(count));
    scrollLayout->addWidget(countLabel, scrollLayout->rowCount()-1, 2, 1, 1, Qt::AlignCenter);
    wgtList.push_back(countLabel);
    countLabel->setToolTip("Количество уроков у данного класса");

    QCheckBox *checkBox = new QCheckBox();
    scrollLayout->addWidget(checkBox, scrollLayout->rowCount()-1, 3, 1, 1, Qt::AlignCenter);
    checkBox->setChecked(_class->isVisible());
    connect(checkBox, &QCheckBox::stateChanged, [=]() {_class->setVisible(!_class->isVisible());});
    wgtList.push_back(checkBox);

    QPushButton* editButton = new QPushButton();
    QPushButton* deleteButton = new QPushButton();
    editButton->setStyleSheet(Styles::editButton());
    deleteButton->setStyleSheet(Styles::deleteButton());
    scrollLayout->addWidget(editButton, scrollLayout->rowCount()-1, 4);
    scrollLayout->addWidget(deleteButton, scrollLayout->rowCount()-1, 5);
    wgtList.push_back(editButton);
    wgtList.push_back(deleteButton);

    connect(editButton, &QPushButton::clicked, [=]() {editClass(_class, nameLabel, sizeLabel);});
    connect(deleteButton, &QPushButton::clicked, [=]() {deleteClass(_class, wgtList);});

}

void classdialog::createClass()
//Создать новый класс
{
    //Вызываем окно редактирования класса
    ClassEditDialog *inputDialog = new ClassEditDialog(cardTable->classList);
    if (inputDialog->exec() == QDialog::Accepted)
    {
        QString _name(inputDialog->nameLine->text());
        int _size = inputDialog->sizeBox->value();
        Class *newClass = new Class("Новый класс", cardTable);
        newClass->setName(_name);
        newClass->setCapacity(_size);
        cardTable->AddClass(newClass);
        addClass(newClass);

        if (height() < 500)
        {
            resize(width(), height()+26);
        }
        scrollArea->verticalScrollBar()->setMaximum(scrollArea->verticalScrollBar()->maximum()+26);
        scrollArea->verticalScrollBar()->setValue(scrollArea->verticalScrollBar()->maximum());
    }
}
