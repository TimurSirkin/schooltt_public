#include "exportteacherdialog.h"
#include "ui_exportteacherdialog.h"
#include "exportoptions.h"
#include <QDebug>

ExportTeacherDialog::ExportTeacherDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExportTeacherDialog)
{
    ui->setupUi(this);
    setWindowTitle("Настройка  файла xlsx");
    setFixedSize(size());
}

ExportTeacherDialog::~ExportTeacherDialog()
{
    delete ui;
}

int ExportTeacherDialog::getTeacherHeight()
{
    return ui->teacherHeight->value();
}

int ExportTeacherDialog::getTeacherWidth()
{
return ui->teacherWidth->value();
}

int ExportTeacherDialog::getTeacherFontSize()
{
return ui->teacherFontSize->value();
}

bool ExportTeacherDialog::getTeacherCursive()
{
    return ui->teacherCursive->isChecked();
}

bool ExportTeacherDialog::getTeacherBold()
{
    return ui->teacherBold->isChecked();
}

int ExportTeacherDialog::getHeaderHeight()
{
    return ui->headerHeight->value();
}

int ExportTeacherDialog::getHeaderWidth()
{
    return ui->headerWidth->value();
}

int ExportTeacherDialog::getHeaderFontSize()
{
    return ui->headerFontSize->value();
}

bool ExportTeacherDialog::getHeaderCursive()
{
return ui->headerCursive->isChecked();
}

bool ExportTeacherDialog::getHeaderBold()
{
    return ui->headerBold->isChecked();
}

int ExportTeacherDialog::getClassHeight()
{
    return ui->classHeight->value();
}

int ExportTeacherDialog::getClassWidth()
{
    return ui->classWidth->value();
}

int ExportTeacherDialog::getClassFontSize()
{
    return ui->classFontSize->value();
}

bool ExportTeacherDialog::getClassCursive()
{
    return ui->classCursive->isChecked();
}

bool ExportTeacherDialog::getClassBold()
{
    return ui->classBold->isChecked();
}

int ExportTeacherDialog::getDayHeight()
{
    return ui->dayHeight->value();
}

int ExportTeacherDialog::getDayWidth()
{
    return ui->dayWidth->value();
}

int ExportTeacherDialog::getDayFontSize()
{
    return ui->dayFontSize->value();
}

bool ExportTeacherDialog::getDayCursive()
{
    return ui->dayCursive->isChecked();
}

bool ExportTeacherDialog::getDayBold()
{
    return ui->dayBold->isChecked();
}

int ExportTeacherDialog::getLessonHeight()
{
    return ui->lessonHeight->value();
}

int ExportTeacherDialog::getLessonWidth()
{
    return ui->lessonWidth->value();
}

int ExportTeacherDialog::getLessonFontSize()
{
    return ui->lessonFontSize->value();
}

bool ExportTeacherDialog::getLessonCursive()
{
    return ui->lessonCursive->isChecked();
}

bool ExportTeacherDialog::getLessonBold()
{
    return ui->lessonBold->isChecked();
}

void ExportTeacherDialog::initDialog()
{
    ui->tableLayout->setAlignment(ui->classCursive, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->teacherCursive, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->headerCursive, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->dayCursive, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->lessonCursive, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->classBold, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->teacherBold, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->headerBold, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->dayBold, Qt::AlignCenter);
    ui->tableLayout->setAlignment(ui->lessonBold, Qt::AlignCenter);
}

void ExportTeacherDialog::setOptions(ExportOptions *exportOptions)
{
    ui->teacherHeight->setValue(exportOptions->t_teacherHeight);
    ui->teacherWidth->setValue(exportOptions->t_teacherWidth);
    ui->teacherFontSize->setValue(exportOptions->t_teacherFontSize);
    ui->teacherBold->setChecked(exportOptions->t_teacherBold);
    ui->teacherCursive->setChecked(exportOptions->t_teacherCursive);

    ui->headerHeight->setValue(exportOptions->t_headerHeight);
    ui->headerWidth->setValue(exportOptions->t_headerWidth);
    ui->headerFontSize->setValue(exportOptions->t_headerFontSize);
    ui->headerBold->setChecked(exportOptions->t_headerBold);
    ui->headerCursive->setChecked(exportOptions->t_headerCursive);

    ui->classHeight->setValue(exportOptions->t_classHeight);
    ui->classWidth->setValue(exportOptions->t_classWidth);
    ui->classFontSize->setValue(exportOptions->t_classFontSize);
    ui->classBold->setChecked(exportOptions->t_classBold);
    ui->classCursive->setChecked(exportOptions->t_classCursive);

    ui->lessonHeight->setValue(exportOptions->t_lessonHeight);
    ui->lessonWidth->setValue(exportOptions->t_lessonWidth);
    ui->lessonFontSize->setValue(exportOptions->t_lessonFontSize);
    ui->lessonBold->setChecked(exportOptions->t_lessonBold);
    ui->lessonCursive->setChecked(exportOptions->t_lessonCursive);

    ui->dayHeight->setValue(exportOptions->t_dayHeight);
    ui->dayWidth->setValue(exportOptions->t_dayWidth);
    ui->dayFontSize->setValue(exportOptions->t_dayFontSize);
    ui->dayBold->setChecked(exportOptions->t_dayBold);
    ui->dayCursive->setChecked(exportOptions->t_dayCursive);

    ui->headerHeight->setValue(ui->lessonHeight->value() + ui->dayHeight->value());
    ui->headerWidth->setValue(ui->teacherWidth->value());
    ui->lessonWidth->setValue(ui->classWidth->value());
    ui->teacherHeight->setValue(ui->classHeight->value());
    ui->dayWidth->setValue(ui->classWidth->value() * 7);
}

void ExportTeacherDialog::on_exportButton_clicked()
{
    accept();
}

void ExportTeacherDialog::on_teacherWidth_valueChanged(int arg1)
{
    ui->headerWidth->setValue(arg1);
}

void ExportTeacherDialog::on_dayHeight_valueChanged(int arg1)
{
    ui->headerHeight->setValue(arg1 + ui->lessonHeight->value());
}

void ExportTeacherDialog::on_lessonHeight_valueChanged(int arg1)
{
    ui->headerHeight->setValue(arg1 + ui->dayHeight->value());
}

void ExportTeacherDialog::on_classHeight_valueChanged(int arg1)
{
    ui->teacherHeight->setValue(arg1);
}

void ExportTeacherDialog::on_classWidth_valueChanged(int arg1)
{
    ui->lessonWidth->setValue(arg1);
    ui->dayWidth->setValue(arg1 * 7);
}
