#ifndef SORTBUTTON_H
#define SORTBUTTON_H

#include <QPushButton>
#include <QGridLayout>
#include <QLabel>

class SortButton : public QPushButton
{
    Q_OBJECT
public:
    explicit SortButton(QWidget *parent = nullptr);

    void setArrowEnadbled(bool value);

    void setFirstPix(QPixmap newPix);
    void setSecondPix(QPixmap newPix);
    void setHover_FirstPix(QPixmap newPix);
    void setHover_SecondPix(QPixmap newPix);
    void setActivated(bool value);
    void setTopToBottom(bool value);

    QPixmap getFirstPix();
    QPixmap getSecondPix();

private:
    bool isActivated = false;
    bool isTopToBottom = true;

    bool isWithArrow = true;

    QPixmap firstPix;
    QPixmap secondPix;
    QPixmap hover_firstPix;
    QPixmap hover_secondPix;
signals:
    void sortClicked(bool flag);

public slots:
    void slot_unactivated();

    // QWidget interface
protected:
    void mouseReleaseEvent(QMouseEvent *event);
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);
};

#endif // SORTBUTTON_H
