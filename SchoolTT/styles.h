#ifndef STYLES_H
#define STYLES_H

#include <QString>

class Styles
{
public:
    Styles();

    static const QString editButton();
    static const QString deleteButton();
    static const QString linkButton();
    static const QString dayButton();
    static const QString filterButton();
    static const QString answerButtons();
    static const QString cell();
};

#endif // STYLES_H
