#ifndef SCROLLAREA_H
#define SCROLLAREA_H

#include <QScrollArea>
#include <QTimer>

class ScrollArea : public QScrollArea
{
    Q_OBJECT
public:
    explicit ScrollArea(QWidget *parent = nullptr);
    ~ScrollArea();

private:
    bool isTimerExist = false;
    int horizontalLimit;
    int verticalLimit;
    int horizontalExLimit;
    int verticalExLimit;

    bool isVerticalLimit(int *value = nullptr);
    bool isHorizontalLimit(int *value = nullptr);
    bool isVerticalExLimit(int *value = nullptr);
    bool isHorizontalExLimit(int *value = nullptr);
    bool isLimit();

signals:

public slots:
    void slot_moveBars();

    // QWidget interface
protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void wheelEvent(QWheelEvent *event);
};

#endif // SCROLLAREA_H
