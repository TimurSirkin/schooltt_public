#include "directorywindow.h"
#include "QLabel"
#include "QGridLayout"

DirectoryWindow::DirectoryWindow()
{
    setMinimumSize( 300, 100);
    elementsGrid = new QGridLayout();
    okButton = new QPushButton("Принять");
    cancelButton = new QPushButton("Отменить");

    QGridLayout *mainGrid = new QGridLayout();
    setLayout(mainGrid);
    QGridLayout *buttonGrid = new QGridLayout();
    buttonGrid->setSizeConstraint(QLayout::SetMinimumSize);

    mainGrid->addLayout(elementsGrid, 0 , 0);
    mainGrid->addLayout(buttonGrid, 1 , 0);

    buttonGrid->addWidget(okButton, 0 , 0);
    buttonGrid->addWidget(cancelButton, 0, 1);
}
