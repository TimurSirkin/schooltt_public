#include "styles.h"

Styles::Styles()
{

}

const QString Styles::editButton()
{
    return "QPushButton{border-image: url(:/new/resource/Images/Settings.png); max-width: 20; max-height: 20; min-width: 20; min-height: 20;} "
           "QPushButton:hover{border-image: url(:/new/resource/Images/Settings2.png);}";
}

const QString Styles::deleteButton()
{
    return "QPushButton{border-image: url(:/new/resource/Images/Delete.png); max-width: 20; max-height: 20; min-width: 20; min-height: 20;} "
           "QPushButton:hover{border-image: url(:/new/resource/Images/Delete2.png);}";
}

const QString Styles::linkButton()
{
    return "QPushButton"
           "{"
            "background-color: rgb(200, 200, 200);"
            "border-width: 0px;"
            "border-radius: 5;"
            "margin: 0;"
            "padding: 0 5 0 5;"
            "color:rgb(70, 70, 200);"
           "}"
           "QPushButton:hover"
           "{background-color: rgb(170, 170, 170);}"
            "QPushButton:disabled"
            "{"
           "background-color: rgba(200, 200, 200, 100);"
           "color:rgba(70, 70, 200, 100);"
           "}";
}

const QString Styles::dayButton()
{
    return "QPushButton"
           "{"
            "background-color: rgba(200, 200, 200, 100);"
            "color: black;"
            "height: 30;"
            "font-weight: bold; "
            "text-align: center; "
            "border: 0;"
           "}"
          "QPushButton:hover"
          "{"
            "color: white;"
            "background-color: rgba(100, 100, 100, 50);"
          "}"
          "QPushButton:disabled"
          "{"
            "color: rgba(250, 250, 250, 125);"
           "}";
}

const QString Styles::filterButton()
{
    return "QPushButton"
           "{"
            //"max-height: 20;"
           "}"
            "QLabel"
           "{"
            "max-height: 0;"
           "}";
}

const QString Styles::answerButtons()
{
    return "QPushButton"
           "{"
            "width: 90;"
            "}";
}

const QString Styles::cell()
{
}
