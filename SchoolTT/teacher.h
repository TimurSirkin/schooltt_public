#ifndef TEACHER_H
#define TEACHER_H

#include <QString>
#include <QMap>
#include <QList>
#include <QObject>
#include "class.h"
#include "lesson.h"
#include "discipline.h"
class Directory;

class Teacher: public QObject
{
     Q_OBJECT
public:
    Teacher(Directory* directory);
    Teacher(int pId);
    QString getShortName() const;
    QString getFirstName() const;
    QString getSecondName() const;
    QString getThirdName() const;
    void setFirstName(QString name);
    void setSecondName(QString name);
    void setThirdName(QString name);

    int getId() const;

    QMap<Discipline*, QMap<Class*, int>> disciplineMap;
    QList<Lesson*> lessonList;

    void edit(QString pFirstName, QString pSecondName, QString pThirdName, QMap<Discipline*, QMap<Class*, int>> pDisciplineMap,  QList<Lesson*> pLessonList);
    void setBackgroundColor(QColor color);
    QColor getBackgroundColor();
    bool isColorSetted();

private:
    QString secondName;
    QString firstName;
    QString thirdName;

    QColor backgroundColor = QColor (177, 185, 198);

    bool colorSetted= false;
    int id;

signals:
    void disciplineDeleted(Discipline* discipline);
    void classDeleted(Discipline* discipline, Class* _class);
    void classAdded(Discipline* discipline, Class* _class);
    void deleted();
    void countChanged(Discipline* discipline, Class* _class, int value);
    void nameChanged();
};


#endif // TEACHER_H
