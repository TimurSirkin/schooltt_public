#ifndef DEFAULTDIALOG_H
#define DEFAULTDIALOG_H

#include <QDialog>
#include "QPushButton"
#include "QGridLayout"

class defaultDialog : public QDialog
{
    Q_OBJECT
public:
    explicit defaultDialog(QWidget *parent = nullptr);

protected:
    QGridLayout mainLayout = new QGridLayout();
    QPushButton *okButton = new QPushButton("Принять");
    QPushButton *cancelButton = new QPushButton("Отмена");

signals:

public slots:
};

#endif // DEFAULTDIALOG_H
