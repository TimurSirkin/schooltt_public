#ifndef TEACHERDIALOG_H
#define TEACHERDIALOG_H

#include <QDialog>
#include <QListWidget>
#include <QLabel>
#include <QScrollArea>
#include "cardtable.h"
#include "cardlist.h"
#include "teacher.h"
#include "directory.h"

class TeacherDialog : public QDialog
{
    Q_OBJECT
public:
    explicit TeacherDialog(CardTable *_cardTable, Directory *_directory, CardList *pCardList = nullptr, QWidget *parent = nullptr);
    void editTeacher(Teacher *teacher, QPushButton *nameLabel = nullptr, QPushButton *countLabel = nullptr, bool isForLooking = false);

private:
    QGridLayout *scrollLayout = new QGridLayout();
    QScrollArea *teacherScrollArea = new QScrollArea();
    Directory *directory;
    CardTable *cardTable;
    CardList *cardList;
    QWidget *addTeacher(Teacher *teacher);
    void deleteTeacher(Teacher *teacher, QList<QWidget *> wgtList);

public slots:
    void createTeacher();
};

#endif // TEACHERDIALOG_H
