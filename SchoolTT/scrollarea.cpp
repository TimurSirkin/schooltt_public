#include "scrollarea.h"
#include <QDebug>
#include <QMouseEvent>
#include <QTimer>
#include <QScrollBar>
#include <QApplication>
#include <QDragEnterEvent>

ScrollArea::ScrollArea(QWidget *parent) : QScrollArea(parent)
{
    setAcceptDrops(true);
    setMouseTracking(true);

}

ScrollArea::~ScrollArea()
{
}

bool ScrollArea::isVerticalLimit(int *value)
{
    if (mapFromGlobal(cursor().pos()).y() <= verticalLimit)
    {
        if (value != nullptr)
        {
            *value = -8;
        }
        return true;
    }
    if (height() - mapFromGlobal(cursor().pos()).y() <= verticalLimit)
    {
        if (value != nullptr)
        {
            *value = 8;
        }
        return true;
    }
    return false;
}

bool ScrollArea::isHorizontalLimit(int *value)
{
    if (mapFromGlobal(cursor().pos()).x() <= horizontalLimit)
    {
        if (value != nullptr)
        {
            *value = -8;
        }
        return true;
    }
    if (width() - mapFromGlobal(cursor().pos()).x() <= horizontalLimit)
    {
        if (value != nullptr)
        {
            *value = 8;
        }
        return true;
    }
    return false;
}

bool ScrollArea::isVerticalExLimit(int *value)
{
    if (mapFromGlobal(cursor().pos()).y() <= verticalExLimit)
    {
        if (value != nullptr)
        {
            *value = -5;
        }
        return true;
    }
    if (height() - mapFromGlobal(cursor().pos()).y() <= verticalExLimit)
    {
        if (value != nullptr)
        {
            *value = 5;
        }
        return true;
    }
    return false;
}

bool ScrollArea::isHorizontalExLimit(int *value)
{
    if (mapFromGlobal(cursor().pos()).x() <= horizontalExLimit)
    {
        if (value != nullptr)
        {
            *value = -5;
        }
        return true;
    }
    if (width() - mapFromGlobal(cursor().pos()).x() <= horizontalExLimit)
    {
        if (value != nullptr)
        {
            *value = 5;
        }
        return true;
    }
    return false;
}

bool ScrollArea::isLimit()
{
    if (isHorizontalLimit() || isVerticalLimit())
    {
        return true;
    }
    else
    {
        return false;
    }
}

void ScrollArea::slot_moveBars()
{
    if(!isLimit() || QApplication::mouseButtons() != Qt::LeftButton)
    {
        isTimerExist = false;
        QTimer *timer = dynamic_cast<QTimer*>(sender());
        delete timer;
    }

    int value;
    if(isVerticalLimit(&value))
    {
        verticalScrollBar()->setValue(verticalScrollBar()->value() + value);
        if(isVerticalExLimit(&value))
        {
            verticalScrollBar()->setValue(verticalScrollBar()->value() + value);
        }
    }
    if(isHorizontalLimit(&value))
    {
        horizontalScrollBar()->setValue(horizontalScrollBar()->value() + value);
        if(isHorizontalExLimit(&value))
        {
            horizontalScrollBar()->setValue(horizontalScrollBar()->value() + value);
        }
    }
}

void ScrollArea::dragEnterEvent(QDragEnterEvent *event)
{
    horizontalLimit = height()/10;
    verticalLimit = width()/10;
    horizontalExLimit = height()/20;
    verticalExLimit = width()/20;

    if(isLimit())
    {
        if (!isTimerExist)
        {
            isTimerExist = true;
            QTimer *timer = new QTimer(this);
            timer->start(25);
            connect(timer, SIGNAL(timeout()), this, SLOT(slot_moveBars()));
        }
        else
        {
            return;
        }
    }
}

void ScrollArea::wheelEvent(QWheelEvent *event)
{
    if (event->modifiers() != Qt::CTRL)
    {
        QScrollArea::wheelEvent(event);
    }
}
