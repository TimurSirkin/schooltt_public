#ifndef ROOMSDIALOG_H
#define ROOMSDIALOG_H

#include <QDialog>
#include <QScrollArea>
#include <QListWidget>
#include <QLabel>
#include "directory.h"
#include "room.h"

class RoomsDialog : public QDialog
{
    Q_OBJECT
public:
    explicit RoomsDialog(Directory *_directory, QWidget *parent = nullptr);

private:
    Directory* directory;
    QScrollArea *scrollArea = new QScrollArea();
    QGridLayout *scrollLayout = new QGridLayout();

    void editRoom(Room *room, QLabel *_numberLabel, QLabel *_capacityLabel);
    void deleteRoom(Room *room, QList<QWidget *> wgtList);

    QString getStr(QString str);

signals:

public slots:
    void addRoom(Room* room);
    void createRoom();
};

#endif // ROOMSDIALOG_H
