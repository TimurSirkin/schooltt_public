#ifndef DISCIPLINE_H
#define DISCIPLINE_H

#include <QString>
#include <QObject>

class Discipline : public QObject
{
     Q_OBJECT
public:
    Discipline(QString _Name, QObject *parent = nullptr);

    QString getName() const;
    QString getExportName() const;
    QString getExportForDoubleName() const;
    void setName(QString value);
    void setExportName(QString value);
    void setExportForDoubleName(QString value);

private:
    QString name;
    QString exportName;
    QString exportForDoubleName;

signals:
    void nameChanged();
};

#endif // DISCIPLINE_H
