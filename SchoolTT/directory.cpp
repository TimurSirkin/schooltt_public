#include "directory.h"
#include <QMap>
#include <QDebug>
#include "discipline.h"
#include "teacher.h"

//Справочник

Directory::Directory(QObject *parent) : QObject(parent)
//Конструктор
{

}

Discipline* Directory::addDiscipline(QString name)
//Создать новый предмет и добавить в список
{
    Discipline *newDiscipline = new Discipline(name);
    disciplineList.push_back(newDiscipline);
    return newDiscipline;
}

void Directory::addDiscipline(Discipline *discipline)
//Добавить предмет в список
{
    disciplineList.push_back(discipline);
}

void Directory::addRoom(Room *room)
//Добавить кабинет в список
{
    roomList.push_back(room);
}

void Directory::addTeacher(Teacher *teacher)
//Добавить учителя в список
{
    connect(teacher, &Teacher::classAdded, [teacher, this](Discipline* discipline, Class* _class) {classAdded(teacher, discipline, _class);});
    teacherList.push_back(teacher);
}

Discipline *Directory::getDiscipline(QString name)
{
    for (Discipline *discipline : disciplineList)
    {
        if (discipline->getName() == name)
        {
            return discipline;
        }
    }
    return nullptr;
}

Teacher *Directory::getTeacher(int id)
{
    for (Teacher *teacher : teacherList)
    {
        if (teacher->getId() == id)
        {
            return teacher;
        }
    }
}

