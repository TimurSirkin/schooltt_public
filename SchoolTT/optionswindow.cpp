#include "optionswindow.h"
#include "QGridLayout"
#include "QLabel"
#include "QPushButton"
#include "QStyle"
#include "styles.h"
#include <QSpacerItem>
#include <QScrollArea>

//Диалоговое окно настроек таблицы

OptionsWindow::OptionsWindow(CardTable *cardTable, QWidget *parent) : QDialog(parent)
//Конструктор
{
    setWindowTitle("Настройки таблицы");

    QGridLayout *mainLayout = new QGridLayout();
    setLayout(mainLayout);
    QScrollArea *scrollArea = new QScrollArea(this);
    mainLayout->addWidget(scrollArea, 0, 0, 1, 2);
    QWidget *widget = new QWidget(scrollArea);
    scrollArea->setWidget(widget);
    scrollArea->setWidgetResizable(true);
    QGridLayout *scrollLayout = new QGridLayout();
    widget->setLayout(scrollLayout);

    rowHeight->setMinimum(20);
    columnWidth->setMinimum(20);
    class_rowHeight->setMinimum(20);
    day_columnWidth->setMinimum(40);
    lesson_columnWidth->setMinimum(20);

    QLabel *class_rowHeightLabel = new QLabel("Высота строки классов");
    scrollLayout->addWidget(class_rowHeightLabel, 0, 0);
    scrollLayout->addWidget(class_rowHeight, 0, 1);
    class_rowHeight->setValue(cardTable->class_rowHeight);
    class_rowHeight->setToolTip(QString::number(class_rowHeight->minimum()) + " - " + QString::number(class_rowHeight->maximum()));


    QLabel *rowHeightLabel = new QLabel("Высота строк");
    scrollLayout->addWidget(rowHeightLabel, 1, 0);
    scrollLayout->addWidget(rowHeight, 1, 1);
    rowHeight->setValue(cardTable->rowHeight);
    rowHeight->setToolTip(QString::number(rowHeight->minimum()) + " - " + QString::number(rowHeight->maximum()));


    QLabel *spacerHeightLabel = new QLabel("Высота разделителей");
    scrollLayout->addWidget(spacerHeightLabel, 2, 0);
    scrollLayout->addWidget(spacerHeight, 2, 1);
    spacerHeight->setValue(cardTable->spacerHeight);
    spacerHeight->setToolTip(QString::number(spacerHeight->minimum()) + " - " + QString::number(spacerHeight->maximum()));


    QLabel *day_columnWidthLabel = new QLabel("Ширина столбца дней");
    scrollLayout->addWidget(day_columnWidthLabel, 3, 0);
    scrollLayout->addWidget(day_columnWidth, 3, 1);
    day_columnWidth->setValue(cardTable->day_columnWidth);
     day_columnWidth->setToolTip(QString::number( day_columnWidth->minimum()) + " - " + QString::number( day_columnWidth->maximum()));


    QLabel *lesson_columnWidthLabel = new QLabel("Ширина столбца уроков");
    scrollLayout->addWidget(lesson_columnWidthLabel, 4, 0);
    scrollLayout->addWidget(lesson_columnWidth, 4, 1);
    lesson_columnWidth->setValue(cardTable->lesson_columnWidth);
    lesson_columnWidth->setToolTip(QString::number(lesson_columnWidth->minimum()) + " - " + QString::number(lesson_columnWidth->maximum()));


    QLabel *columnWidthLabel = new QLabel("Ширина столбцов");
    scrollLayout->addWidget(columnWidthLabel, 5, 0);
    scrollLayout->addWidget(columnWidth, 5, 1);
    columnWidth->setValue(cardTable->columnWidth);
    columnWidth->setToolTip(QString::number(columnWidth->minimum()) + " - " + QString::number(columnWidth->maximum()));


    QSpacerItem *spacer = new QSpacerItem(0, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);
    scrollLayout->addItem(spacer, 6, 0);

    QLabel *minHeightForTextLabel = new QLabel("Минимальная высота ячейки для отображения в ней текста");
    scrollLayout->addWidget(minHeightForTextLabel, 7, 0);
    scrollLayout->addWidget(minHeightForText, 7, 1);
    minHeightForText->setValue(cardTable->minCellHeightForText);
    minHeightForText->setToolTip(QString::number(minHeightForText->minimum()) + " - " + QString::number(minHeightForText->maximum()));


    QLabel *minWidthForTextLabel = new QLabel("Минимальная ширина ячейки для отображения в ней текста");
    scrollLayout->addWidget(minWidthForTextLabel, 8, 0);
    scrollLayout->addWidget(minWidthForText, 8, 1);
    minWidthForText->setValue(cardTable->minCellWidthForText);
    minWidthForText->setToolTip(QString::number(minWidthForText->minimum()) + " - " + QString::number(minWidthForText->maximum()));


    QLabel *minHeightForDoubleTextLabel = new QLabel("Минимальная высота ячейки для отображения в ней всего текста");
    scrollLayout->addWidget(minHeightForDoubleTextLabel, 9, 0);
    scrollLayout->addWidget(minHeightForDoubleText, 9, 1);
    minHeightForDoubleText->setValue(cardTable->minCellHeightForDoubleText);
    minHeightForDoubleText->setToolTip(QString::number(minHeightForDoubleText->minimum()) + " - " + QString::number(minHeightForDoubleText->maximum()));


    QPushButton *okButton = new QPushButton("Принять");
    okButton->setStyleSheet(Styles::answerButtons());
    okButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogOkButton));
    QPushButton *cancelButton = new QPushButton("Отменить");
    cancelButton->setStyleSheet(Styles::answerButtons());
    cancelButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogCancelButton));
    mainLayout->addWidget(okButton, 1, 0, 1, 1,  Qt::AlignLeft);
    mainLayout->addWidget(cancelButton, 1, 1, 1, 1,  Qt::AlignRight);
    connect(okButton, SIGNAL(clicked()), SLOT(accept()));
    connect(cancelButton, SIGNAL(clicked()), SLOT(reject()));

    class_rowHeight->setFocus();
    setFixedSize(sizeHint());
}
