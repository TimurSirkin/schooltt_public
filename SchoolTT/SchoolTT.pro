#-------------------------------------------------
#
# Project created by QtCreator 2018-09-12T18:45:03
#
#-------------------------------------------------

QT       += core gui

include(3rdparty/qtxlsx/src/xlsx/qtxlsx.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SchoolTT
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    exportteacherdialog.cpp \
        main.cpp \
        mainwindow.cpp \
    tableelement.cpp \
    day.cpp \
    lesson.cpp \
    class.cpp \
    cell.cpp \
    cardtable.cpp \
    teacher.cpp \
    discipline.cpp \
    directory.cpp \
    optionswindow.cpp \
    disciplinedialog.cpp \
    room.cpp \
    roomsdialog.cpp \
    roomeditdialog.cpp \
    classdialog.cpp \
    classeditdialog.cpp \
    teacherdialog.cpp \
    disciplineeditdialog.cpp \
    teachereditdialog.cpp \
    teacher_disciplineeditdialog.cpp \
    card.cpp \
    cardeditdialog.cpp \
    widgetmimedata.cpp \
    cardlist.cpp \
    sortbutton.cpp \
    filtereditdialog.cpp \
    styles.cpp \
    cardoptionwindow.cpp \
    startwidget.cpp \
    newfiledialog.cpp \
    namevalidator.cpp \
    dialogfeatures.cpp \
    scrollarea.cpp \
    teacherlistdialog.cpp \
    newfilebydialog.cpp \
    exportdialog.cpp \
    exportoptions.cpp

HEADERS += \
    exportteacherdialog.h \
        mainwindow.h \
    tableelement.h \
    day.h \
    lesson.h \
    class.h \
    cell.h \
    cardtable.h \
    teacher.h \
    discipline.h \
    directory.h \
    optionswindow.h \
    disciplinedialog.h \
    room.h \
    roomsdialog.h \
    roomeditdialog.h \
    classdialog.h \
    classeditdialog.h \
    teacherdialog.h \
    disciplineeditdialog.h \
    teachereditdialog.h \
    teacher_disciplineeditdialog.h \
    card.h \
    cardeditdialog.h \
    widgetmimedata.h \
    cardlist.h \
    sortbutton.h \
    filtereditdialog.h \
    styles.h \
    cardoptionwindow.h \
    startwidget.h \
    newfiledialog.h \
    namevalidator.h \
    dialogfeatures.h \
    scrollarea.h \
    teacherlistdialog.h \
    newfilebydialog.h \
    exportdialog.h \
    exportoptions.h

FORMS += \
    exportteacherdialog.ui \
        mainwindow.ui \
    newfiledialog.ui \
    mainwindow.ui \
    newfilebydialog.ui \
    exportdialog.ui

RESOURCES += \
    images.qrc
