#ifndef CLASSEDITDIALOG_H
#define CLASSEDITDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QCheckBox>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QPushButton>
#include "class.h"
#include "directory.h"

class ClassEditDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ClassEditDialog(QVector<Class*> pClassVector, Class *pClass = nullptr, QWidget *parent = nullptr);

    QLineEdit *nameLine = new QLineEdit();
    QSpinBox *sizeBox = new QSpinBox();
    QGridLayout *newLayout = new QGridLayout();
    QLabel *nameLabel = new QLabel("Нименование");
    QLabel *sizeLabel = new QLabel("Количество учеников");
    QPushButton *okButton = new QPushButton("Принять");
    QPushButton *cancelButton = new QPushButton("Отмена");

private:
    void checkName();
    Class *_class;
    QVector<Class*> classVector;

};

#endif // CLASSEDITDIALOG_H
