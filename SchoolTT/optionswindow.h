#ifndef OPTIONSWINDOW_H
#define OPTIONSWINDOW_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QSpinBox>
#include <QWidget>
#include "cardtable.h"

class OptionsWindow: public QDialog
{
public:
    OptionsWindow(CardTable* cardTable, QWidget *parent = nullptr);

    QSpinBox *spacerHeight = new QSpinBox();
    QSpinBox *rowHeight  = new QSpinBox();
    QSpinBox *columnWidth = new QSpinBox();
    QSpinBox *day_columnWidth = new QSpinBox();
    QSpinBox *class_rowHeight = new QSpinBox();
    QSpinBox *lesson_columnWidth = new QSpinBox();
    QSpinBox *minHeightForText = new QSpinBox();
    QSpinBox *minWidthForText = new QSpinBox();
    QSpinBox *minHeightForDoubleText = new QSpinBox();
};

#endif // OPTIONSWINDOW_H
