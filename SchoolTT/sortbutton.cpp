#include "sortbutton.h"
#include <QStyle>
#include <QMouseEvent>
#include <QDebug>

SortButton::SortButton(QWidget *parent) : QPushButton(parent)
{
}

void SortButton::setArrowEnadbled(bool value)
//Говорит о том, будет у кнопки стрелка - или нет (у Кастомной сортировки ее нет)
{
    isWithArrow = value;
}

void SortButton::setSecondPix(QPixmap newPix)
//Иконка сортировки снизу вверх
{
    secondPix = newPix;
}

void SortButton::setHover_FirstPix(QPixmap newPix)
//Иконка сортировки сверху вниз, при наведении курсора
{
    hover_firstPix = newPix;
}

void SortButton::setHover_SecondPix(QPixmap newPix)
//Иконка сортировки снизу вверх, при наведении курсора
{
    hover_secondPix = newPix;
}

void SortButton::setActivated(bool value)
//Делает кнопку активной - поставлен текущий режим сортировки
{
    isActivated = value;
    if (isActivated)
    {
        setStyleSheet("QPushButton {background-color: rgb(160, 200, 230);}");
    }
    else
    {
        setStyleSheet("");
    }
}

void SortButton::setTopToBottom(bool value)
{
    isTopToBottom = value;
}

QPixmap SortButton::getFirstPix()
{
    return firstPix;
}

QPixmap SortButton::getSecondPix()
{
    return  secondPix;
}

void SortButton::setFirstPix(QPixmap newPix)
//Деволтная иконка / сортировка сверху вниз
{
    firstPix = newPix;
    setIcon(firstPix);
    //setStyleSheet("QPushButton{ border-image: url(:/new/resource/Images/Dis_down.png) }");
}

void SortButton::slot_unactivated()
//Обработчик события разактивации
{
    setActivated(false);
}

void SortButton::mouseReleaseEvent(QMouseEvent *event)
//Переопределяю событие отпускания мыши
{
    if(!rect().contains(event->pos()))
    {
       return;
    }

    QPushButton::mouseReleaseEvent(event);

    if(isWithArrow)
    {
        if(isActivated)
        {
            isTopToBottom = !isTopToBottom;
            emit sortClicked(isTopToBottom);
        }
        else
        {
            setActivated(true);
            emit sortClicked(isTopToBottom);
        }

        if(isTopToBottom)
        {
            setIcon(hover_firstPix);
        }
        else
        {
            setIcon(hover_secondPix);
        }
    }
    else
    {
        if(!isActivated)
        {
            setActivated(true);
        }
    }
}

void SortButton::enterEvent(QEvent *event)
{
    if (!isEnabled())
    {
        return;
    }
    if(isTopToBottom || !isWithArrow)
    {
        setIcon(hover_firstPix);
    }
    else
    {
        setIcon(hover_secondPix);
    }
}

void SortButton::leaveEvent(QEvent *event)
{
    if (!isEnabled())
    {
        return;
    }

    if(isTopToBottom || !isWithArrow)
    {
        setIcon(firstPix);
    }
    else
    {
        setIcon(secondPix);
    }
}
