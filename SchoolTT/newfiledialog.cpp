#include "newfiledialog.h"
#include "ui_newfiledialog.h"
#include <QFileDialog>

NewFileDialog::NewFileDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewFileDialog)
{
    ui->setupUi(this);
    ui->nameLine->setFocus();
    ui->pathLine->setEnabled(false);

    daySpin = ui->daySpinBox;
    lessonSpin = ui->lessonSpinBox;
    classSpin = ui->classSpinBox;
    nameLine = ui->nameLine;
    pathLine = ui->pathLine;
    ui->createButton->setEnabled(false);

    setFixedSize(sizeHint());
}

NewFileDialog::~NewFileDialog()
{
    delete ui;
}

void NewFileDialog::slot_path()
{
    QString filePath = QFileDialog::getExistingDirectory();
    ui->pathLine->setText(filePath);
    ui->createButton->setEnabled(ui->pathLine->text() != "");
}
