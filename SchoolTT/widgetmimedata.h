#ifndef WIDGETMIMEDATA_H
#define WIDGETMIMEDATA_H

#include <QMimeData>

class WidgetMimeData: public QMimeData
{
public:
    WidgetMimeData();

    QString getType();

    void setWidget(QWidget* value, QString pFlag = "none");

    QWidget* widget() const;

private:
    QWidget* pointer;
    QString flag;
};

#endif // WIDGETMIMEDATA_H
