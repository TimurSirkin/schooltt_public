#ifndef DISCIPLINEEDITDIALOG_H
#define DISCIPLINEEDITDIALOG_H

#include <QWidget>
#include <QDialog>
#include <QLabel>
#include <QCheckBox>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include "directory.h"

class DisciplineEditDialog : public QDialog
{
    Q_OBJECT
public:
    explicit DisciplineEditDialog(Directory *pDirectory, Discipline *pDiscipline = nullptr, QWidget *parent = nullptr);

    QLineEdit *nameLine = new QLineEdit();
    QLineEdit *exportNameLine = new QLineEdit();
    QLineEdit *exportForDoubleNameLine = new QLineEdit();
    QGridLayout *newLayout = new QGridLayout();
    QPushButton* okButton = new QPushButton( "Принять" );
    QLabel *namelabel = new QLabel("Наименование");
    QLabel *exportNameLabel = new QLabel("Сокращенное");
    QLabel *exportForDoubleNameLabel = new QLabel("При двух занятиях на одном уроке");
    QPushButton* cancelButton = new QPushButton( "Отменить" );

private:
    Directory *directory;
    Discipline *discipline;

    void checkName();
};

#endif // DISCIPLINEEDITDIALOG_H
