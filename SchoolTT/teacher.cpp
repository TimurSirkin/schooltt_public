#include "teacher.h"
#include <QDebug>
#include "mainwindow.h"
#include "directory.h"

Teacher::Teacher(Directory *directory)
{
    int newId = 0;
    for (Teacher *teacher : directory->teacherList)
    {
        if (teacher->getId() > newId)
        {
            newId = teacher->getId();
        }
    }
    newId++;
    id = newId;
}

Teacher::Teacher(int pId)
{
    id = pId;
}

QString Teacher::getShortName() const
{
    QString str;
    str += secondName + " ";
    str += firstName.left(1) + ".";
    str += thirdName.left(1) + ".";
    return str;
}

QString Teacher::getFirstName() const
{
    return firstName;
}

QString Teacher::getSecondName() const
{
    return secondName;
}

QString Teacher::getThirdName() const
{
    return thirdName;
}

void Teacher::setFirstName(QString name)
{
    firstName = name;
}

void Teacher::setSecondName(QString name)
{
    secondName = name;
}

void Teacher::setThirdName(QString name)
{
    thirdName = name;
}

int Teacher::getId() const
{
    return id;
}

void Teacher::edit(QString pFirstName, QString pSecondName, QString pThirdName, QMap<Discipline *, QMap<Class *, int> > pDisciplineMap, QList<Lesson *> pLessonList)
{
    firstName = pFirstName;
    secondName = pSecondName;
    thirdName = pThirdName;
    emit nameChanged();
    disciplineMap = pDisciplineMap;
    lessonList = pLessonList;
}

void Teacher::setBackgroundColor(QColor color)
{
    backgroundColor = color;
    colorSetted = true;
}

QColor Teacher::getBackgroundColor()
{
    return backgroundColor;
}

bool Teacher::isColorSetted()
{
    return colorSetted;
}
