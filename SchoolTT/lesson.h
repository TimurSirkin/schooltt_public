#ifndef LESSON_H
#define LESSON_H

#include "tableelement.h"
#include "cell.h"


class Lesson : public TableElement
{
public:
   Lesson(QString text, QWidget *parent);

   QAction *action_delete = new QAction("Удалить последний урок");
   QAction *action_clear = new QAction("Убрать карточки");

   // QWidget interface
protected:
   void contextMenuEvent(QContextMenuEvent *event);
};

#endif // LESSON_H
