#include "dialogfeatures.h"
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QScrollBar>
#include <QDebug>

DialogFeatures::DialogFeatures(QObject *parent) : QObject(parent)
{

}

void DialogFeatures::sort(QGridLayout *layout)
{
    for(int roundIndex = 0; roundIndex < layout->rowCount(); roundIndex++)
    {
        for (int rowIndex = 1; rowIndex < layout->rowCount()-1; rowIndex++)
        {
            QString firstName = getName(rowIndex, layout);
            QString secondName = getName(rowIndex+1, layout);
            if (firstName == "" || secondName == "")
            {
                continue;
            }
            if (firstName > secondName)
            {
                swapRows(rowIndex, rowIndex+1, layout);
            }
        }
    }
}

QString DialogFeatures::getName(int rowIndex, QGridLayout *layout)
{
    QLayoutItem *item = layout->itemAtPosition(rowIndex, 0);
    if(item == nullptr)
    {
        return "";
    }
    QPushButton *nameButton = dynamic_cast<QPushButton*>(item->widget());
    if (nameButton != nullptr)
    {
        return nameButton->text();
    }
    else
    {
        QLabel *label = dynamic_cast<QLabel*>(item->widget());
        if (label != nullptr)
        {
            return label->text();
        }
    }
}

void DialogFeatures::swapRows(int firstIndex, int secondIndex, QGridLayout *layout)
{
    for (int columnIndex = 0; columnIndex < layout->columnCount(); columnIndex++)
    {
        QLayoutItem *firstItem = layout->itemAtPosition(firstIndex, columnIndex);
        QLayoutItem *secondItem = layout->itemAtPosition(secondIndex, columnIndex);
        QWidget *firstWidget = firstItem->widget();
        QWidget *secondWidget = secondItem->widget();
        layout->removeWidget(firstWidget);
        layout->removeWidget(secondWidget);
        if(columnIndex == 0)
        {
            layout->addWidget(secondWidget, firstIndex, columnIndex, Qt::AlignLeft);
            layout->addWidget(firstWidget, secondIndex, columnIndex, Qt::AlignLeft);
        }
        else
        {
            if (columnIndex == 1)
            {
                layout->addWidget(secondWidget, firstIndex, columnIndex, Qt::AlignCenter);
                layout->addWidget(firstWidget, secondIndex, columnIndex, Qt::AlignCenter);
            }
            else
            {
                layout->addWidget(secondWidget, firstIndex, columnIndex);
                layout->addWidget(firstWidget, secondIndex, columnIndex);
            }
        }
    }
}

void DialogFeatures::scrollTo(QWidget *wgt, QScrollArea *scrollArea, QGridLayout *layout)
{
    int wgtRowIndex = 0;
    for (int rowIndex = 0; rowIndex < layout->rowCount(); rowIndex++)
    {
        if (layout->itemAtPosition(rowIndex, 0) == nullptr)
        {
            wgtRowIndex--;
            continue;
        }
        if (layout->itemAtPosition(rowIndex, 0)->widget() == wgt)
        {
            wgtRowIndex = rowIndex;
        }
    }
    double placeProp = double(wgtRowIndex+1) / double(layout->rowCount());
    int newBarValue = double(scrollArea->verticalScrollBar()->maximum())  * placeProp;
    scrollArea->verticalScrollBar()->setValue(newBarValue);
}
