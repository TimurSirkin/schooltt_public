#include "roomsdialog.h"
#include <QGridLayout>
#include <QMessageBox>
#include <QLabel>
#include <QPushButton>
#include <QString>
#include "styles.h"
#include "room.h"
#include "discipline.h"
#include"roomeditdialog.h"

//Диалоговое окно для добавления, удаления и редактирования кабинетов

RoomsDialog::RoomsDialog(Directory *_directory, QWidget *parent) : QDialog(parent)
  //Конструктор
{
    directory = _directory;
    setWindowTitle("Справочник кабинетов");
    QGridLayout *mainLayout = new QGridLayout();
    setLayout(mainLayout);
    QPushButton *addButton = new QPushButton("Добавить кабинет");
    mainLayout->addWidget(addButton, 1, 0, 1, 2);
    connect(addButton, SIGNAL(clicked()), SLOT(createRoom()));

    mainLayout->addWidget(scrollArea,0,0,1, 2);
    QWidget *wgt = new QWidget();
    scrollArea->setWidget(wgt);
    scrollArea->setWidgetResizable(true);
    wgt->setLayout(scrollLayout);
    wgt->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    scrollLayout->addWidget(new QLabel("<b>Название"), scrollLayout->rowCount(), 0, 1, 1, Qt::AlignLeft);
    scrollLayout->addWidget(new QLabel("<b>Вместимость"), scrollLayout->rowCount()-1, 1, 1, 1, Qt::AlignCenter);

    for (Room *room : directory->roomList)
    {
        addRoom(room);
    }

    setFixedWidth(300);
}

void RoomsDialog::editRoom(Room *room, QLabel *_numberLabel, QLabel *_capacityLabel)
//Редактировать кабинет
{
    RoomEditDialog *inputDialog = new RoomEditDialog();
    inputDialog->numberLine->setText(room->number);
    inputDialog->capacityBox->setValue(room->capacity);
    if (inputDialog->exec() == QDialog::Accepted)
    {
        if (inputDialog->numberLine->text() == "")
        {
            return;
        }
        QString _number(inputDialog->numberLine->text());
        int _capacity = inputDialog->capacityBox->value();
        room->number = _number;
        room->capacity = _capacity;
        _numberLabel->setText(getStr(_number));
        _capacityLabel->setText(getStr(QString::number(_capacity)));
    }
}

void RoomsDialog::deleteRoom(Room *room, QList<QWidget*> wgtList)
//Удалить кабинет
{
    QMessageBox *messageBox = new QMessageBox(QMessageBox::Question, room->number, "Вы уверены, что хотите удалить кабинет?");
    messageBox->addButton("Нет", QMessageBox::RejectRole);
    messageBox->addButton("Да", QMessageBox::AcceptRole);
    if (messageBox->exec() == QDialog::Accepted)
    {
        directory->roomList.removeOne(room);
        for (QWidget *wgt : wgtList)
        {
            delete wgt;
        }
        delete room;
    }
}

QString RoomsDialog::getStr(QString str)
{
    if (str.length() > 11)
    {
        str = str.left(9) + "...";
    }
    return str;
}

void RoomsDialog::addRoom(Room *room)
//Добавить кабинет в список кабинетов окна
{
    QList<QWidget*> wgtList;

    QLabel* nameLabel= new QLabel(getStr(room->number));
    scrollLayout->addWidget(nameLabel, scrollLayout->rowCount(), 0, 1, 1, Qt::AlignLeft);
    wgtList.push_back(nameLabel);

    QLabel* capacityLabel = new QLabel(getStr(QString::number(room->capacity)));
    scrollLayout->addWidget(capacityLabel, scrollLayout->rowCount()-1, 1, 1, 1, Qt::AlignCenter);
    wgtList.push_back(capacityLabel);

    QPushButton* editButton = new QPushButton( );
    QPushButton* deleteButton = new QPushButton();
    editButton->setStyleSheet(Styles::editButton());
    deleteButton->setStyleSheet(Styles::deleteButton());
    scrollLayout->addWidget(editButton, scrollLayout->rowCount()-1, 2);
    scrollLayout->addWidget(deleteButton, scrollLayout->rowCount()-1, 3);
    wgtList.push_back(editButton);
    wgtList.push_back(deleteButton);

    connect(editButton, &QPushButton::clicked, [room, nameLabel, capacityLabel, this]() {editRoom(room, nameLabel, capacityLabel);});
    connect(deleteButton, &QPushButton::clicked, [=]() {deleteRoom(room, wgtList);});
}

void RoomsDialog::createRoom()
//Создать новый кабинет
{
    Room *newRoom = new Room();
    RoomEditDialog *inputDialog = new RoomEditDialog();
    if (inputDialog->exec() == QDialog::Accepted)
    {
        //Проверка на непустоту названия
        if (inputDialog->numberLine->text() == "")
        {
            return;
        }
        QString _number(inputDialog->numberLine->text());
        int _capacity = inputDialog->capacityBox->value();
        newRoom->number = _number;
        newRoom->capacity = _capacity;
        addRoom(newRoom);
        directory->addRoom(newRoom);
    }

    //Создаем новый кабинет до вызова окна редактирования, поэтому если нажать кнопку отмены - кабинет удалится
    else
    {
        delete newRoom;
    }
}
