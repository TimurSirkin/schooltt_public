#ifndef NEWFILEDIALOG_H
#define NEWFILEDIALOG_H

#include <QDialog>
#include <QSpinBox>

namespace Ui {
class NewFileDialog;
}

class NewFileDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewFileDialog(QWidget *parent = nullptr);
    ~NewFileDialog();

    QSpinBox *daySpin;
    QSpinBox *lessonSpin;
    QSpinBox *classSpin;
    QLineEdit *nameLine;
    QLineEdit *pathLine;

private:
    Ui::NewFileDialog *ui;

private slots:
    void slot_path();
};

#endif // NEWFILEDIALOG_H
