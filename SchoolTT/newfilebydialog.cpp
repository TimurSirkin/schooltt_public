#include "newfilebydialog.h"
#include "ui_newfilebydialog.h"
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>

NewFileByDialog::NewFileByDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewFileByDialog)
{
    ui->setupUi(this);
    ui->nameLine->setFocus();
    ui->pathLine->setEnabled(false);
    nameLine = ui->nameLine;
    pathLine = ui->pathLine;
    ui->createButton->setEnabled(false);

    setWindowTitle("Новое расписание");
    setFixedSize(sizeHint());
}

NewFileByDialog::~NewFileByDialog()
{
    delete ui;
}

void NewFileByDialog::slot_path()
{
    QString filePath = QFileDialog::getExistingDirectory();
    ui->pathLine->setText(filePath);
    ui->createButton->setEnabled(ui->pathLine->text() != "");
}

void NewFileByDialog::slot_create()
{
    fileName = QFileDialog::getOpenFileName();
    if (fileName != "")
    {
        accept();
    }
}
