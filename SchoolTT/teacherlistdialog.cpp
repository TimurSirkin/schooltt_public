#include "teacherlistdialog.h"
#include <QGridLayout>
#include <QLabel>
#include <QScrollArea>
#include <QPushButton>
#include <QDebug>
#include <QCheckBox>
#include <QStyle>
#include "styles.h"
#include "teacherdialog.h"

TeacherListDialog::TeacherListDialog(CardTable *pCardTable,  QWidget *parent) : QDialog(parent)
{
    setWindowTitle("Список учителей");

    cardTable = pCardTable;
    directory = cardTable->directory;
    QGridLayout *mainLayout = new QGridLayout();
    setLayout(mainLayout);

    QScrollArea *scrollArea = new QScrollArea();
    QWidget *widget = new QWidget();
    scrollArea->setWidget(widget);
    scrollArea->setWidgetResizable(true);
    widget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    widget->setLayout(scrollLayout);

    mainLayout->addWidget(mainBox, mainLayout->rowCount(), 0, 1, 2, Qt::AlignLeft);

    QPushButton *okButton = new QPushButton("Принять");
    QPushButton *cancelButton = new QPushButton("Отменить");
    okButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogOkButton));
    okButton->setStyleSheet(Styles::answerButtons());
    cancelButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogCancelButton));
    cancelButton->setStyleSheet(Styles::answerButtons());
    connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    mainLayout->addWidget(scrollArea, mainLayout->rowCount(), 0, 1, 2);
    mainLayout->addWidget(okButton, mainLayout->rowCount(), 0, 1, 1, Qt::AlignLeft);
    mainLayout->addWidget(cancelButton, mainLayout->rowCount()-1, 1, 1, 1, Qt::AlignRight);

    for (Teacher *teacher : directory->teacherList)
    {
        addTeacher(teacher);
    }

    setFixedWidth(sizeHint().width());
}

QList<Teacher *> TeacherListDialog::getTeacherList()
{
    return teacherList;
}

void TeacherListDialog::addTeacher(Teacher *teacher)
{
    QPushButton* nameButton= new QPushButton(teacher->getShortName());
    nameButton->setStyleSheet(Styles::linkButton());
    scrollLayout->addWidget(nameButton, scrollLayout->rowCount(), 0, 1, 1, Qt::AlignLeft);
    connect(nameButton, &QPushButton::clicked, [=]()
    {
        TeacherDialog *teacherDialog = new  TeacherDialog(cardTable,  directory);
        teacherDialog->editTeacher(teacher, nullptr, nullptr, true);
    });

    QCheckBox *checkBox = new QCheckBox();
    scrollLayout->addWidget(checkBox, scrollLayout->rowCount()-1, 1, 1, 1, Qt::AlignRight);
    connect(checkBox, &QCheckBox::stateChanged, [=]()
    {
        if(checkBox->isChecked())
        {
            teacherList.push_back(teacher);
        }
        else
        {
            teacherList.removeOne(teacher);
        }
    });
    connect(mainBox, &QCheckBox::stateChanged, [=]()
    {
        checkBox->setChecked(mainBox->isChecked());
    });
}
