#include "class.h"
#include <QDrag>
#include <QApplication>
#include <QDebug>
#include "cardtable.h"
#include "widgetmimedata.h"
#include "classeditdialog.h"

Class::Class(QString text, QWidget *parent):TableElement(parent)
  //Конструктор класса
{
    _backgroundColor = QColor (200,200,200);
    name = text;
    fontSize = 10;

    defaultHeight = 30;
    defaultWidth = 50;

    connect(action_clear, &QAction::triggered, [this] () {emit signal_clear();});

    setAcceptDrops(true);

    CardTable *cardTable = dynamic_cast<CardTable*>(parent);
    id = cardTable->getClassId();
}

int Class::getCapacity() const
//Геттер размера класса (кол-во учеников)
{
    return capacity;
}

void Class::setCapacity(int value)
//Сеттер размера
{
    capacity = value;
}

void Class::edit()
{
    ClassEditDialog *inputDialog = new ClassEditDialog((dynamic_cast<CardTable*>(parent()))->classList, this);
    inputDialog->nameLine->setText(getName());
    inputDialog->sizeBox->setValue(getCapacity());
    if (inputDialog->exec() == QDialog::Accepted)
   {
        if (inputDialog->nameLine->text() == "")
        {
            return;
        }
        QString _name(inputDialog->nameLine->text());
        int _size = inputDialog->sizeBox->value();
        setName(_name);
        setCapacity(_size);
    }
}

int Class::getId() const
{
    return id;
}

void Class::dragEnterEvent(QDragEnterEvent *event)
//Переопределять событие попадания курсора во время перетаскивания на виджет
{
    if (event->mimeData()->hasFormat("application/widget/class") )
    {
        event->acceptProposedAction();
    }
}

void Class::dropEvent(QDropEvent *event)
//Переопределяю событие броска на класс
{
    const WidgetMimeData* mimeData = dynamic_cast<const WidgetMimeData*> (event->mimeData());
    Class *guestClass = (Class*)(mimeData->widget());
    emit signal_wantToSwapWith(guestClass);
}

void Class::mousePressEvent(QMouseEvent *event)
//Переопределяю событие нажайтие на мышь
{
    if (event->button() == Qt::LeftButton)
    {
        dragStartPosition = event->pos();
    }
}

void Class::mouseMoveEvent(QMouseEvent *event)
//Переопрделяю событие движения мыши
{
    //Если не зажата левая кнопка мыши - return
    if (!(event->buttons() & Qt::LeftButton))
    {
        return;
    }
    //Смотрю чтобы мышь продвинулась на расстояние startDragDisctance, иначе - return
    if ((event->pos() - dragStartPosition).manhattanLength() < QApplication::startDragDistance())
    {
        return;
    }

    //Задаю параметры перетаскивания
    QDrag *drag = new QDrag(this);

    //Создаю перемунную, которая сможет хранить ссылку на виджет
    WidgetMimeData* mimeData = new WidgetMimeData;
    mimeData->setWidget(this);

    drag->setMimeData(mimeData);
    drag->exec();
}

void Class::contextMenuEvent(QContextMenuEvent *event)
//Переопределя. метод вызова конт. меню
{
    QMenu *contextMenu = new QMenu();
    contextMenu->addAction(action_clear);
    contextMenu->addAction("Сделать невидимым", this, [=](){setVisible(false);});
    contextMenu->addAction("Редактировать...", this, [=](){edit();});
    contextMenu->exec(event->globalPos());
}

