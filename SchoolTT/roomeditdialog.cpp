#include "roomeditdialog.h"
#include <QCheckBox>
#include <QListWidgetItem>
#include <QDialog>
#include <QStyle>
#include "styles.h"
#include "discipline.h"


//Диалоговое окно редактирования кабинетов

RoomEditDialog::RoomEditDialog(QWidget *parent) : QDialog(parent)
  //Конструктор
{
    this->setWindowTitle("Редактирование кабинета");
    okButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogOkButton));
    okButton->setStyleSheet(Styles::answerButtons());
    cancelButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogCancelButton));
    cancelButton->setStyleSheet(Styles::answerButtons());
    connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    newLayout->addWidget(numberLabel, 0, 0, 1, 2);
    newLayout->addWidget(numberLine, 1,0, 1, 2);
    newLayout->addWidget(capacityLabel, 2, 0, 1, 2);
    newLayout->addWidget(capacityBox,3,0,1,2);
    newLayout->addWidget(okButton,4,0);
    newLayout->addWidget(cancelButton,4,1);
    this->setLayout(newLayout);
}
