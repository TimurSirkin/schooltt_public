#ifndef DIALOGFEATURES_H
#define DIALOGFEATURES_H

#include <QObject>
#include <QLayout>
#include <QScrollArea>

class DialogFeatures : public QObject
{
    Q_OBJECT
public:
    explicit DialogFeatures(QObject *parent = nullptr);

    static void sort(QGridLayout *layout);
    static QString getName(int rowIndex, QGridLayout *layout);
    static void swapRows(int firstIndex, int secondIndex, QGridLayout *layout);
    static void scrollTo(QWidget *wgt, QScrollArea* scrollArea, QGridLayout *layout);
};

#endif // DIALOGFEATURES_H
