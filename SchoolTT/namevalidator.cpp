#include "namevalidator.h"
#include <QDebug>

NameValidator::NameValidator(QObject *parent) : QValidator(parent)
{

}

QValidator::State NameValidator::validate(QString &input, int &position) const
{
    if (input.isEmpty())
    {
        return Acceptable;
    }

    //Удаляю пробелы в начале
    while (!input.isEmpty() && input[0] == " ")
    {
        input.remove(0,1);
    }

    //Перевожу первый символ в верхний ригистр
    if (!input.isEmpty())
    {
        input[0] = input[0].toUpper();
    }

    return Acceptable;
}
