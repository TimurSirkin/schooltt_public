#ifndef DIRECTORYWINDOW_H
#define DIRECTORYWINDOW_H
#include "QDialog"
#include "QPushButton"
#include "QGridLayout"


class DirectoryWindow : public QDialog
{
public:
    DirectoryWindow();

    QPushButton *okButton;
    QPushButton *cancelButton;
    QGridLayout *elementsGrid;
};

#endif // DIRECTORYWINDOW_H
