#include "filtereditdialog.h"
#include <QStyle>
#include <QPushButton>
#include <QLabel>
#include <QCheckBox>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QCheckBox>
#include <QDebug>
#include <QAbstractItemView>
#include "cardtable.h"

FilterEditDialog::FilterEditDialog(Directory *pDirectory, CardList *pCardList, CardTable *pCardTable, QWidget *parent) : QDialog(parent)
//Конструктор окна
{
    directory = pDirectory;
    cardList = pCardList;
    cardTable = pCardTable;

    //Создаю копии списков объектов фильтрации (каких учителей, предметы и классы показывать)
    teacherFilterList = cardList->teacherFilterList;
    disciplineFilterList = cardList->disciplineFilterList;
    classFilterList = cardList->classFilterList;

    setWindowTitle("Настройки фильтра");
    QGridLayout *mainLayout = new QGridLayout();
    setLayout(mainLayout);
    QPushButton *okButton = new QPushButton("Принять");
    QPushButton *cancelButton = new QPushButton("Отменить");
    okButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogOkButton));
    cancelButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogCancelButton));
    connect(okButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    //Чек бокс для режима оторюражения карточек
    QCheckBox *cardVisionBox = new QCheckBox();
    cardVisionBox->setChecked(cardList->isCardVisible());
    cardVisionBox->setText("Отображать закончившиеся карточки");
    cardVisible = cardVisionBox->isChecked();
    connect(cardVisionBox, &QCheckBox::stateChanged, [=](){cardVisible = cardVisionBox->isChecked();});

    //Создаю списки объектов, присутствующих в списке карточек (прохожусь по ним и вытаскиваю учителей и остальные атрибуты по одному экземпляру)
    QList<Teacher*> teacherList;
    QList<Discipline*> disciplineList;
    QVector<Class*> tempClassVector;
    for (int itemIndex = 0; itemIndex < cardList->count(); itemIndex++)
    {
        Card *card = (Card*)(cardList->itemWidget(cardList->item(itemIndex)));
        if (!teacherList.contains(card->getTeacher()))
        {
            teacherList.push_back(card->getTeacher());
        }
        if (!disciplineList.contains(card->getDiscipline()))
        {
            disciplineList.push_back(card->getDiscipline());
        }
        if (!tempClassVector.contains(card->getClass()))
        {
            tempClassVector.push_back(card->getClass());
        }
    }

    //Костыльное временное решение - сортирую свписок классов
    QVector<Class*> classVector;
    for (Class *_class : cardTable->classList)
    {
        if (tempClassVector.contains(_class))
        {
            classVector.push_back(_class);
        }
    }

    QTreeWidget *tree = new QTreeWidget();
    tree->setHeaderHidden(true);
    tree->setSelectionMode(QAbstractItemView::NoSelection);

    QTreeWidgetItem *teacherTree = new QTreeWidgetItem(tree);
    QCheckBox *teacherBox = new QCheckBox();
    teacherBox->setChecked(teacherFilterList.count() == teacherList.count());
    tree->setItemWidget(teacherTree, 0,  teacherBox);
    teacherBox->setText("Учителя");

    for(Teacher *teacher : teacherList)
    {
        QTreeWidgetItem *newItem = new QTreeWidgetItem(teacherTree);
        QCheckBox *newBox = new QCheckBox();
        newBox->setText(teacher->getSecondName());
        if(teacherFilterList.contains(teacher))
        {
            newBox->setChecked(true);
            teacherTree->setExpanded(true);
        }
        tree->setItemWidget(newItem, 0, newBox);
        connect(newBox, &QCheckBox::stateChanged, [=] () {slot_changeTeacherList(teacher, newBox->isChecked());});
        connect(teacherBox, &QCheckBox::stateChanged,[=] () {newBox->setChecked(teacherBox->isChecked());});
    }
    if (teacherBox->isChecked())
    {
        teacherTree->setExpanded(false);
    }

    QTreeWidgetItem *disciplineTree = new QTreeWidgetItem(tree);
    QCheckBox *disciplineBox = new QCheckBox();
    disciplineBox->setChecked(disciplineFilterList.count() == disciplineList.count());
    tree->setItemWidget(disciplineTree, 0,  disciplineBox);
    disciplineBox->setText("Предметы");

    for(Discipline *discipline : disciplineList)
    {
        QTreeWidgetItem *newItem = new QTreeWidgetItem(disciplineTree);
        QCheckBox *newBox = new QCheckBox();
        newBox->setText(discipline->getName());
        if(disciplineFilterList.contains(discipline))
        {
            newBox->setChecked(true);
            disciplineTree->setExpanded(true);
        }
        tree->setItemWidget(newItem, 0, newBox);
        connect(newBox, &QCheckBox::stateChanged, [=] () {slot_changeDisciplineList(discipline, newBox->isChecked());});
        connect(disciplineBox, &QCheckBox::stateChanged,[=] () {newBox->setChecked(disciplineBox->isChecked());});
    }
    if (disciplineBox->isChecked())
    {
         disciplineTree->setExpanded(false);
    }

    QTreeWidgetItem *classTree = new QTreeWidgetItem(tree);
    QCheckBox *classBox = new QCheckBox();
    classBox->setChecked(classFilterList.count() == classVector.count());
    tree->setItemWidget(classTree, 0,  classBox);
    classBox->setText("Классы");

    for (Class *_class : classVector)
    {
        QTreeWidgetItem *newItem = new QTreeWidgetItem(classTree);
        QCheckBox *newBox = new QCheckBox();
        newBox->setText(_class->getName());
        if(classFilterList.contains(_class))
        {
            newBox->setChecked(true);
            classTree->setExpanded(true);
        }
        tree->setItemWidget(newItem, 0, newBox);
        connect(newBox, &QCheckBox::stateChanged, [=] () {slot_changeClassList(_class, newBox->isChecked());});
        connect(classBox, &QCheckBox::stateChanged,[=] () {newBox->setChecked(classBox->isChecked());});
    }
    if(classBox->isChecked())
    {
        classTree->setExpanded(false);
    }


    mainLayout->addWidget(cardVisionBox, 0, 0, 1, 2);
    mainLayout->addWidget(tree, 1, 0, 1, 2);
    mainLayout->addWidget(okButton, 2, 0);
    mainLayout->addWidget(cancelButton, 2, 1);

    setFixedWidth(sizeHint().width());
    setMinimumHeight(400);
}

bool FilterEditDialog::isCardVisible()
{
    return cardVisible;
}

void FilterEditDialog::slot_changeTeacherList(Teacher *teacher, bool isNeedToPush)
//Слот для редактирования списка фильтрации учителей - срабатывает при нажатие на соотв-ий чекбокс
{
    if(isNeedToPush)
    {
        teacherFilterList.push_back(teacher);
    }
    else
    {
        teacherFilterList.removeOne(teacher);
    }
}

void FilterEditDialog::slot_changeDisciplineList(Discipline *discipline, bool isNeedToPush)
//Слот для редактирования списка фильтрации предметов - срабатывает при нажатие на соотв-ий чекбокс
{
    if(isNeedToPush)
    {
        disciplineFilterList.push_back(discipline);
    }
    else
    {
        disciplineFilterList.removeOne(discipline);
    }
}

void FilterEditDialog::slot_changeClassList(Class *_class, bool isNeedToPush)
//Слот для редактирования списка фильтрации классов - срабатывает при нажатие на соотв-ий чекбокс
{
    if(isNeedToPush)
    {
        classFilterList.push_back(_class);
    }
    else
    {
       classFilterList.removeOne(_class);
    }
}
