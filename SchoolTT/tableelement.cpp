#include "tableelement.h"
#include <QColor>
#include <QFont>
#include <QPainter>
#include <QtMath>
#include <QDebug>

//Базовый класс элементов таблицы

TableElement::TableElement(QWidget *parent) : QWidget(parent)
  //Конструктор
{
}

void TableElement::setName(QString txt)
//Сеттер названия
{
    name = txt;
    this->repaint();
    emit nameChanged();
}

QString TableElement::getName() const
//Геттер названия
{
    return name;
}

void TableElement::paintEvent(QPaintEvent *event)
//Переопределяю событие перерисовки
{
    QPainter painter(this);
    painter.setBrush(_backgroundColor);
    painter.save();
    QPen *newPen = new QPen();
    if (!isFocused)
    {
        newPen->setColor(_borderColor);
    }
    else
    {
        newPen->setColor(_focusedColor);
    }
    newPen->setWidth(2);
    painter.setPen(*newPen);
    painter.drawRect(rect());
    painter.restore();
    painter.setPen(_foregroundColor);
    QFont font;
    font.setPointSize(fontSize);
    font.setBold(true);
    painter.setFont(font);

    //Проверяю помещается ли строка в ячейку
    QFontMetrics fontMetrics(font);
    QString str = name;
    //Если не помещается - пишу только первые три символа и троеточик
    if(fontMetrics.width(str) > rect().width()*0.8)
    {
        painter.drawText(rect(), Qt::AlignCenter, name.left(3) + "...");
    }
    //Иначе пишу всю строку
    else
    {
        painter.drawText(rect(), Qt::AlignCenter, name);
    }

    delete newPen;
}

void TableElement::resizeEvent(QResizeEvent *event)
 //Переопрделяю события изменения размера, в нем изменяю шрифт
{
    //Определяю во сколько изменились ширина и выстота
    double heightDiff = rect().height()/defaultHeight;
    double widthDiff = rect().width()/defaultWidth;
    double minDiff;

    //Определяю что меньше изменилось - ширина или высота
    if (heightDiff < widthDiff)
    {
         minDiff = heightDiff;
    }
    else
    {
        minDiff = widthDiff;
    }

    //Таким образом если увеличивать только ширину или длину, то шрифт не будет меняться, но если после этого увеличивать другой параметр -
    // - то шрифт будет увеличиваться
    fontSize = fontSize*minDiff;

    //Тута храню ширину и длину абстрактного четырехугольника, у которого соотношение сторон равно первоначальному четырехугольнику
    defaultHeight = defaultHeight*minDiff;
    defaultWidth = defaultWidth*minDiff;
}

void TableElement::mouseReleaseEvent(QMouseEvent *event)
//Переопредляю событие отпускания мыши после нажатия над виджетом
{
    //При отпускании лкм над виджетом
    if (event->button() == Qt::LeftButton && rect().contains(event->pos()))
    {
        //Высылаю сигнал взятия в фокус
        emit focused();
    }
}

void TableElement::setVisible(bool visible)
{
    QWidget::setVisible(visible);
    emit signal_changeCellVisible(visible);
    emit signal_visibleChanged(visible);
}

