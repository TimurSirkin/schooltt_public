#ifndef CELL_H
#define CELL_H

#include <QPoint>
#include <QSize>
#include "class.h"
#include "tableelement.h"
class Card;
class CardTable;
class Lesson;

class Cell : public TableElement
{
public:
    Cell(CardTable *parent = nullptr);

    void setFirstCard(Card *value);
    void setSecondCard(Card *value);
    void setClass(Class *pClass);
    void setLesson(Lesson *pLesson);
    Card *getFirstCard();
    Card *getSecondCard();
    Card *getCard(bool isFirst);
    Class *getClass();
    Lesson *getLesson();
    bool isEmpty();
    int getCardCount();

    void clearAll();
    void clearFirst(bool isFromSetter = true);
    void clearSecond();
    void swapCardOn(Card *hostCard, Card *guestCard);
    void removeCard(Card *card);
    void swapFfromF(Cell * guestCell);
    void swapFfromS(Cell * guestCell);
    void swapSfromF(Cell * guestCell);
    void swapSfromS(Cell * guestCell);

    QMetaObject::Connection classNameConnection;
    QMetaObject::Connection classVisibleConnection;

    bool isCardTeacherValid(Card *card, bool isFirst);
    bool cardValidation(Card *card, bool isFirst);

private:
    Card *firstCard = nullptr;
    Card *secondCard = nullptr;
    Lesson *lesson;
    Class *_class;
    CardTable *cardTable;

    QMetaObject::Connection fCardDestroyedConnection;
    QMetaObject::Connection sCardDestroyedConnection;

    QPoint dragStartPosition;

    QAction *clearFirstAction = new QAction();
    QAction *clearSecondAction= new QAction();
    QAction *clearAllAction= new QAction();

    QPoint savedPoint;
    QSize savedSize;


     //QWidget interface
protected:
    void paintEvent(QPaintEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void contextMenuEvent(QContextMenuEvent *event);
    void enterEvent(QEvent *event);
};

#endif // CELL_H
